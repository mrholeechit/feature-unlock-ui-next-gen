import React from 'react';
import { length, last, dropLast } from 'ramda';
import { number, any, string, bool } from 'prop-types';
import cn from 'classnames';

const handleInput = (event) => {
  if (event.target.type === 'number' && !length(last(event.target.value))) {
    event.target.value = dropLast(1, event.target.value); // eslint-disable-line
  }

  if (event.target.maxLength > -1 && event.target.value > event.target.maxLength) {
    event.target.value = event.target.value.slice(0, event.target.maxLength); // eslint-disable-line
  }

  return event;
};

const Input = ({
  maxLength,
  error,
  value,
  autoHeight,
  className,
  ...props
}) => (
  <input
    {...props}
    onInput={handleInput}
    value={value}
    className={cn('input', className, {
      '-error': !!length(error),
      '-autoHeight': autoHeight,
    })}
    maxLength={maxLength}
  />
);

Input.propTypes = {
  maxLength: number,
  autoHeight: bool,
  value: any,
  className: string,
  error: any,
};

export default Input;
