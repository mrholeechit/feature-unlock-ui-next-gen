import React from 'react';
import { multiply, subtract } from 'ramda';
import { func, number } from 'prop-types';
import styled from 'styled-components';
import Button from 'components/Button';

const StyledButton = styled(Button)`
  margin-right: 10px;

  &:last-child {
    margin-right: 0;
  }
`;

const Wrapper = styled.div`
  margin: 0 auto;
  width: 310px;
  text-align: center;
`;

const Pager = ({
  onPreviousPage,
  onNextPage,
  offset,
  length,
  limit,
}) => (
  <Wrapper>
    <StyledButton
      aria-label="previous"
      color="alto"
      disabled={offset === 0}
      onClick={onPreviousPage}
    >
      &laquo; Previous
    </StyledButton>
    <StyledButton
      aria-label="next"
      color="alto"
      disabled={subtract(length, multiply(offset, limit)) <= limit}
      onClick={onNextPage}
    >
      Next &raquo;
    </StyledButton>
  </Wrapper>
);

Pager.propTypes = {
  onPreviousPage: func,
  onNextPage: func,
  offset: number,
  limit: number,
  length: number,
};

export default Pager;
