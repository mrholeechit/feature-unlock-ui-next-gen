import styled from 'styled-components';
import theme from 'utils/styles';

const states = {
  processing: theme.color.viking,
  failed: theme.color.monza,
  completed: theme.color.fern,
};

export default styled.span`
  padding: 5px;
  font-size: 12px;
  font-weight: ${theme.font.bold};
  color: #fff;
  text-align: center;
  border-radius: ${theme.border.radius};
  background: ${({ state }) => state && states[state]};
`;
