import React from 'react';
import { func, node } from 'prop-types';

const handleSubmit = (onSubmit) => (evt) => {
  evt.preventDefault();
  onSubmit(evt);
};

const Form = ({
  onSubmit,
  children,
  ...props
}) => (
  <form onSubmit={handleSubmit(onSubmit)} {...props}>
    { children }
  </form>
);

Form.propTypes = {
  onSubmit: func,
  children: node,
};

export default Form;
