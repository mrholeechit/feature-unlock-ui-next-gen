import React from 'react';
import styled from 'styled-components';
import { string, node, object, bool, arrayOf } from 'prop-types';
import theme, { mixins } from 'utils/styles';
import { Accordion as ReAccordion, AccordionItem, AccordionItemTitle, AccordionItemBody } from 'react-accessible-accordion';

const StyledAccordion = styled(ReAccordion)`
  float: left;
  width: 100%;
  margin-bottom: 10px;

  &:last-child {
    margin-bottom: 0;
  }

  .accordion__title {
    padding: 10px;
    float: left;
    width: 100%;
    border: 1px solid ${theme.color.alto};
    border-radius: ${theme.border.radius};

    &[aria-expanded="true"] .title:before {
      transform: translate(0,-50%) rotate(90deg);
    }
  }

  .title {
    float: left;
    width: 100%;
    position: relative;
    padding-left: 20px;

    &:before {
      content: '';
      width: 0;
      height: 0;
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-left: 7px solid;
      position: absolute;
      ${mixins.center('vertical')}
      left: 0;
      transition: transform ${theme.transition};
    }
  }

  .accordion__item {
    float: left;
    width: 100%;
  }

  .accordion__body{
    float: left;
    width: 100%;
  }
`;

const Accordion = ({
  title,
  children,
  items,
  expanded = false,
  ...props
}) => (
  <StyledAccordion {...props}>
    <Choose>
      <When condition={items}>
        <For each="item" index="index" of={items}>
          <AccordionItem key={index} expanded={expanded}>
            <AccordionItemTitle>
              <strong className="title">{item.title}</strong>
            </AccordionItemTitle>
            <AccordionItemBody>
              {item.children}
            </AccordionItemBody>
          </AccordionItem>
        </For>
      </When>
      <Otherwise>
        <AccordionItem expanded={expanded}>
          <AccordionItemTitle>
            <strong className="title">{title}</strong>
          </AccordionItemTitle>
          <AccordionItemBody>
            {children}
          </AccordionItemBody>
        </AccordionItem>
      </Otherwise>
    </Choose>
  </StyledAccordion>
);

Accordion.propTypes = {
  title: string,
  children: node,
  expanded: bool,
  items: arrayOf(object),
};

export default Accordion;
