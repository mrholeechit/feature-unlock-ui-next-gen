import React from 'react';
import ReSelect from 'react-select';
import styled from 'styled-components';
import theme from 'utils/styles';
import { func, string } from 'prop-types';
import { propOr } from 'ramda';

const handleChange = (name, onChange) => (data) => onChange({
  target: {
    name,
    value: propOr(data, 'value', data),
  },
});

const StyledSelect = styled(ReSelect)`
  &:hover .Select-control {
    box-shadow: none;
  }

  .Select-control {
    border: ${theme.border.width} ${theme.color.alto} solid;
    cursor: text;
    border-radius: ${theme.border.radius};
    overflow: auto;
  }

  .Select-placeholder {
    color: ${theme.color.mineShaft};
    opacity: 0.4;
    padding-left: 8px;
  }

  .Select-menu-outer {
    border-bottom-right-radius: ${theme.border.radius};
    border-bottom-left-radius: ${theme.border.radius};
    border-color: ${theme.color.alto} !important;
    box-shadow: none;
    z-index: 2;
  }

  .Select-input {
    padding: 0;

    > input {
      padding: 8px;
      font-size: ${theme.font.base};

      &:focus {
        outline: -webkit-focus-ring-color auto 5px;
      }
    }
  }

  .Select-option {
    padding: 5px 8px;
    font-size: ${theme.font.small};
    color: ${theme.color.mineShaft};

    &.is-focused {
      background: ${theme.color.concrete};
    }
  }

  .Select-noresults {
    color: ${theme.color.mineShaft};
    opacity: 0.4;
  }
`;

const Select = ({
  onChange,
  name,
  id,
  ...props
}) => (
  <StyledSelect
    onChange={handleChange(name, onChange)}
    clearable={false}
    autosize={false}
    openOnFocus
    inputProps={{
      id,
    }}
    {...props}
  />
);

Select.propTypes = {
  onChange: func,
  name: string,
  id: string,
};

export default Select;
