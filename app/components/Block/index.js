import styled, { css } from 'styled-components';

export default styled.div`
  float: left;
  width: 100%;

  ${({ center }) => center && css`
    text-align: center;
  `}

  ${({ right }) => right && css`
    text-align: right;
  `}

  ${({ left }) => left && css`
    text-align: left;
  `}
`;
