import React from 'react';
import styled from 'styled-components';
import Button from '../Button';

const Bar = styled.span`
  display: block;
  width: 22px;
  height: 2px;
  border-radius: 1px;
  background-color: #fff;
  margin-top: 4px;

  &:first-child {
    margin-top: 0;
  }
`;

const HarburgerWrapper = styled(Button)`
  margin-right: 15px;
  padding: 9px 10px;
  margin-top: 8px;
  margin-bottom: 8px;
  vertical-align: middle;
  background-color: transparent;
  background-image: none;
  border: 1px solid #333;
  border-radius: 4px;
  margin: 0;

  &:hover,
  &:focus,
  &:visited {
    background-color: #333 !important;
    border-color: #333 !important;
  }
`;

const Harburger = (props) => (
  <HarburgerWrapper {...props}>
    <Bar />
    <Bar />
    <Bar />
  </HarburgerWrapper>
);

export default Harburger;
