import React from 'react';
import { string, oneOfType, object, node, func, bool } from 'prop-types';
import { Link as ReLink } from 'react-router';
import styled, { css } from 'styled-components';
import isExternal from 'is-url-external';
import theme from 'utils/styles';

const DefaultLink = styled.a`
  padding: 0;
  color: ${theme.color.sanJuan};
  font-weight: ${theme.font.bold};

  ${({ underlined }) => underlined && css`
    border-bottom: 1px solid ${theme.color.sanJuan};
  `}

  ${({ small }) => small && css`
    font-size: ${theme.font.small};
  `}

  &:disabled {
    opacity: .5;
    cursor: not-allowed;
  }
`;

const StyledLink = DefaultLink.withComponent(ReLink);

const StyledButton = DefaultLink.withComponent('button');

const Link = ({
  to,
  onClick,
  children,
  ...props
}) => (
  <Choose>
    <When condition={isExternal(to)}>
      <DefaultLink href={to} {...props}>{children}</DefaultLink>
    </When>
    <When condition={to}>
      <StyledLink to={to} {...props}>{children}</StyledLink>
    </When>
    <Otherwise>
      <StyledButton onClick={onClick} {...props}>{children}</StyledButton>
    </Otherwise>
  </Choose>
);

Link.propTypes = {
  to: oneOfType([
    string,
    object,
  ]),
  children: node,
  onClick: func,
  underlined: bool,
};

export default Link;
