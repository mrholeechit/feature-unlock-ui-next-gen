/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import { string, func, bool, node } from 'prop-types';
import styled, { css } from 'styled-components';
import Icon from 'components/Icon';
import theme, { mixins } from 'utils/styles';

const Wrapper = styled.span`
  display: inline-block;
  font-size: 0;
  position: relative;

  input {
    width: 0;
    height: 0;
    float: left;

    &:checked + label {
      background: ${theme.color.fern};
      border-color: ${theme.color.fern};

      .icon svg {
        fill: #fff;
      }
    }
  }

  label {
    transition: all ${theme.transition}
  }

  ${({ error }) => error && css`
    label {
      border-color: ${theme.color.monza};
      color: ${theme.color.monza};
      border-width: 2px;
    }
  `}
`;

const Check = styled.label`
  width: 21px;
  height: 21px;
  display: inline-block;
  position: relative;
  border: ${theme.border.width} solid ${theme.color.mineShaft};
  border-radius: ${theme.border.radius};
  transition: all ${theme.transition};
  cursor: pointer;
  vertical-align: middle;

  .icon {
    width: calc(100% - 5px);
    height: calc(100% - 5px);
    position: absolute;
    ${mixins.center()}

    svg {
      fill: transparent;
      transition: all ${theme.transition}
    }
  }
`;

const Message = styled.label`
  vertical-align: middle;
  font-size: ${theme.font.base};
  margin-left: 5px;
`;

const Checkbox = ({
  name,
  onChange,
  checked,
  children,
  ...props
}) => (
  <Wrapper {...props}>
    <input type="checkbox" name={name} id={name} onChange={onChange} checked={checked} />
    <Check htmlFor={name}>
      <Icon name="check" />
    </Check>
    <If condition={children}>
      <Message htmlFor={name}>{children}</Message>
    </If>
  </Wrapper>
);

Checkbox.propTypes = {
  name: string,
  onChange: func,
  checked: bool,
  children: node,
};

export default Checkbox;
