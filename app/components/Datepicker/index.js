import React from 'react';
import Flatpickr from 'react-flatpickr';
import styled from 'styled-components';
import { func, string, object } from 'prop-types';

const StyledDatepicker = styled(Flatpickr)`
  background-color: #fff !important;
`;

const handleChange = (name, onChange) => (value) => onChange({
  target: {
    name,
    value,
  },
});

const Datepicker = ({
  name,
  onChange,
  options,
  ...props
}) => (
  <StyledDatepicker
    onChange={handleChange(name, onChange)}
    className="input"
    options={{
      dateFormat: 'm/d/Y H:i',
      ...options,
    }}
    ref={(node) => { this.node = node; }}
    {...props}
  />
);

Datepicker.propTypes = {
  onChange: func,
  name: string,
  options: object,
};

export default Datepicker;
