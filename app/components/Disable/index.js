import React, { Component } from 'react';
import { func } from 'prop-types';

export const disableOnClick = (WrappedComponent) =>
  class Disabled extends Component {
    static propTypes = {
      onClick: func,
    };

    state = {
      disabled: false,
    };

    handleClick = (onClick) => (evt) => {
      this.setState({ disabled: true });
      onClick(evt);
    }

    render() {
      const { onClick, ...props } = this.props;
      return (
        <WrappedComponent onClick={this.handleClick(onClick)} disabled={this.state.disabled} {...props} />
      );
    }
  };
