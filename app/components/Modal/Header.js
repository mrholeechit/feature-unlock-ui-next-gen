import React from 'react';
import { node, func } from 'prop-types';
import styled from 'styled-components';
import theme, { mixins } from 'utils/styles';
import Icon from 'components/Icon';
import Button from 'components/Button';

const padding = '20px';

const Header = styled.header`
  float: left;
  width: 100%;
  padding: ${padding} ${padding} ${padding} 65px;
  background: ${theme.color.mineShaft};
  color: #fff;
  position: relative;
  font-size: ${theme.font.medium};
  text-align: center;
  font-weight: ${theme.font.semiBold};
`;

const StyledButton = styled(Button)`
  position: absolute;
  ${mixins.center('vertical')}
  left: ${padding};
  padding: 5px;
  border: none;

  .icon svg {
    fill: #fff;
    width: 25px;
    height: 25px;
  }
`;

export default function StyledHeader({
  children,
  onClose,
}) {
  return (
    <Header>
      <StyledButton color="transparent" onClick={onClose}>
        <Icon name="close" />
      </StyledButton>
      {children}
    </Header>
  );
}

StyledHeader.propTypes = {
  children: node,
  onClose: func,
};
