import React from 'react';
import { node, func } from 'prop-types';
import ReactModal from 'react-modal';
import theme from 'utils/styles';
import styled, { css } from 'styled-components';
import Header from './Header';
import Content from './Content';

const style = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    overflow: 'auto',
    textAlign: 'center',
    padding: theme.container.gutter,
  },
  content: {
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    border: 'none',
    textAlign: 'left',
    display: 'inline-block',
    verticalAlign: 'middle',
    background: theme.color.concrete,
    WebkitOverflowScrolling: 'touch',
    borderRadius: theme.border.radius,
    outline: 'none',
    padding: 0,
    boxShadow: '0 3px 20px rgba(0, 0, 0, 0.2)',
    backfaceVisibility: 'hidden',
  },
};

const StyledModal = styled(ReactModal)`
  max-width: 800px;
  width: 100%;

  ${({ small }) => small && css`
    max-width: 600px;
  `}
`;

export default function Modal({
  children,
  title,
  onClose,
  ...props
}) {
  return (
    <If condition={process.env.NODE_ENV !== 'test'}>
      <StyledModal
        contentLabel={title}
        style={style}
        onRequestClose={onClose}
        closeTimeoutMS={200}
        {...props}
      >
        <Header onClose={onClose}>{title}</Header>
        <Content>
          {children}
        </Content>
      </StyledModal>
    </If>
  );
}

Modal.propTypes = {
  title: node.isRequired,
  onClose: func,
  children: node,
};
