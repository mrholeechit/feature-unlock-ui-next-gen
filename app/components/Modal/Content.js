import styled from 'styled-components';

const Content = styled.div`
  float: left;
  width: 100%;
  padding: 50px;
`;

export default Content;
