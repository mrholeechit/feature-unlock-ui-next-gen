import React from 'react';
import styled, { keyframes } from 'styled-components';
import Letter from './Letter';

const generateLetterKeyframe = (startsAt, endsAt) => keyframes`
  ${startsAt}% {
    transform: translate(-25px, 50px);
    opacity: 0;
  }
  ${endsAt}% {
    transform: translate( 0px, 0px);
    opacity: 1;
  }
  100% {
    transform: translate( 0px, 0px);
    opacity: 1;
  }
`;

const toggleLetterA = generateLetterKeyframe(0, 10);
const toggleLetterG = generateLetterKeyframe(10, 20);
const toggleLetterC = generateLetterKeyframe(20, 30);
const toggleLetterO = generateLetterKeyframe(30, 40);

const Word = styled.div`
  display: inline-block;
  vertical-align: bottom;
  margin-left: -14px;
  margin-bottom: 1px;

  div:nth-child(1) {
    animation-name: ${toggleLetterA};
  }
  div:nth-child(2) {
    animation-name: ${toggleLetterG};
  }
  div:nth-child(3) {
    animation-name: ${toggleLetterC};
  }
  div:nth-child(4) {
    animation-name: ${toggleLetterO};
  }
`;

export default () => (
  <Word>
    <Letter>A</Letter>
    <Letter>G</Letter>
    <Letter>C</Letter>
    <Letter>O</Letter>
  </Word>
);
