import styled from 'styled-components';

export default styled.div`
  display: inline-block;
  vertical-align: bottom;
  background: url(http://i.imgur.com/EF3ttqt.png) no-repeat 50% 50%;
  width: 82px;
  height: 61px;
  background-size: cover;
  position: relative;
  z-index: 1;
`;
