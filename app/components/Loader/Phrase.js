import React from 'react';
import styled from 'styled-components';

const Phrase = styled.p`
  float: left;
  width: 100%;
  font-size: 12px;
  letter-spacing: 1.5px;
  font-family: 'Michroma';
  padding-left: 0;
  margin: 3px 0 0;
  font-weight: bold;
  color: #fff;
`;

export default () => (
  <Phrase>Your Agriculture Company</Phrase>
);
