import styled from 'styled-components';

export default styled.div`
  display: inline-block;
  vertical-align: bottom;
  font-weight: bold;
  font-size: 42px;
  line-height: 37px;
  letter-spacing: -2px;
  color: #fff;
  font-family: 'Michroma';
  position: relative;
  opacity: 0;
  animation-fill-mode: both;
  animation-iteration-count: infinite;
  animation-duration: 2s;
`;
