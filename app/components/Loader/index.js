import React from 'react';
import styled from 'styled-components';
import { VelocityTransitionGroup } from 'velocity-react';
import { bool } from 'prop-types';
import Brand from './Brand';
import Word from './Word';
import Phrase from './Phrase';

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.6);
  z-index: 9999999;
`;

const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  width: 244px;
  transform: translate(-50%, -50%);
`;

const Group = styled.div`
  display: inline-block;
  vertical-align: bottom;
`;

const StyledLoader = () => (
  <Overlay>
    <Wrapper>
      <Group>
        <Brand />
        <Word />
      </Group>
      <Phrase />
    </Wrapper>
  </Overlay>
);

const Loader = ({ show }) => (
  <VelocityTransitionGroup enter={{ animation: 'fadeIn' }} leave={{ animation: 'fadeOut' }} >
    <If condition={show}>
      <StyledLoader />
    </If>
  </VelocityTransitionGroup>
);

Loader.propTypes = {
  show: bool,
};

export default Loader;
