import styled, { css } from 'styled-components';
import theme from 'utils/styles';

export default styled.strong.attrs({
  className: 'title',
})`
  color: ${theme.color.monza};
  margin: 0;
  font-size: ${theme.font.base};
  font-weight: ${theme.font.bold};

  ${({ big }) => big && css`
    font-size: ${theme.font.large};
  `}

  ${({ block }) => block && css`
    float: left;
    width: 100%;
  `}
`;
