import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.p.attrs({
  className: 'alert',
})`
  float: left;
  width: 100%;
  padding: 5px 10px;
  border-radius: ${theme.border.radius};
  background-color: #fff3cd;
  border: 1px solid #ffc200;
  margin: 0;
`;
