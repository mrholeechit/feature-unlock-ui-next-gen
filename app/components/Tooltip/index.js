import React, { Component } from 'react';
import mitt from 'mitt';
import { equals } from 'ramda';
import ReTooltip from 'rc-tooltip';
import { node, string } from 'prop-types';
import styled from 'styled-components';
import theme from 'utils/styles';

const emitter = mitt();

const Overlay = styled.div`
  border-radius: 7.5px;
  padding: 15px;
  box-shadow: 0 0 20px 0px rgba(51, 51, 51, 0.50);
  max-width: 266px;
`;

const Title = styled.strong`
  width: 100%;
  color: ${theme.color.mineShaft};
  font-size: ${theme.font.base};
  display: block;
  margin-bottom: 10px;
  text-align: center;
  text-transform: uppercase;
`;

const Body = styled.div`
  text-align: center;
  word-wrap: break-word;
`;

class Tooltip extends Component {
  static hide = (id) => {
    Tooltip.changeVisibility(id, false);
  }

  static show = (id) => {
    Tooltip.changeVisibility(id, true);
  }

  static changeVisibility = (id, visible) => {
    emitter.emit(`tooltip-${id}`, visible);
  }

  constructor() {
    super();

    this.state = {
      visible: false,
    };
  }

  componentDidMount() {
    const { id } = this.props;

    emitter.on(`tooltip-${id}`, this.onVisibleChange);
  }

  onVisibleChange = (show) => {
    this.setState({
      visible: show,
    });
  };

  render() {
    const {
      overlay,
      children,
      title,
      type,
      placement = 'left',
      ...props
    } = this.props;

    return (
      <ReTooltip
        placement={placement}
        trigger={equals('error', type) ? ['none'] : ['click']}
        visible={this.state.visible}
        onVisibleChange={this.onVisibleChange}
        overlayClassName={`-${type}`}
        overlay={
          <Overlay className="overlay">
            <If condition={title}>
              <Title className="title">{title}</Title>
            </If>
            <Body className="body">{overlay}</Body>
          </Overlay>
        }
        {...props}
      >
        { children }
      </ReTooltip>
    );
  }
}

Tooltip.propTypes = {
  overlay: node,
  children: node,
  id: string,
  title: string,
  type: string,
  placement: string,
};

export default Tooltip;
