import React from 'react';
import styled, { css } from 'styled-components';
import theme from 'utils/styles';

const styles = {
  transparent: {
    default: css`
      background: transparent;
      color: #fff;
      border-color: #fff;
    `,
  },
  alto: {
    default: css`
      border-color: ${theme.color.alto};
    `,
    hover: css`
      &:hover:before {
        background: ${theme.color.alto};
        opacity: .2;
      }
    `,
  },
  mineShaft: {
    default: css`
      border-color: ${theme.color.mineShaft};
      color: ${theme.color.mineShaft};
    `,
    filled: css`
      background: ${theme.color.mineShaft};
      color: #fff;
    `,
  },
  sanJuan: {
    default: css`
      border-color: ${theme.color.sanJuan};
      color: ${theme.color.sanJuan};
    `,
    filled: css`
      background: ${theme.color.sanJuan};
      color: #fff;
    `,
  },
  monza: {
    default: css`
      border-color: ${theme.color.monza};
      color: ${theme.color.monza};
    `,
    filled: css`
      background: ${theme.color.monza};
      color: #fff;
    `,
  },
};

const Button = styled.button`
  padding: 10px 35px;
  border: 2px solid #fff;
  background: #fff;
  overflow: hidden;
  color: ${theme.color.mineShaft};
  position: relative;
  border-radius: ${theme.border.radius};
  font-size: ${theme.font.base};
  letter-spacing: 1px;
  font-weight: ${theme.font.bold};
  transition: all ${theme.transition};

  &:before {
    content: '';
    width: 50px;
    height: 50px;
    background: rgba(255, 255, 255, 0.25);
    position: absolute;
    left: -50px;
    bottom: -50px;
    border-radius: 50%;
    transition: all ${theme.transition};
  }

  &:hover:before {
    transform: scale(10);
  }

  &:disabled {
    opacity: .5;
    cursor: not-allowed;
  }

  ${({ compact }) => compact && css`
    padding: 7px 20px;
    border-width: 1px;
    font-weight: ${theme.font.normal};
    font-size: ${theme.font.tiny};
  `}

  ${({ color }) => color && (styles[color].default || '')};
  ${({ color }) => color && (styles[color].hover || '')};
  ${({ filled, color }) => filled && (styles[color].filled || '')};
`;

const Content = styled.span`
  position: relative;
  z-index: 1;
`;

export default ({ children, ...props }) => ( // eslint-disable-line react/prop-types
  <Button {...props}>
    <Content>{children}</Content>
  </Button>
);
