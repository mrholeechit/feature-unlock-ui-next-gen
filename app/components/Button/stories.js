import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import Button from 'components/Button';

const actions = {
  onClick: action('onClick'),
};

storiesOf('Button', module)
  .addWithInfo('Default', () => (
    <Button {...actions} >Text</Button>
  ))
  .addWithInfo('Primary', () => (
    <Button {...actions} primary >Text</Button>
  ))
  .addWithInfo('Transparent', () => (
    <Button {...actions} transparent>Text</Button>
  ))
  .addWithInfo('Small', () => (
    <Button {...actions} small >Text</Button>
  ));
