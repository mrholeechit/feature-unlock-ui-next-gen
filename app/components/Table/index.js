import React, { Component } from 'react';
import { arrayOf, object, string } from 'prop-types';
import { map, reject, isNil } from 'ramda';
import styled, { css } from 'styled-components';
import theme from 'utils/styles';
import ReactTable from 'react-table';
import NoData from './NoData';

const StyledReactTable = styled(ReactTable)`
  display: ${({ block }) => block ? 'block' : 'flex'};
  border-radius: ${theme.border.radius};
  overflow: visible;

  .rt-tbody,
  .rt-table {
    overflow: visible;
  }

  ${({ style: { height } }) => height && css`
    overflow: hidden;

    .rt-tbody,
    .rt-table {
      overflow: auto;
    }
  `}

  .rt-thead {
    overflow-y: ${({ scroll }) => scroll ? 'scroll' : 'auto'};
    background: #FFF;
    border-bottom: 1px solid ${theme.color.alto};
    box-shadow: none !important;
  }

  .rt-tr {
    text-align: left !important;
  }

  .rt-td,
  .rt-th {
    white-space: normal;
    text-overflow: unset;
    padding: 15px !important;
    display: flex;
    overflow: visible;
    align-items: center;
    font-size: ${theme.font.base}
  }

  .rt-tr-group {
    background: #FFF;
    border-bottom-color: ${theme.color.alto};

    &:nth-child(odd) {
      background: ${theme.color.concrete};
    }
  }
`;
const header = (name) => (
  <strong title={name}>{name}</strong>
);

const convert = map(({
  field,
  name,
  width,
  format,
}) => {
  const data = {
    Header: header(name),
    accessor: format || field,
    width: width || null,
    id: field,
  };

  return reject(isNil, data);
});

let i = 0;

class CustomTable extends Component {
  constructor(props) {
    super(props);

    i = i + 1; // eslint-disable-line

    this.state = {
      scroll: false,
      element: `.table-${i} .rt-tbody`,
    };
  }

  componentDidUpdate() {
    const { scroll } = this.state;
    const element = document.querySelector(this.state.element);

    if (!element) return;

    const hasVerticalScrollbar = element.scrollHeight > element.clientHeight;

    if (scroll === hasVerticalScrollbar) return;

    this.setState({ // eslint-disable-line
      scroll: hasVerticalScrollbar,
    });
  }

  render() {
    const {
      items,
      fields,
      height,
      ...props
    } = this.props;

    const { scroll } = this.state;

    return (
      <StyledReactTable
        data={items}
        minRows={8}
        scroll={scroll}
        columns={convert(fields)}
        defaultPageSize={50}
        filterable={false}
        showPagination={false}
        showPageSizeOptions={false}
        showPageJump={false}
        sortable={false}
        NoDataComponent={NoData}
        resizable={false}
        style={{
          height,
        }}
        className={`table-${i}`}
        {...props}
      />
    );
  }
}

CustomTable.propTypes = {
  height: string,
  fields: arrayOf(object),
  items: arrayOf(object),
};


export default CustomTable;
