import React from 'react';
import { node } from 'prop-types';
import styled from 'styled-components';
import { mixins } from 'utils/styles';

const Wrapper = styled.div`
  position: absolute;
  ${mixins.center()}
`;

const NoData = ({ children }) => (
  <Wrapper>{children}</Wrapper>
);

NoData.propTypes = {
  children: node,
};

export default NoData;
