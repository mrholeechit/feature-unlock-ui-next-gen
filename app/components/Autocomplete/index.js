import React from 'react';
import { string, func, arrayOf, object, bool } from 'prop-types';
import styled from 'styled-components';
import theme from 'utils/styles';
import ReAutocomplete from 'react-autocomplete';
import { VelocityTransitionGroup } from 'velocity-react';

const StyledAutocomplete = styled.div`
  position: relative;

  > div {
    position: relative;
    width: 100%;
  }

  .list {
    position: absolute;
    top: 100%;
    transform: translateY(15px);
    left: 0;
    padding: 10px;
    background: white;
    border-radius: 3px;
    border: 1px solid ${theme.color.alto};
    z-index: 1;
    width: 100%;
    box-shadow: 0px 10px 20px -5px rgba(0, 0, 0, 0.50);

    &:before {
      content: "";
      position: absolute;
      top: -7px;
      left: 14px;
      width: 0;
      height: 0;
      padding: 6.5px;
      background: #fff;
      border: inherit;
      border-right: 0;
      border-bottom: 0;
      -webkit-transform: rotate(45deg);
      transform: rotate(45deg);
    }
  }

  .item {
    padding: 5px 7px;
    border-radius: 3px;
    transition: all ${theme.transition};
    color: ${theme.color.mineShaft};
    display: block;
    float: left;
    width: 100%;
    position: relative;
    z-index: 1;

    &:hover,
    &.-highlighted {
      background: ${theme.color.sanJuan};
      color: #fff;
      cursor: pointer;
    }
  }

  .input {
    width: 100%;
    background: #fff;
    padding: 10px;
    border: 1px solid ${theme.color.alto};
    color: ${theme.color.scorpion};
    border-radius: ${theme.border.radius};
  }
`;

export default function Autocomplete({
  value,
  items,
  onChange,
  placeholder,
  id,
  onSelect,
  autoHeight,
  autoFocus,
  getItemValue = (item) => item.value,
  renderItem = (item, isHighlighted) => (
    <span className={`item ${isHighlighted ? '-highlighted' : ''}`} key={item.value}>{item.label}</span>
  ),
  renderMenu = (children) => (
    <div className="list">{children}</div>
  ),
  ...props
}) {
  const inputProps = {
    placeholder,
    id,
    className: `input ${autoHeight ? '-autoHeight' : ''}`,
    autoFocus,
  };

  return (
    <StyledAutocomplete className="autocomplete">
      <ReAutocomplete
        items={items}
        value={value}
        onChange={onChange}
        renderMenu={(children) => (
          <VelocityTransitionGroup
            runOnMount
            enter={{
              animation: 'fadeIn',
              duration: 150,
            }}
            leave={{
              animation: 'fadeOut',
              duration: 150,
            }}
          >
            <If condition={items.length > 0}>
              {renderMenu(children)}
            </If>
          </VelocityTransitionGroup>
        )}
        onSelect={onSelect}
        getItemValue={getItemValue}
        renderItem={renderItem}
        inputProps={inputProps}
        {...props}
      />
    </StyledAutocomplete>
  );
}

Autocomplete.propTypes = {
  value: string.isRequired,
  placeholder: string,
  id: string,
  autoHeight: bool,
  autoFocus: bool,
  items: arrayOf(object),
  getItemValue: func,
  renderMenu: func,
  renderItem: func,
  onChange: func,
  onSelect: func,
  onClear: func,
};
