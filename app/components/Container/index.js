import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.div`
  max-width: ${theme.container.width};
  padding: 0 ${theme.container.gutter};
  margin: 0 auto;
`;
