import React from 'react';
import { string } from 'prop-types';
import styled from 'styled-components';

const StyledIcon = styled.i`
  display: inline-block;
  vertical-align: top;
  font-size: 0;
`;

const Icon = ({ name }) => {
  const markup = {
    __html: require(`!!svg-inline-loader?!../../images/icons/${name}.svg`), // eslint-disable-line
  };

  return (
    <StyledIcon className={`icon -${name}`} dangerouslySetInnerHTML={markup} />
  );
};

Icon.propTypes = {
  name: string,
};

export default Icon;
