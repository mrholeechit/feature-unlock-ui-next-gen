import React from 'react';
import { storiesOf } from '@kadira/storybook';
import Icon from 'components/Icon';

storiesOf('Icon', module)
  .addWithInfo('Example', () => (
    <Icon name="close" />
  ));
