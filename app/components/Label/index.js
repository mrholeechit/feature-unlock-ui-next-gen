import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.label`
  color: ${theme.color.mineShaft};
  font-weight: bold;
  font-size: ${theme.font.small};
  display: block;
  margin-bottom: 5px;
`;
