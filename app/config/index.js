import translate from 'utils/translate';
import { sortBy, prop, test } from 'ramda';

export const FEATURE_UNLOCK_URL = process.env.FEATURE_UNLOCK_URL || 'https://fuse-features-api-qa.herokuapp.com';

export const IAM_API_URL = process.env.FEATURE_UNLOCK_URL || 'https://agco-iam-test.herokuapp.com';

export const AC2_URL = process.env.AC2_URL || 'http://agcommand2-staging.herokuapp.com/';

export const SUPPORT_DASHBOARD_URL = 'https://fun-support-qa.herokuapp.com';

export const isAgcoVin = (serialNumber) => test(/(^(AGC|AG3|AAA|AKC|AVT|8AA|AMX|YK5|WAM|WAH|XRR|TAB|VKK|ZN2)[A-Za-z0-9]{14}$)/i, serialNumber);

export const API = {
  retries: 3,
  timeout: 10000,
};

export const NA_SA_CUSTOMER_REFERENCE_LENGTH = 8;
export const EAME_CUSTOMER_REFERENCE_LENGTH = 17;

const AUTH_BASE_URL = process.env.AUTH_BASE_URL || 'https://aaat.agcocorp.com/auth/UI';

export const AUTH_REDIRECT_URL = window.location.origin || 'http://localhost:3000';

export const AUTH = {
  loginUrl: `${AUTH_BASE_URL}/Login?realm=%2Fdealers&goto=https%3A%2F%2Faaat.agcocorp.com%2Fauth%2Foauth2%2Fauthorize%3Fresponse_type%3Dtoken%26client_id%3DfeatureunlockClient%26redirect_uri%3D${encodeURI(AUTH_REDIRECT_URL)}%252F%26realm%3D%252Fdealers%26scope%3Dcn%2520mail%2520agcoUUID`,
  logoutUrl: `${AUTH_BASE_URL}/Logout?goto=${AUTH_REDIRECT_URL}`,
  roles: {
    SystemUser: '2da93eea-269a-4220-b092-28648c779a1f',
    DealerUser: '5cbe4794-ae64-459d-a3a0-a1736555e602',
    InternalDealerUser: '5cbe4794-ae64-459d-a3a0-a1736555e603',
  },
  allowedUserTypes: {
    DealerUsers: 'dealerUsers',
    AgcoUsers: 'agcoUsers',
    ServiceUsers: 'serviceUsers',
  },
};

// wget -q -O /tmp/libpng12.deb http://mirrors.kernel.org/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1_amd64.deb && dpkg -i /tmp/libpng12.deb && rm /tmp/libpng12.deb

export const VENDORS = sortBy(prop('label'), [
  {
    label: 'ACM',
    title: 'ACM',
    value: 'AGCO',
    match: /(^[0-9]{6}$)|(^[0-9]{9}$)|(^AGC[A-Za-z0-9]{14}$)/i,
    description: translate('PRODUCT_INFO.GATEWAY.DESCRIPTION'),
    image: require('images/ACM.png'), // eslint-disable-line
    patterns: [
      {
        format: translate('NUMBER_FORMATS.GATEWAY_SERIALS'),
        patterns: [
          translate('NUMBER_FORMATS.GATEWAY_6_DIGITS'),
          translate('NUMBER_FORMATS.GATEWAY_9_DIGITS'),
          translate('NUMBER_FORMATS.GATEWAY_17_CHARS'),
        ],
      },
      {
        format: translate('NUMBER_FORMATS.GATEWAY_NOTE'),
        patterns: [],
      },
    ],
  },
  {
    label: 'AgCommand',
    title: 'AgCommand',
    value: 'Topcon',
    match: /(^617-[0-9]{4,5}$)|(^1223-[A-Za-z0-9]{8}$)/i,
    description: translate('PRODUCT_INFO.AGCOMMAND.DESCRIPTION'),
    image: require('images/AgCommand.png'), // eslint-disable-line
    patterns: [
      {
        format: translate('NUMBER_FORMATS.AGCOMMAND_USES'),
        patterns: [
          translate('NUMBER_FORMATS.AM50'),
          translate('NUMBER_FORMATS.AM53'),
        ],
      },
    ],
  },
  {
    label: 'AgControl',
    title: 'AgControl',
    value: 'NT03',
    match: /(^[0-9]{7}[a-zA-Z]{2}$$)|(^(AGC|AG3|AAA|AKC|AVT|8AA|AMX|YK5|WAM|WAH|XRR|TAB|VKK|ZN2)[A-Za-z0-9]{14}$)/i,
    description: translate('PRODUCT_INFO.AGCONTROL.DESCRIPTION'),
    image: require('images/AgControl.png'), // eslint-disable-line
    patterns: [
      {
        format: translate('NUMBER_FORMATS.AGCONTROL_USES'),
        patterns: [
          translate('NUMBER_FORMATS.AGCONTROL_VIN'),
          translate('NUMBER_FORMATS.AGCONTROL_TERMINAL'),
        ],
      },
      {
        format: translate('NUMBER_FORMATS.AGCONTROL_FIELDSTAR'),
        patterns: [
          translate('NUMBER_FORMATS.AGCONTROL_TERMINAL'),
        ],
      },
    ],
  },
  {
    label: 'Auto-Guide',
    title: 'Auto-Guide™',
    value: 'Trimble',
    match: /(^7[A-Za-z0-9]{5}$)/i,
    description: translate('PRODUCT_INFO.AUTO_GUIDE.DESCRIPTION'),
    image: require('images/AutoGuide.png'), // eslint-disable-line
    patterns: [
      {
        format: translate('NUMBER_FORMATS.AUTO_GUIDE_USES'),
        patterns: [
          translate('NUMBER_FORMATS.AUTO_GUIDE_1'),
        ],
      },
    ],
  },
  {
    label: 'VarioDoc Pro/TaskDoc Pro',
    title: 'VarioDoc Pro™ / TaskDoc Pro™',
    value: 'DeviceInsight',
    match: /(^[0-9]{2}[A-Za-z0-9]{7}$)|(^[0-9]{3}\s[0-9]{2}\s[0-9]{4}$)|(^TP[A-Za-z0-9]{8}$)|(^AGC[A-Za-z0-9]{14}$)/i,
    description: translate('PRODUCT_INFO.TASKDOC.DESCRIPTION'),
    image: require('images/VarioDocTaskDoc.png'), // eslint-disable-line
    patterns: [
      {
        format: translate('NUMBER_FORMATS.ACCEPTABLE_VIN_SN'),
        patterns: [
          translate('NUMBER_FORMATS.AGCO_FENDT'),
          translate('NUMBER_FORMATS.TERMINAL_9_CHAR'),
          translate('NUMBER_FORMATS.TERMINAL_10_CHAR'),
          translate('NUMBER_FORMATS.AGCO_17_CHAR'),
        ],
      },
      {
        format: translate('NUMBER_FORMATS.VARIO_NOTE'),
        patterns: [],
      },
    ],
  },
  {
    label: 'VarioGuide-Topcon',
    title: translate('NUMBER_FORMATS.VARIOGUIDE_AUTOGUIDE_TOPCON'),
    value: 'Trimble',
    match: /^2[A-Za-z0-9]{5}$/i,
    description: translate('PRODUCT_INFO.VARIOGUIDE_TOPCON.DESCRIPTION'),
    image: require('images/Topcon.png'), // eslint-disable-line
    patterns: [
      {
        format: translate('NUMBER_FORMATS.AUTO_GUIDE_USES'),
        patterns: [
          translate('NUMBER_FORMATS.SYSTEM_150'),
          translate('NUMBER_FORMATS.VARIOGUIDE_AUTOGUIDE'),
        ],
      },
    ],
  },
  {
    label: 'VarioGuide/Auto-Guide Trimble',
    title: translate('NUMBER_FORMATS.CEAGUIDE_FLEX'),
    value: 'Trimble',
    match: /^(55|56|57){1}[A-Za-z0-9]{8}$/i,
    description: translate('PRODUCT_INFO.VARIOGUIDE_TRIMBLE.DESCRIPTION'),
    image: require('images/Trimble.png'), // eslint-disable-line
    patterns: [
      {
        format: translate('NUMBER_FORMATS.CEAGUIDE_FLEX_USES'),
        patterns: [
          translate('NUMBER_FORMATS.PRODUCTS_2015'),
          translate('NUMBER_FORMATS.PRODUCTS_2016'),
        ],
      },
    ],
  },
  {
    label: 'VarioGuide/Auto-Guide NovAtel',
    title: translate('NUMBER_FORMATS.CEAGUIDE_SMART'),
    value: 'NovAtel',
    match: /(^[\S]{1}FN[\S]{8}$)|(^[\S]{1}FN[\S]{10}$)|(^NMD[\S]{8}$)|(^NMD[\S]{10}$)/i,
    description: translate('PRODUCT_INFO.VARIOGUIDE_NOVATEL.DESCRIPTION'),
    image: require('images/NovAtel.png'), // eslint-disable-line
    patterns: [
      {
        format: translate('NUMBER_FORMATS.HOW_TO_CEA'),
        patterns: [
          translate('NUMBER_FORMATS.BFN'),
          translate('NUMBER_FORMATS.NMD'),
        ],
      },
    ],
  },
]);

