import { any, pathEq, prepend, clone, concat, keys, map, reject, filter, equals, find, toString, has, dissoc, head, length, propEq, pathOr, prop, propOr, path, pick, reduce, merge, pipe, pluck, sort, uniq, groupBy } from 'ramda';
import { VENDORS } from 'config';
import { format } from 'date-fns';
import camelCase from 'lodash.camelcase';
import { ORDER_TYPES, DEVICE_FEATURE_STATUS } from 'utils/constants';

const transformOrder = (order) => {
  let transformedOrder = dissoc('activationSchedule', order);
  transformedOrder = dissoc('createdOn', order);
  if (!order.immediateActivation) {
    transformedOrder.activationDate = format(order.activationSchedule, 'MM/DD/YYYY');
  } else {
    transformedOrder.activationDate = format(order.createdOn, 'MM/DD/YYYY');
  }
  transformedOrder.createdOn = format(order.createdOn, 'MM/DD/YYYY');

  if (transformedOrder.extraAttributes) {
    transformedOrder.extraAttributes = reduce((acc, { name, value }) => merge(acc, { [camelCase(name)]: value }), {}, transformedOrder.extraAttributes);
  }

  return transformedOrder;
};

const transformEngineHours = (expirationCriteria) => {
  const engineHours = find(propEq('unit', 'engineHours'), expirationCriteria);
  return propOr(null, 'value', engineHours);
};

const transformCalendarExpiration = (expirationCriteria) => {
  const isEngineHours = (criterion) => !propEq('unit', 'engineHours')(criterion);
  return find(isEngineHours, expirationCriteria);
};

const transformExpirationDate = (expirationCriteria) => {
  const expirationDate = find(propEq('unit', 'expirationDate'), expirationCriteria);
  return format(prop('value', expirationDate), 'MM/DD/YYYY');
};

export const transformDeviceFeature = (deviceFeature) => {
  if (!has('expirationCriteria', deviceFeature)) return deviceFeature;

  const expirationCriteria = prop('expirationCriteria', deviceFeature);
  const transformedDeviceFeature = dissoc('expirationCriteria', deviceFeature);
  transformedDeviceFeature.expirationDate = transformExpirationDate(expirationCriteria);
  transformedDeviceFeature.expirationEngineHours = transformEngineHours(expirationCriteria);
  return transformedDeviceFeature;
};

const applyOverrides = (feature, overrides) => {
  const processedOverrides = reduce(merge, {}, overrides);
  let transformedfeature = merge(feature, processedOverrides);
  transformedfeature = dissoc('country', transformedfeature);
  return dissoc('region', transformedfeature);
};

const transformFeature = (feature) => {
  let transformedfeature = dissoc('expirationCriteria', feature);
  if (has('expirationCriteria', feature)) {
    const expirationCriteria = prop('expirationCriteria', feature);
    transformedfeature = dissoc('expirationCriteria', feature);
    transformedfeature.calendarExpiration = transformCalendarExpiration(expirationCriteria);
    transformedfeature.engineHours = transformEngineHours(expirationCriteria);
  }
  const overrides = pathOr('', ['included', 'regionalOverrides'], transformedfeature);
  transformedfeature = applyOverrides(transformedfeature, overrides);
  return dissoc('included', transformedfeature);
};

export const transformEngineHoursResponse = (payload) => prop('engineHours', payload);
export const transformVINResponse = (payload) => prop('vin', payload);

export const transformVendor = (serialNumber, vendor) => {
  const vendors = filter(propEq('value', vendor), VENDORS);
  const getFields = pick(['value', 'label', 'description', 'image', 'title']);

  if (length(vendors) === 1) return getFields(head(vendors));

  return getFields(find(({ match }) => match.test(serialNumber), vendors));
};

export const transformCanUserEnroll = (payload) => path(['meta', 'message'], payload);

const attachOrderToDeviceFeature = (deviceFeature, orders, features) => {
  const getOrder = (id) => find(propEq('id', id), orders);
  const getFeature = (id) => find(propEq('id', id), features);
  const transformedDeviceFeature = clone(deviceFeature);
  transformedDeviceFeature.order = getOrder(deviceFeature.links.order);
  transformedDeviceFeature.feature = getFeature(deviceFeature.links.feature);
  return transformedDeviceFeature;
};

export const transformACMPurchases = (purchases) => {
  const cs310Purchases = filter(pathEq(['feature', 'vendor'], 'CS310'), purchases);
  const acmPurchases = filter(pathEq(['feature', 'vendor'], 'AGCO'), purchases);
  const activeACMPurchase = find(propEq('active', true), acmPurchases);
  const items = activeACMPurchase ? prepend(activeACMPurchase, cs310Purchases) : cs310Purchases;
  const status = prop('activationStatus', activeACMPurchase);
  const hasActivatedSubscription = equals(DEVICE_FEATURE_STATUS.activated, status);
  const hasAvailableSubscription = equals('Available', status);
  return { items, hasActivatedSubscription, hasAvailableSubscription };
};

export const groupSubscriptions = (subscriptions, purchase) => {
  const sortItems = sort((a, b) => a - b);

  const transformToSelect = (item) => ({ label: toString(item), value: item });

  const findSubscription = (item) => find(propEq('serviceLevel', parseInt(item, 10)), subscriptions);

  const findNumberOfYears = pipe(filter(pipe(
    path(['calendarExpiration', 'unit']),
    equals('year')
  )), map(path(['calendarExpiration', 'value'])), uniq, sortItems, map(transformToSelect));

  const findEngineHours = pipe(pluck('engineHours'), uniq, sortItems, map(transformToSelect));

  const subscriptionsByServiceLevel = groupBy(prop('serviceLevel'), subscriptions);

  const groupedSubscriptions = map((serviceLevel) => ({
    ...findSubscription(serviceLevel),
    selectableEngineHours: findEngineHours(prop(serviceLevel, subscriptionsByServiceLevel)),
    selectableNumberOfYears: findNumberOfYears(prop(serviceLevel, subscriptionsByServiceLevel)),
  }), keys(subscriptionsByServiceLevel));

  const subscriptionsWithoutPreviousLevels = reject(({ serviceLevel }) => serviceLevel < pathOr(0, ['feature', 'serviceLevel'], purchase), groupedSubscriptions);

  return {
    subscriptions: subscriptionsWithoutPreviousLevels,
  };
};

const disableAlreadyPurchasedUpgrades = (upgrade, purchases) => {
  const transformedUpgrade = clone(upgrade);
  const hasPurchase = any(pathEq(['links', 'feature'], upgrade.id), purchases);
  transformedUpgrade.available = !hasPurchase && transformedUpgrade.available;
  return transformedUpgrade;
};

export const transformFeatures = (data, vendor) => {
  const features = map(transformFeature, prop('features', data));
  const getFeature = (available) => (id) => merge(find(propEq('id', id), features), { available });
  const featuresAvailable = map(getFeature(true), path(['links', 'featuresAvailable'], data));
  const featuresUnavailable = map(getFeature(false), path(['links', 'featuresUnavailable'], data));
  const allFeatures = concat(featuresAvailable, featuresUnavailable);
  const transformedFeatures = filter(propEq('status', 'Active'), allFeatures);
  const deviceFeatures = map(transformDeviceFeature, propOr([], 'deviceFeatures', data));
  const orders = map(transformOrder, propOr([], 'orders', data));
  const purchases = map((deviceFeature) => attachOrderToDeviceFeature(deviceFeature, orders, allFeatures), deviceFeatures);
  const upgrades = filter(propEq('kind', ORDER_TYPES.upgrade), transformedFeatures);
  const activePurchases = filter(propEq('active', true), purchases);
  const vin = prop('vin', data);
  const NT03AGCOserialNumber = prop('serialNumber', data);
  if (vendor === 'NT03' || vendor === 'AGCO') {
    return {
      vin,
      NT03AGCOserialNumber,
      subscriptions: filter(propEq('kind', ORDER_TYPES.subscription), transformedFeatures),
      upgrades: map((upgrade) => disableAlreadyPurchasedUpgrades(upgrade, purchases), upgrades),
      purchases: activePurchases,
    };
  }
  return {
    subscriptions: filter(propEq('kind', ORDER_TYPES.subscription), transformedFeatures),
    upgrades: map((upgrade) => disableAlreadyPurchasedUpgrades(upgrade, purchases), upgrades),
    purchases: activePurchases,
  };
};
