import { expect as expectChai } from 'chai';
import {
  transformFeaturesInfoFixture,
  groupBySubscriptionFixture,
  engineHoursFixture,
  transformACMPurchasesWithACMActivatedAndCS310,
  transformACMPurchasesWithACMAvailableAndCS310,
  transformACMPurchasesOnlyCS310,
  canUserEnrollTrue,
  canUserEnrollFalse,
  vinFixture,
} from './fixture';
import { transformFeatures, groupSubscriptions, transformEngineHoursResponse, transformACMPurchases, transformCanUserEnroll, transformVINResponse } from '../index';

describe('featuresInfoTransformers', () => {
  describe('transformFeaturesInfo', () => {
    it('should transform API output into readable Object', () => {
      const result = transformFeatures(transformFeaturesInfoFixture.input);
      expectChai(result.subscriptions).to.deep.equal(transformFeaturesInfoFixture.output.subscriptions);
      expectChai(result.upgrades).to.deep.equal(transformFeaturesInfoFixture.output.upgrades);
      expect(result.purchases[0]).toEqual(transformFeaturesInfoFixture.output.purchases[0]);
      expect(result).toEqual(transformFeaturesInfoFixture.output);
      expectChai(result.purchases[1]).to.deep.equal(transformFeaturesInfoFixture.output.purchases[1]);
    });
  });

  describe('groupSubscriptions', () => {
    it('Should group subscriptions to display in the ACM product page', () => {
      const result = groupSubscriptions(groupBySubscriptionFixture);
      expectChai(result).to.be.an('object');
      expectChai(result.subscriptions).to.have.lengthOf(3);
      expectChai(result.subscriptions[0].selectableNumberOfYears).to.have.lengthOf(1);
      expectChai(result.subscriptions[1].selectableNumberOfYears).to.have.lengthOf(1);
      expectChai(result.subscriptions[2].selectableNumberOfYears).to.have.lengthOf(1);
      expectChai(result.subscriptions[0].selectableEngineHours).to.have.lengthOf(5);
      expectChai(result.subscriptions[1].selectableEngineHours).to.have.lengthOf(4);
      expectChai(result.subscriptions[2].selectableEngineHours).to.have.lengthOf(5);
      expectChai(result.subscriptions[0].selectableEngineHours[0]).to.deep.equal({ label: '300', value: 300 });
      expectChai(result.subscriptions[0].selectableNumberOfYears[0]).to.deep.equal({ label: '1', value: 1 });
    });
  });

  describe('transformEngineHours', () => {
    it('Should parse engine hours', () => {
      const result = transformEngineHoursResponse(engineHoursFixture);
      expectChai(result).to.equal(1200);
    });
  });

  describe('transformACMPurchases with ACM activated and CS310', () => {
    it('Should identify last active acm subscription and show the activated one', () => {
      const result = transformACMPurchases(transformACMPurchasesWithACMActivatedAndCS310);
      expectChai(result.hasActivatedSubscription).to.equal(true);
      expectChai(result.purchases).to.equal(transformACMPurchasesWithACMActivatedAndCS310.purchases);
    });
  });

  describe('transformACMPurchases with ACM available and CS310', () => {
    it('Should identify last active acm subscription and show the available one because no one is activated', () => {
      const result = transformACMPurchases(transformACMPurchasesWithACMAvailableAndCS310);
      expectChai(result.hasActivatedSubscription).to.equal(false);
      expectChai(result.purchases).to.equal(transformACMPurchasesWithACMAvailableAndCS310.purchases);
    });
  });

  describe('transformACMPurchases with no ACM available and only CS310', () => {
    it('Should show only CS310 purchase', () => {
      const result = transformACMPurchases(transformACMPurchasesOnlyCS310);
      expectChai(result.hasActivatedSubscription).to.equal(false);
      expectChai(result.purchases).to.equal(transformACMPurchasesOnlyCS310.purchases);
    });
  });
  describe('transformACMPurchases with ACM activated and CS310', () => {
    it('Should identify last active acm subscription and show the activated one', () => {
      const result = transformACMPurchases(transformACMPurchasesWithACMActivatedAndCS310);
      expectChai(result.hasActivatedSubscription).to.equal(true);
      expectChai(result.purchases).to.equal(transformACMPurchasesWithACMActivatedAndCS310.purchases);
    });
  });

  describe('transformACMPurchases with ACM available and CS310', () => {
    it('Should identify last active acm subscription and show the available one because no one is activated', () => {
      const result = transformACMPurchases(transformACMPurchasesWithACMAvailableAndCS310);
      expectChai(result.hasActivatedSubscription).to.equal(false);
      expectChai(result.purchases).to.equal(transformACMPurchasesWithACMAvailableAndCS310.purchases);
    });
  });

  describe('transformCanUserEnroll', () => {
    it('Should return true when API response contains true', () => {
      const result = transformCanUserEnroll(canUserEnrollTrue);
      expectChai(result).to.equal(true);
    });
    it('Should return false when API response contains true', () => {
      const result = transformCanUserEnroll(canUserEnrollFalse);
      expectChai(result).to.equal(false);
    });
  });

  describe('transforVIN', () => {
    it('Should parse vin', () => {
      const result = transformVINResponse(vinFixture);
      expectChai(result).to.equal('AGC2198473');
    });
  });
});
