import { format } from 'date-fns';
import { contains, map, propEq, path, mapObjIndexed, find, filter, propOr, prop, equals, has, assoc, pick, merge, values } from 'ramda';

const AGCO_VIN_VENDORS = ['CS310', 'AGCO'];
const isACM = (vendor) => contains(vendor, AGCO_VIN_VENDORS);
const replaceAcmVendor = (vendor) => isACM(vendor) ? 'AGCO' : vendor;

export const transformOrders = (data) => {
  const features = path(['linked', 'features'], data);
  const findFeature = (featureId) => find(propEq('id', featureId), features) || {};
  const orderWithCreatedOn = filter(has('createdOn'), prop('orders', data));

  return map((order) => {
    const featureId = path(['links', 'feature'], order);
    const feature = findFeature(featureId);

    return {
      orderDate: format(prop('createdOn', order), 'MM/DD/YYYY'),
      orderReference: prop('regionalRefNumber', order),
      customerReference: prop('customerIndirectReference', order),
      serialNumber: prop('equipmentSerialNumber', order),
      vendor: propOr('', 'vendor', feature).replace(' ', ''),
      product: prop('description', feature),
      status: prop('status', order),
      realVendor: replaceAcmVendor(propOr('', 'vendor', feature).replace(' ', '')),
    };
  }, orderWithCreatedOn);
};

export const transformOrdersCount = (response) => parseInt(path(['headers', 'orders-count'], response), 10);

export const transformOrdersCountFilters = ({ customerReference }) => customerReference ? { customerIndirectReference: `${customerReference}*` } : {};

export const filterUpgrades = filter(propEq('kind', 'UPGRADE'));
export const filterSubscriptions = filter(propEq('kind', 'SUBSCRIPTION'));
export const transformCartToCheckout = (cart) => ({
  upgrades: filterUpgrades(cart),
  subscriptions: filterSubscriptions(cart),
});

export const transformOrdersFilters = ({
  offset,
  limit,
  customerReference,
}) => {
  const filters = {
    offset: (offset * limit),
    include: 'feature',
  };

  if (customerReference) {
    filters.customerIndirectReference = `${customerReference}*`;
  }

  return filters;
};

const getVendorAttributes = ({ vendor, metaTags }) => {
  const isTrimble = equals(vendor, 'Trimble') && equals(metaTags, 'VRS');
  const isDeviceInsight = equals(vendor, 'DeviceInsight');

  if (isTrimble) {
    return ['POS_ID', 'POS_PASSWORD'];
  } else if (isDeviceInsight) {
    return ['CUSTOMER_NAME', 'CUSTOMER_FIRSTNAME', 'CUSTOMER_LASTNAME', 'FIS_USERNAME', 'FIS_PASSWORD', 'FIS_PASSWORD_CONFIRM', 'CUSTOMER_EMAIL', 'CUSTOMER_EMAIL_CONFIRM', 'CUSTOMER_PREFERRED_LANGUAGE'];
  }

  return [];
};

const buildExtraAttributes = ({ metaTags, extraAttributes, vendor }) => {
  const attributes = pick(getVendorAttributes({ vendor, metaTags }), extraAttributes);

  return {
    extraAttributes: values(mapObjIndexed((value, name) => ({ name, value }), attributes)),
  };
};

const buildDefaultPayload = ({
  id,
  immediateActivation,
  activationDate,
  equipmentSerialNumber,
  machineLocation,
  customerReference,
}) => {
  const payload = {
    equipmentSerialNumber,
    immediateActivation,
    activationLocation: machineLocation,
    customerIndirectReference: customerReference,
    links: {
      feature: id,
    },
  };

  return !immediateActivation ? assoc('activationSchedule', activationDate, payload) : payload;
};

export const transformOrdersToCheckout = ({ cart, extraAttributes, vendor }) => map(({ metaTags, ...item }) => merge(buildDefaultPayload(item), buildExtraAttributes({ metaTags, extraAttributes, vendor })), cart);
