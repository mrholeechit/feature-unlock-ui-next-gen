import axios from 'axios';
import axiosRetry from 'axios-retry';
import { pathOr } from 'ramda';
import { clearToken } from 'containers/App/actions';
import { API } from '../config';

let store = {};

const UNAUTHORIZED = 401;
const SERVER_ERROR = 500;

if (process.env.NODE_ENV !== 'test') {
  store = require('app').store; // eslint-disable-line
  store.subscribe(() => {
    const {
      access_token: token,
      token_type: type,
    } = pathOr({}, ['app', 'token'], store.getState().toJS());

    axios.defaults.headers.common.Authorization = `${type} ${token}`;
  });
}

axiosRetry(axios, { retries: API.retries });

axios.interceptors.response.use((response) => {
  if (response.config.method === 'head') {
    return response;
  }
  return response.data;
}, (error) => {
  if (error.response.status === UNAUTHORIZED) {
    if (process.env.NODE_ENV !== 'test') {
      store.dispatch(clearToken());
    }
  } else if (error.response.status >= SERVER_ERROR) {
    console.error('SERVER ERROR!'); // eslint-disable-line
  }

  return Promise.reject(error);
});

export default axios;
