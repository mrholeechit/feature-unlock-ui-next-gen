import { head, keys, path } from 'ramda';
import request from '../request';
import { IAM_API_URL, FEATURE_UNLOCK_URL } from '../../config';

export default {
  get() {
    return Promise.all([
      request.get(`${IAM_API_URL}/whoami`),
      request.get(`${FEATURE_UNLOCK_URL}/dealer`),
    ]).then(([user, dealer]) => {
      const userType = head(keys(user));

      return {
        userType,
        roles: path([userType, 0, 'links', 'roles'], user),
        ...path([userType, 0], user),
        ...dealer,
      };
    });
  },
};
