import request from '../request';
import { FEATURE_UNLOCK_URL } from '../../config';

export default {
  get() {
    return request.get(`${FEATURE_UNLOCK_URL}/canUserEnroll`, {});
  },
};

