import request from '../request';
import { FEATURE_UNLOCK_URL } from '../../config';

export default {
  info({
    serialNumber,
    ...query
  }) {
    return request.get(`${FEATURE_UNLOCK_URL}/getFeaturesInfo`, {
      params: {
        equipmentSerialNumber: serialNumber,
        ...query,
      },
    });
  },
  actions({
    id,
  }) {
    return request.get(`${FEATURE_UNLOCK_URL}/deviceFeatures/${id}/options`);
  },
  get({
    limit = 50,
    sort = '-sortOrder',
    ...query
  }) {
    return request.get(`${FEATURE_UNLOCK_URL}/features`, {
      params: {
        limit,
        sort,
        ...query,
      },
    });
  },
  head({
    ...query
  }) {
    return request.head(`${FEATURE_UNLOCK_URL}/features`, {
      params: {
        ...query,
      },
    });
  },
  post(data) {
    return request.post(`${FEATURE_UNLOCK_URL}/features`, data);
  },
  put(id, data) {
    return request.put(`${FEATURE_UNLOCK_URL}/featuresUpdate/${id}`, data);
  },
};
