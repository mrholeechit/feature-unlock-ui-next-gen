import request from '../request';
import { FEATURE_UNLOCK_URL } from '../../config';

export default {
  existCustomerReference({ customerReference, dealerCode }) {
    return request.head(`${FEATURE_UNLOCK_URL}/eame/customerReference/${customerReference}`, {
      params: {
        dealerCode,
      },
    });
  },
};
