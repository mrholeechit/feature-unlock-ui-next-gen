import request from '../request';
import { FEATURE_UNLOCK_URL } from '../../config';

export default {
  get({
    limit = 20,
    sort = '-createdOn',
    createdOn,
    ...query
  }) {
    return request.get(`${FEATURE_UNLOCK_URL}/orders${createdOn ? `?${createdOn}` : ''}`, {
      params: {
        include: 'feature',
        limit,
        sort,
        ...query,
      },
    });
  },
  post(orders) {
    return request.post(`${FEATURE_UNLOCK_URL}/orders`, orders);
  },
  head({
    createdOn,
    ...params
  }) {
    return request.head(`${FEATURE_UNLOCK_URL}/orders${createdOn ? `?${createdOn}` : ''}`, {
      params,
    });
  },
  getEvents(orderId) {
    return request.get(`${FEATURE_UNLOCK_URL}/orderevents?order=${orderId}`);
  },
  resend(order) {
    return request.post(`${FEATURE_UNLOCK_URL}/orders/${order}/resend`);
  },
  reports(params) {
    return request.get(`${FEATURE_UNLOCK_URL}/dumps/orders`, {
      responseType: 'arraybuffer',
      params,
    });
  },
};
