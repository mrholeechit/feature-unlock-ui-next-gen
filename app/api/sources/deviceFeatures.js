import request from '../request';
import { FEATURE_UNLOCK_URL } from '../../config';

export default {
  transfer({ id, from, to }) {
    return request.post(`${FEATURE_UNLOCK_URL}/deviceFeatures/${id}/transfer`, {
      data: {},
      included: [
        {
          type: 'tractors',
          attributes: {
            vinOrSerial: to,
            name: '',
            model: '',
            phoneNumber: '',
            password: '',
          },
        },
      ],
    }, {
      params: {
        oldVin: from,
        newVin: to,
      },
    });
  },
};
