import jsonp from 'jsonp';
import request from '../request';

export default {
  getHeaders() {
    return new Promise((resolve, reject) => jsonp('https://ajaxhttpheaders3.appspot.com', (err, data) => {
      if (err) reject(err);

      resolve(data);
    }));
  },
  call({ method = 'get', resource }) {
    return request[method](resource, {});
  },
};
