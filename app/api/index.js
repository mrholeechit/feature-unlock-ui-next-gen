import request from './request';
import orders from './sources/orders';
import features from './sources/features';
import engineHours from './sources/engineHours';
import deviceFeatures from './sources/deviceFeatures';
import vin from './sources/vin';
import canUserEnroll from './sources/canUserEnroll';
import users from './sources/users';
import eame from './sources/eame';
import utils from './sources/utils';

export default {
  request,
  orders,
  features,
  engineHours,
  vin,
  canUserEnroll,
  eame,
  users,
  deviceFeatures,
  utils,
};
