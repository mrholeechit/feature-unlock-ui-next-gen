import 'sanitize.css/sanitize.css';
import 'react-table/react-table.css';
import 'react-select/dist/react-select.css';
import theme from 'utils/styles';
import { injectGlobal } from 'styled-components';
import 'flatpickr/dist/themes/airbnb.css';
import 'react-accessible-accordion/dist/react-accessible-accordion.css';
import 'rc-tooltip/assets/bootstrap_white.css';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
    color: ${theme.color.mineShaft};
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    padding-top: 54px;
  }

  body.-font-loaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.ReactModal__Body--open {
    overflow: hidden;
  }

  .ReactModal__Overlay::after {
    display: inline-block;
    height: 100%;
    margin-left: -.05em;
    content: "";
    vertical-align: middle;
  }

  .ReactModal__Overlay {
    opacity: 0;
  }

  .ReactModal__Overlay--after-open {
    opacity: 1;
    transition: opacity ${theme.transition};
  }

  .ReactModal__Overlay--before-close {
    opacity: 0;
  }

  #app {
    min-height: 100%;
    min-width: 100%;
  }

  a {
    text-decoration: none;
  }

  button {
    padding: 0;
    cursor: pointer;
  }

  ::-webkit-input-placeholder {
    color: ${theme.color.mineShaft};
    opacity: 0.4;
  }

  :-moz-placeholder {
    color: ${theme.color.mineShaft};
    opacity: 0.4;
  }

  ::-moz-placeholder {
    color: ${theme.color.mineShaft};
    opacity: 0.4;
  }

  :-ms-input-placeholder {
    color: ${theme.color.mineShaft};
    opacity: 0.4;
  }

  ::-ms-input-placeholder {
    color: ${theme.color.mineShaft};
    opacity: 0.4;
  }

  span.flatpickr-day {
    display: block;
    flex: 1 0 auto;
  }

  .rc-tooltip {
    opacity: 1;
    padding: 0;

    .rc-tooltip-inner {
      padding: 0;
      border: none;
    }

    .rc-tooltip-arrow {
      border-left-color: #fff;
    }
  }

  .input {
    border-radius: ${theme.border.radius};
    background: #fff;
    padding: 8px;
    font-size: ${theme.font.base};
    border: ${theme.border.width} ${theme.color.alto} solid;
    transition: border-color ${theme.transition};
    height: 36px;

    &.-error {
      border-color: ${theme.color.monza};
      border-width: 2px;
    }

    &.-autoHeight {
      height: auto;
    }
  }

  .rc-tooltip {
    .rc-tooltip-inner {
      border-radius: 7.5px;
    }

    &.-error {
      .overlay {
        background-color: ${theme.color.monza};
        color: #FFF;
        border-radius: 10px;
        padding: 10px;
        max-width: 200px;
      }

      .body {
        font-size: ${theme.font.tiny} !important;
        font-weight: ${theme.font.bold}
      }

      &.rc-tooltip-placement-top .rc-tooltip-arrow,
      &.rc-tooltip-placement-topLeft .rc-tooltip-arrow,
      &.rc-tooltip-placement-topRight .rc-tooltip-arrow  {
        border-top-color: ${theme.color.monza};
      }

      &.rc-tooltip-placement-right .rc-tooltip-arrow,
      &.rc-tooltip-placement-rightTop .rc-tooltip-arrow,
      &.rc-tooltip-placement-rightBottom .rc-tooltip-arrow {
        border-right-color: ${theme.color.monza};
      }

      &.rc-tooltip-placement-left .rc-tooltip-arrow,
      &.rc-tooltip-placement-leftTop .rc-tooltip-arrow,
      &.rc-tooltip-placement-leftBottom .rc-tooltip-arrow {
        border-left-color: ${theme.color.monza};
      }

      &.rc-tooltip-placement-bottom .rc-tooltip-arrow,
      &.rc-tooltip-placement-bottomLeft .rc-tooltip-arrow,
      &.rc-tooltip-placement-bottomRight .rc-tooltip-arrow {
        border-bottom-color: ${theme.color.monza};
      }
    }
  }
`;
