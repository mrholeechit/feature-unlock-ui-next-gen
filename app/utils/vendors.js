import { filter } from 'ramda';
import { VENDORS } from 'config';

export const search = (serialNumber) => {
  const filterVendor = filter(({
    match,
    customValidation = null,
  }) => match.test(serialNumber) && (!customValidation || customValidation()));

  return filterVendor(VENDORS);
};
