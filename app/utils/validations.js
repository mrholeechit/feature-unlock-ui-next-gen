/* eslint-disable */
import Yup from 'yup';
import { length, test, path, reduce, pipe, replace, pathOr } from 'ramda';
import { NA_SA_CUSTOMER_REFERENCE_LENGTH, EAME_CUSTOMER_REFERENCE_LENGTH } from 'config';
import API from 'api';
import isString from 'lodash.isstring';
import translate from './translate';

export const RULES = {
  LENGTH: 'length',
  EXIST_CUSTOMER_REFERENCE: 'existCustomerReference',
  ESPECIAL_CHARS: 'especialChars',
  SPACE: 'space',
};

const MESSAGES = {
  [RULES.LENGTH]: translate('PRODUCT_PAGE.CUSTOMER_REFERENCE_VALIDATION_LENGTH'),
  [RULES.EXIST_CUSTOMER_REFERENCE]: translate('PRODUCT_PAGE.CUSTOMER_REFERENCE_VALIDATION_EXIST'),
  [RULES.ESPECIAL_CHARS]: translate('PRODUCT_PAGE.CUSTOMER_REFERENCE_VALIDATION_INVALID'),
  [RULES.SPACE]: translate('PRODUCT_PAGE.CUSTOMER_REFERENCE_VALIDATION_SPACE'),
};

const VALIDATIONS = {
  EAME: {
    [RULES.LENGTH]: ({ customerReference }) => new Promise((resolve, reject) => {
      return length(customerReference) <= EAME_CUSTOMER_REFERENCE_LENGTH ? resolve() : reject(MESSAGES[RULES.LENGTH]);
    }),
    [RULES.ESPECIAL_CHARS]: ({ customerReference }) => new Promise((resolve, reject) => {
      return test(/(?:(^[A-Z0-9])|(^$))/gi, customerReference) ? resolve() : reject(MESSAGES[RULES.ESPECIAL_CHARS]);
    }),
    [RULES.EXIST_CUSTOMER_REFERENCE]: ({ customerReference, dealerCode }) => new Promise((resolve, reject) => {
      return API.eame.existCustomerReference({ customerReference, dealerCode })
        .then(() => reject(MESSAGES[RULES.EXIST_CUSTOMER_REFERENCE]))
        .catch(() => resolve());
    }),
    [RULES.SPACE]: ({ customerReference }) => new Promise((resolve, reject) => {
      return test(/^$|^\S+(?: \S+)*$/, customerReference) ? resolve() : reject(MESSAGES[RULES.SPACE]);
    }),
  },
  NA: {
    [RULES.LENGTH]: ({ customerReference }) => new Promise((resolve, reject) => {
      return length(customerReference) <= NA_SA_CUSTOMER_REFERENCE_LENGTH ? resolve() : reject(MESSAGES[RULES.LENGTH]);
    }),
    [RULES.SPACE]: ({ customerReference }) => new Promise((resolve, reject) => {
      return test(/^$|^\S+(?: \S+)*$/, customerReference) ? resolve() : reject(MESSAGES[RULES.SPACE]);
    }),
  },
  SA: {
    [RULES.LENGTH]: ({ customerReference }) => new Promise((resolve, reject) => {
      return length(customerReference) <= NA_SA_CUSTOMER_REFERENCE_LENGTH ? resolve() : reject(MESSAGES[RULES.LENGTH]);
    }),
    [RULES.SPACE]: ({ customerReference }) => new Promise((resolve, reject) => {
      return test(/^$|^\S+(?: \S+)*$/, customerReference) ? resolve() : reject(MESSAGES[RULES.SPACE]);
    }),
  },
};

export default (rules, region, data = {}) => new Promise(async (resolve, reject) => {
  try {
    if (isString(rules)) {
      await pathOr(() => Promise.resolve(), [region, rules], VALIDATIONS)(data);
    } else {
      for (let key in rules) {
        await pathOr(() => Promise.resolve(), [region, rules[key]], VALIDATIONS)(data);
      }
    }

    resolve();
  } catch (e) {
    reject(new Error(e));
  }
});

const normalizePath = pipe(
  replace('[', '.'),
  replace(']', '')
);

Yup.addMethod(Yup.string, 'equalTo', function (ref, msg) {
  return this.test({
    name: 'equalTo',
    exclusive: false,
    message: msg || '${path} must be the same as ${reference}',
    params: {
      reference: ref.path
    },
    test: function(value) {
      return value === this.resolve(ref);
    }
  });
});

export const validate = (schema, values) => schema.validate(values, {
  abortEarly: false,
}).catch(({ inner }) => Promise.reject(reduce((acc, { path, message }) => ({
  ...acc,
  [normalizePath(path)]: message,
}), {}, inner)));

