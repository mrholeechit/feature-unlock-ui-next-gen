import { sortBy, prop } from 'ramda';

export const VENDORS = {
  agco: 'AGCO',
  novatel: 'NovAtel',
  trimble: 'Trimble',
  cs310: 'CS310',
  nt03: 'NT03',
  deviceInsight: 'DeviceInsight',
  topcon: 'Topcon',
};

export const DEVICE_FEATURE_STATUS = {
  activated: 'Activated',
  delivered: 'Delivered',
};

export const ORDER_STATUS = {
  processing: 'Processing',
  completed: 'Completed',
  failed: 'Failed',
};

export const ORDER_TYPES = {
  subscription: 'SUBSCRIPTION',
  upgrade: 'UPGRADE',
};

export const DATE_FORMAT = 'MM/DD/YYYY';
export const TIME_FORMAT = 'HH:mm:ss';
export const DATE_TIME_FORMAT = `${DATE_FORMAT} ${TIME_FORMAT}`;

export const LANGUAGES = sortBy(prop('label'), [
  { label: 'English', value: 'en' },
  { label: 'Dansk', value: 'da' },
  { label: 'Deutsch', value: 'de' },
  { label: 'Español', value: 'es' },
  { label: 'Suomi', value: 'fi' },
  { label: 'Français', value: 'fr' },
  { label: 'Italiano', value: 'it' },
  { label: 'Norsk', value: 'nb' },
  { label: 'Nederlands', value: 'nl' },
  { label: 'Język Polski', value: 'pl' },
  { label: 'Português', value: 'pt' },
  { label: 'Русский', value: 'ru' },
  { label: 'Svenska', value: 'sv' },
  { label: 'Türkçe', value: 'tr' },
]);
