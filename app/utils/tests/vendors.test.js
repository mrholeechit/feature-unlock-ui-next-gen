import { search } from 'utils/vendors';
import { find, forEachObjIndexed, propEq, pipe, prop, forEach } from 'ramda';

const vendors = {
  AGCO: [
    '123456',
    'AGCbsdr41s15w9e81',
  ],
  Topcon: [
    '617-60050',
    '1223-973qU056',
  ],
  DeviceInsight: [
    '45abf4894',
    '123 12 1254',
    'TPa4f5e9w6',
    'AGCs6e9q4a5s6d5e8',
  ],
  Trimble: [
    '55a1s5e9q5',
    '5655575557',
    '57a1s5e9q5',
    '7sde15',
    '2abc15',
  ],
  NovAtel: [
    '@FN5s1e5_9d',
    'BFN5s1e5_9d59',
  ],
};

describe('vendors', () => {
  describe('search', () => {
    forEachObjIndexed((codes, expected) => {
      forEach((code) => {
        it(`should return ${expected} when ${code}`, () => {
          const actual = pipe(
            find(propEq('value', expected)),
            prop('value'),
          )(search(code));

          expect(actual).toEqual(expected);
        });
      }, codes);
    }, vendors);
  });
});
