import { convertInputToField } from 'utils/form';
import { expect } from 'chai';

describe('form', () => {
  it('should convert input event to field format', () => {
    expect({
      name: 'name',
      value: 'value',
    }).to.deep.equal(convertInputToField({
      target: {
        name: 'name',
        value: 'value',
      },
    }));
  });
});
