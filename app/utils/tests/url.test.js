import { parseHash } from 'utils/url';
import { expect } from 'chai';

describe('parse URL', () => {
  it('should parse hash query string', () => {
    expect({
      access_token: 'access_token',
      expires_in: 'expires_in',
      token_type: 'token_type',
    }).to.deep.equal(parseHash('http://localhost:3000#access_token=access_token&expires_in=expires_in&token_type=token_type'));
  });

  it('should return empty object', () => {
    expect({}).to.deep.equal(parseHash(''));
  });
});
