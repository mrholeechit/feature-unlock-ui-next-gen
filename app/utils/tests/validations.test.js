import validate, { RULES } from 'utils/validations';
import chaiAsPromised from 'chai-as-promised';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { FEATURE_UNLOCK_URL } from 'config';
import chai, { expect } from 'chai';

chai.use(chaiAsPromised);

describe('validate customer reference', () => {
  let mock;

  beforeEach(() => {
    mock = new MockAdapter(axios);
  });

  it('should reject if NA and bigger than 8', () => expect(validate(RULES.LENGTH, 'NA', { customerReference: '123456789' })).to.be.rejected);

  it('should resolve if NA and shorter than 8', () => expect(validate(RULES.LENGTH, 'NA', { customerReference: '1234567' })).to.be.fulfilled);

  it('should resolve if NA and equal to 8', () => expect(validate(RULES.LENGTH, 'NA', { customerReference: '12345678' })).to.be.fulfilled);

  it('should reject if NA and starts with space', () => expect(validate(RULES.SPACE, 'NA', { customerReference: ' 12345678' })).to.be.rejected);

  it('should reject if NA and ends with space', () => expect(validate(RULES.SPACE, 'NA', { customerReference: '12345678 ' })).to.be.rejected);

  it('should reject if NA and have space', () => expect(validate(RULES.SPACE, 'NA', { customerReference: '1234 5678' })).to.be.fulfilled);

  it('should reject if SA and bigger than 8', () => expect(validate(RULES.LENGTH, 'SA', { customerReference: '123456789' })).to.be.rejected);

  it('should resolve if SA and shorter than 8', () => expect(validate(RULES.LENGTH, 'SA', { customerReference: '1234567' })).to.be.fulfilled);

  it('should resolve if SA and equal to 8', () => expect(validate(RULES.LENGTH, 'SA', { customerReference: '12345678' })).to.be.fulfilled);

  it('should reject if SA and starts with space', () => expect(validate(RULES.SPACE, 'SA', { customerReference: ' 12345678' })).to.be.rejected);

  it('should reject if SA and ends with space', () => expect(validate(RULES.SPACE, 'SA', { customerReference: '12345678 ' })).to.be.rejected);

  it('should reject if SA and have space', () => expect(validate(RULES.SPACE, 'SA', { customerReference: '1234 5678' })).to.be.fulfilled);

  it('should reject if EAME and bigger than 17', () => expect(validate(RULES.LENGTH, 'EAME', { customerReference: '123456789012345678' })).to.be.rejected);

  it('should resolve if EAME and shorter than 17', () => expect(validate(RULES.LENGTH, 'EAME', { customerReference: '1234567890123456' })).to.be.fulfilled);

  it('should resolve if EAME and equal to 17', () => expect(validate(RULES.LENGTH, 'EAME', { customerReference: '12345678901234567' })).to.be.fulfilled);

  it('should reject if EAME and first is especial char', () => expect(validate(RULES.ESPECIAL_CHARS, 'EAME', { customerReference: '@55124@s8787' })).to.be.rejected);

  it('should resolve if EAME and have especial chars', () => expect(validate(RULES.ESPECIAL_CHARS, 'EAME', { customerReference: 'as$d@3as' })).to.be.fulfilled);

  it('should reject if EAME and starts with space', () => expect(validate(RULES.SPACE, 'EAME', { customerReference: ' 12345678' })).to.be.rejected);

  it('should reject if EAME and ends with space', () => expect(validate(RULES.SPACE, 'EAME', { customerReference: '12345678 ' })).to.be.rejected);

  it('should reject if EAME and have space', () => expect(validate(RULES.SPACE, 'EAME', { customerReference: '1234 5678' })).to.be.fulfilled);

  it('should reject if EAME and exist', () => {
    mock.onHead(`${FEATURE_UNLOCK_URL}/eame/customerReference/12345678901234567`, {
      params: {
        dealerCode: 123,
      },
    }).replyOnce(200);

    return expect(validate(RULES.EXIST_CUSTOMER_REFERENCE, 'EAME', { customerReference: '12345678901234567', dealerCode: 123 })).to.be.rejected;
  });

  it('should resolve if EAME and not exist', () => {
    mock.onHead(`${FEATURE_UNLOCK_URL}/eame/customerReference/12345678901234567`, {
      params: {
        dealerCode: 123,
      },
    }).replyOnce(404);

    return expect(validate(RULES.EXIST_CUSTOMER_REFERENCE, 'EAME', { customerReference: '12345678901234567', dealerCode: 123 })).to.be.fulfilled;
  });
});
