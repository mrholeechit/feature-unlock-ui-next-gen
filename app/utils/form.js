const isCheckbox = ({ type }) => type === 'checkbox';

export const convertInputToField = (event) => {
  const { target } = event;

  return {
    name: target.name,
    value: isCheckbox(target) ? target.checked : target.value,
  };
};
