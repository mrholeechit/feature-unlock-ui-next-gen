import { DEFAULT_LOCALE } from 'containers/LanguageProvider/constants';
import { pathOr, split, isEmpty, forEachObjIndexed, replace } from 'ramda';

const SEPARATOR = '.';

export default function translate(id, replaces = null) {
  const locale = window.locale || DEFAULT_LOCALE;
  const dictionary = require(`../translations/${locale}.js`).default; // eslint-disable-line
  let message = pathOr('', split(SEPARATOR, id.toUpperCase()), dictionary);

  if (!isEmpty(replaces)) {
    forEachObjIndexed((value, key) => {
      message = replace(new RegExp(`{${key}}`, 'i'), value, message);
    }, replaces);
  }

  return message;
}
