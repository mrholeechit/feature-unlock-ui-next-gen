import { parse } from 'qs';
import { split, prop } from 'ramda';

export const parseHash = (url) => parse(prop(1, split('#', url)));
