import { css } from 'styled-components';
import { ellipsis } from 'polished';

const theme = {
  color: {
    mineShaft: '#333333',
    monza: '#c1002e',
    sanJuan: '#38557d',
    concrete: '#f2f2f2',
    alto: '#D7D7D7',
    scorpion: '#565656',
    gallery: '#EAEAEA',
    viking: '#5bc0de',
    fern: '#5cb85c',
    silver: '#CCCCCC',
  },
  font: {
    fontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",

    tiny: '12px',
    small: '14px',
    base: '16px',
    medium: '18px',
    large: '24px',

    light: '300',
    normal: '400',
    semiBold: '600',
    bold: '700',
    extraBold: '800',
  },
  container: {
    width: '1366px',
    gutter: '20px',
  },
  border: {
    width: '1px',
    radius: '3px',
  },
  transition: '.2s ease-in-out',
  fullHeightNoHeader: 'calc(100vh - 54px)',
  breakpoints: {
    mobile: 0,
    tablet: 768,
    desktop: 1024,
  },
};

export default theme;

export const mixins = {
  ellipsis,
  center(orientation) {
    let top = '50%';
    let left = '50%';
    let translate = '-50%, -50%';

    if (orientation === 'vertical') {
      left = 'auto';
      translate = '0, -50%';
    } else if (orientation === 'horizontal') {
      top = 'auto';
      translate = '-50%, 0';
    }

    return `
      top: ${top};
      left: ${left};
      transform: translate(${translate});
    `;
  },
  breakpoint(name, breakpoints = theme.breakpoints) {
    const px = breakpoints[name];
    const ems = px / 16;

    if (px === 0) return (...args) => css(...args);

    return (...args) => css`@media (max-width: ${ems}em) {
      ${css(...args)}
    }`;
  },
};
