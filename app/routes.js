import { getAsyncInjectors } from 'utils/asyncInjectors';
import { equals, pathOr } from 'ramda';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

const isAGCO = ({ vendor }) => equals(vendor, 'AGCO');

const isAM50 = ({ serialNumber, state }) => {
  const region = pathOr('NA', ['app', 'dealer', 'regionCode'], state.toJS());
  const sn = serialNumber.replace(/[^A-Za-z0-9]/g, '');

  return (sn.length === 8 || sn.length === 7) && sn.substr(0, 3) === '617' && region === 'NA';
};

const isDeviceInsight = ({ vendor }) => equals(vendor, 'DeviceInsight');

const getProductPage = (params) => {
  if (isAGCO(params)) {
    return import('containers/ProductPage/ACM');
  } else if (isAM50(params)) {
    return import('containers/ProductPage/AM50');
  } else if (isDeviceInsight(params)) {
    return import('containers/ProductPage/DeviceInsight');
  }

  return import('containers/ProductPage/Generic');
};

export default function createRoutes(store) {
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return {
    getComponent(nextState, cb) {
      const importModules = Promise.all([
        import('containers/LanguageProvider/sagas'),
        import('containers/App/reducer'),
        import('containers/App/sagas'),
        import('containers/App'),
      ]);

      const renderRoute = loadModule(cb);

      importModules.then(([localeSagas, reducer, sagas, component]) => {
        injectReducer('app', reducer.default);
        injectSagas('localeSagas', localeSagas.default);
        injectSagas('appSagas', sagas.default);
        renderRoute(component);
      });

      importModules.catch(errorLoading);
    },
    childRoutes: [
      {
        path: '/',
        name: 'search',
        getComponent(nextState, cb) {
          const importModules = Promise.all([
            import('containers/SearchPage/reducer'),
            import('containers/SearchPage'),
          ]);

          const renderRoute = loadModule(cb);

          importModules.then(([reducer, component]) => {
            injectReducer('search', reducer.default);
            renderRoute(component);
          });

          importModules.catch(errorLoading);
        },
      },
      {
        path: '/product',
        name: 'product',
        getComponent(nextState, cb) {
          const {
            location: {
              query: {
                vendor,
                serialNumber,
              },
            },
          } = nextState;

          const importModules = [
            import('containers/ProductPage/reducer'),
            import('containers/ProductPage/sagas'),
            getProductPage({
              serialNumber,
              vendor,
              state: store.getState(),
            }),
          ];

          const renderRoute = loadModule(cb);

          Promise.all(importModules).then(([reducer, sagas, component]) => {
            injectReducer('product', reducer.default);
            injectSagas('productSagas', sagas.default);

            renderRoute(component);
          }).catch(errorLoading);
        },
      },
      {
        path: '/order-history',
        name: 'order-history',
        getComponent(nextState, cb) {
          const importModules = Promise.all([
            import('containers/OrderHistoryPage/reducer'),
            import('containers/OrderHistoryPage/sagas'),
            import('containers/OrderHistoryPage'),
          ]);

          const renderRoute = loadModule(cb);

          importModules.then(([reducer, sagas, component]) => {
            injectReducer('orderHistory', reducer.default);
            injectSagas('orderHistorySagas', sagas.default);

            renderRoute(component);
          });

          importModules.catch(errorLoading);
        },
      },
      {
        path: '/checkout',
        name: 'order-checkout',
        getComponent(nextState, cb) {
          const importModules = Promise.all([
            import('containers/CheckoutPage/reducer'),
            import('containers/CheckoutPage/sagas'),
            import('containers/CheckoutPage'),
          ]);

          const renderRoute = loadModule(cb);

          importModules.then(([reducer, sagas, component]) => {
            injectReducer('checkout', reducer.default);
            injectSagas('checkoutSagas', sagas.default);

            renderRoute(component);
          });

          importModules.catch(errorLoading);
        },
      },
      {
        path: '*',
        name: '404',
        getComponent(nextState, cb) {
          import('containers/404Page/index.js')
            .then(loadModule(cb))
            .catch(errorLoading);
        },
      },
    ],
  };
}
