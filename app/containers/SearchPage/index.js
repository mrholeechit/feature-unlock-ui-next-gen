import React from 'react';
import { object, func } from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';
import theme from 'utils/styles';
import translate from 'utils/translate';
import Modal from 'components/Modal';
import { VENDORS } from 'config';
import FUNBackground from 'images/feature-unlock-background.jpg';
import { queryChange, helpToggle } from './actions';
import { makeSelectSearch } from './selectors';
import SearchBox from './SearchBox/';
import HelpBox from './HelpBox';

const StyledSearch = styled.div`
  float: left;
  width: 100%;
  height: ${theme.fullHeightNoHeader};
  background: url(${FUNBackground}) center top no-repeat;
  background-size: cover;
  position: relative;

  &:after {
    content: '';
    position: absolute;
    bottom: 0;
    width: 100%;
    left: 0;
    box-shadow: 0 0 250px 250px rgba(0, 0, 0, 0.5);
  }
`;

export const SearchPage = ({
  data: {
    query,
    vendors,
    showHelp,
  },
  onChangeQuery,
  onClickVendor,
  onToggleHelp,
}) => (
  <StyledSearch>
    <SearchBox
      onChangeQuery={onChangeQuery}
      onClickVendor={onClickVendor}
      onHelpClick={onToggleHelp}
      query={query}
      vendors={vendors}
    />
    <Modal
      isOpen={showHelp}
      title={translate('NUMBER_FORMATS.NUMBER_FORMATS')}
      onClose={onToggleHelp}
    >
      <HelpBox vendors={VENDORS} />
    </Modal>
  </StyledSearch>
);

SearchPage.propTypes = {
  data: object.isRequired,
  onChangeQuery: func.isRequired,
  onClickVendor: func.isRequired,
  onToggleHelp: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  data: makeSelectSearch(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeQuery: (_, value) => dispatch(queryChange(value)),
    onClickVendor: ({ vendor: { value }, query }) => dispatch(push({
      pathname: 'product',
      query: {
        vendor: value,
        serialNumber: query,
      },
    })),
    onToggleHelp: () => dispatch(helpToggle()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
