import { QUERY_CHANGE, HELP_TOGGLE } from './constants';

export function queryChange(query) {
  return {
    type: QUERY_CHANGE,
    query,
  };
}

export function helpToggle() {
  return {
    type: HELP_TOGGLE,
  };
}
