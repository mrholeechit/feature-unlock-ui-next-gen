import { createSelector } from 'reselect';

export const makeSelectSearch = () => createSelector(
  (state) => state.get('search'),
  (state) => state.toJS()
);
