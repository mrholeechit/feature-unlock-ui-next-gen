import {
  queryChange,
  helpToggle,
} from '../actions';
import {
  QUERY_CHANGE,
  HELP_TOGGLE,
} from '../constants';

describe('SearchPage actions', () => {
  describe('Query change Action', () => {
    it('has a type of QUERY_CHANGE', () => {
      const expected = {
        type: QUERY_CHANGE,
        query: 'a',
      };
      expect(queryChange('a')).toEqual(expected);
    });
  });

  describe('Help toggle action', () => {
    it('has a type of HELP_TOGGLE', () => {
      const expected = {
        type: HELP_TOGGLE,
      };
      expect(helpToggle()).toEqual(expected);
    });
  });
});
