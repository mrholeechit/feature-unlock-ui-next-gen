import { fromJS } from 'immutable';
import searchPageReducer from '../reducer';
import {
  QUERY_CHANGE,
  HELP_VENDOR_CLICK,
  SEARCH_CLEAR,
  HELP_TOGGLE,
} from '../constants';

describe('searchPageReducer', () => {
  it('returns the initial state', () => {
    expect(searchPageReducer(undefined, {})).toEqual(fromJS({
      query: '',
      vendors: [],
      showHelp: false,
    }));
  });

  it('change query', () => {
    expect(searchPageReducer(undefined, {
      type: QUERY_CHANGE,
      query: '@FN5s1e5_9d',
    }).toJS()).toEqual({
      currentVendor: null,
      showHelp: false,
      query: '@FN5s1e5_9d',
      vendors: [
        {
          description: 'This AGCO-guidance system utilizes a NovAtel receiver. Accuracy upgrades to enable Centimetre level accuracy can be purchased from this site. This upgrade is a permanent, non-refundable option. A correction signal subscription for Centimeter services can also be purchased.',
          image: 'IMAGE_MOCK',
          label: 'VarioGuide/Auto-Guide NovAtel',
          match: /(^[\S]{1}FN[\S]{8}$)|(^[\S]{1}FN[\S]{10}$)|(^NMD[\S]{8}$)|(^NMD[\S]{10}$)/i,
          patterns: [{
            format: 'VarioGuide/Auto-Guide NovAtel has 11 or 13 Character serial numbers. The first three characters are consistently "*FN****" or "NMD****"',
            patterns: ['*FN**********', 'NMD**********'],
          }],
          title: 'VarioGuide/Auto-Guide with NovAtel Receiver',
          value: 'NovAtel',
        },
      ],
    });
  });

  it('click vendor', () => {
    expect(searchPageReducer(undefined, {
      type: HELP_VENDOR_CLICK,
      vendor: 'NovAtel',
    }).toJS()).toEqual({
      query: '',
      vendors: [],
      showHelp: false,
    });
  });

  it('clear search', () => {
    expect(searchPageReducer(undefined, {
      type: SEARCH_CLEAR,
    }).toJS()).toEqual({
      query: '',
      vendors: [],
      showHelp: false,
    });
  });

  it('help toggle', () => {
    const expected = fromJS({
      query: '',
      vendors: [],
      showHelp: true,
    });

    expect(searchPageReducer(undefined, {
      type: HELP_TOGGLE,
    }).toJS()).toEqual(expected.toJS());

    expect(searchPageReducer(expected, {
      type: HELP_TOGGLE,
    }).toJS()).toEqual({
      query: '',
      vendors: [],
      showHelp: false,
    });
  });
});
