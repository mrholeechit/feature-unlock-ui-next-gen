import { fromJS } from 'immutable';

import {
  makeSelectSearch,
} from '../selectors';

describe('selectLanguage', () => {
  it('should select the search state', () => {
    const search = fromJS({
      query: '',
      vendors: [],
      currentVendor: null,
    });
    const mockedState = fromJS({
      search,
    });
    expect(makeSelectSearch()(mockedState)).toEqual(search.toJS());
  });
});
