import React from 'react';
import { func } from 'prop-types';
import styled from 'styled-components';
import theme from 'utils/styles';
import Link from 'components/Link';
import Icon from 'components/Icon';
import translate from 'utils/translate';

const StyledLink = styled(Link)`
  float: left;
  font-size: ${theme.font.small};
  position: relative;
  margin-top: 13px;
  margin-left: 2px;

  > span {
    display: inline-block;
    vertical-align: middle;
  }

  > .icon svg {
    margin-left: 5px;
    height: 13px;
    width: 13px;
  }
`;

const Help = ({
  onClick,
}) => (
  <StyledLink underlined onClick={onClick}>{translate('FORMATTING_SERIAL')} <Icon name="exclamation-circle" /></StyledLink>
);

Help.propTypes = {
  onClick: func,
};

export default Help;
