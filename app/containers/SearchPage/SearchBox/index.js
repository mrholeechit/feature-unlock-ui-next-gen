import React from 'react';
import { string, arrayOf, object, func } from 'prop-types';
/* eslint-disable import/extensions */
import SearchIcon from 'images/icons/search.svg?fill=#565656';
/* eslint-enable import/extensions */
import styled from 'styled-components';
import theme from 'utils/styles';
import Autocomplete from 'components/Autocomplete';
import translate from 'utils/translate';
import Title from './Title';
import Help from './Help';
import Instructions from './Instructions';

const StyledSearchBox = styled.div`
  max-width: 1024px;
  width: calc(100% - ${theme.container.gutter});
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 0;
  transform: translate(-50%, -100%);
  background: ${theme.color.concrete};
  border-radius: ${theme.border.radius};
  padding: 40px;
  box-shadow: 0px 10px 20px -5px rgba(0, 0, 0, 0.50);

  .input {
    padding: 15px 15px 15px 30px;
    background: #FFF url(${SearchIcon}) 10px 47% no-repeat;
    background-size: 15px;
  }
`;

const renderMenu = (children) => (
  <div className="list">
    <Instructions />
    {children}
  </div>
);

const SearchBox = ({
  query,
  vendors,
  onChangeQuery,
  onClickVendor,
  onHelpClick,
}) => (
  <StyledSearchBox>
    <Title />
    <Autocomplete
      placeholder={translate('SEARCH_BY')}
      value={query}
      items={vendors}
      onChange={onChangeQuery}
      onSelect={(_, vendor) => onClickVendor({
        vendor,
        query,
      })}
      autoHeight
      autoFocus
      id="search"
      renderMenu={renderMenu}
    />
    <Help onClick={onHelpClick} />
  </StyledSearchBox>
);

SearchBox.propTypes = {
  query: string,
  vendors: arrayOf(object),
  onChangeQuery: func,
  onClickVendor: func,
  onHelpClick: func,
};

export default SearchBox;
