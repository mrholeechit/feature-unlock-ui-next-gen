import React from 'react';
import Title from 'components/Title';
import translate from 'utils/translate';

const StyledTitle = Title.withComponent('label').extend`
  text-align: left;
  margin: 0 0 15px 0;
  display: inline-block;
`;

export default () => (
  <StyledTitle big htmlFor="search">{translate('FIND_PRODUCTS')}</StyledTitle>
);
