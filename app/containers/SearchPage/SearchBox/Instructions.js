import React from 'react';
import styled from 'styled-components';
import theme from 'utils/styles';
import translate from 'utils/translate';

const Instructions = styled.p`
  float: left;
  width: 100%;
  color: ${theme.color.mineShaft};
  margin: 0 0 10px 0;
  font-weight: ${theme.font.semiBold};
  padding: 5px 7px 10px 7px;
  border-bottom: 1px solid ${theme.color.alto};
`;

export default () => (
  <Instructions>{translate('REFINE_SEARCH')}</Instructions>
);
