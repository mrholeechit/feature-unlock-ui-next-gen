import { fromJS } from 'immutable';
import { search } from 'utils/vendors';
import { QUERY_CHANGE, HELP_TOGGLE } from './constants';

const initialState = fromJS({
  query: '',
  vendors: [],
  showHelp: false,
});

export default function searchPageReducer(state = initialState, action) {
  switch (action.type) {
    case QUERY_CHANGE:
      return state
        .set('query', action.query)
        .set('currentVendor', null)
        .set('vendors', search(action.query));
    case HELP_TOGGLE:
      return state
        .set('showHelp', !state.get('showHelp'));
    default:
      return state;
  }
}
