import styled from 'styled-components';

export default styled.ul`
  padding: 0 0 0 20px;
  float: left;
  width: 100%;
  margin: 0;
`;
