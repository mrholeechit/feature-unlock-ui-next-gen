import React from 'react';
import { arrayOf, object } from 'prop-types';
import styled from 'styled-components';
import theme from 'utils/styles';
import Title from 'components/Title';
import translate from 'utils/translate';
import Accordion from 'components/Accordion';
import { map } from 'ramda';
import Item from './Item';
import List from './List';
import Description from './Description';

const StyledAccordion = styled(Accordion)`
  .accordion__title {
    border: 0;
    border-top: 1px solid ${theme.color.alto};
    background: none;
    border-radius: 0;
    padding: 10px 0;

    .title {
      color: ${theme.color.mineShaft};
    }
  }

  .accordion__body {
    padding: 0 20px 20px 20px;
    border-radius: 0;
  }

  .accordion__item {
    &:last-child .accordion__title {
      border-bottom: 1px solid ${theme.color.alto};
      &[aria-selected="true"] {
        border-bottom: none;
      }
    }

    .accordion__body[aria-hidden="false"] {
      border-bottom: 1px solid ${theme.color.alto};
    }
  }
`;

const HelpBox = ({ vendors }) => {
  const items = map(({ label, patterns }) => ({
    title: label,
    children: (
      <For each="pattern" index="patternKey" of={patterns}>
        <Description>{pattern.format}</Description>
        <List>
          <For each="pattern" index="patternKey" of={pattern.patterns}>
            <Item key={patternKey}>{pattern}</Item>
          </For>
        </List>
      </For>
    ),
  }), vendors);

  return (
    <div>
      <Title>{translate('NUMBER_FORMATS.SEARCH_BY_RECEIVER')}</Title>
      <p>{translate('NUMBER_FORMATS.RECEIVER_SERIAL')}</p>
      <StyledAccordion items={items} />
    </div>
  );
};

HelpBox.propTypes = {
  vendors: arrayOf(object),
};

export default HelpBox;
