import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.p`
  float: left;
  width: 100%;
  margin: 0 0 5px;
  color: ${theme.color.mineShaft};
`;
