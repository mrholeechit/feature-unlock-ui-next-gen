import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.li`
  float: left;
  width: 100%;
  padding: 0;
  margin: 0;
  color: ${theme.color.mineShaft};
  list-style: disc;
`;
