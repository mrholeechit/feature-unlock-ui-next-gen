import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.p`
  float: left;
  font-size: ${theme.font.small};
  margin-bottom: 0;
`;
