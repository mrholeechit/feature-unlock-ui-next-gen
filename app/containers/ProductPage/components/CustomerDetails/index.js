import React, { PureComponent } from 'react';
import { equals, any } from 'ramda';
import { func, string, bool } from 'prop-types';
import theme from 'utils/styles';
import styled from 'styled-components';
import translate from 'utils/translate';
import Form from 'components/Form';
import Input from 'components/Input';
import Label from 'components/Label';
import FormGroup from 'components/FormGroup';
import Select from 'components/Select';
import Tooltip from 'components/Tooltip';
import { COUNTRIES } from 'containers/ProductPage/Generic/constants';
import Button from 'components/Button';
import Description from './Description';
import Title from './Title';

const StyledButton = styled(Button)`
  float: right;
`;

const Wrapper = styled.footer`
  float: left;
  width: 100%;
  padding-top: 35px;
  margin-top: 35px;
  padding-bottom: 35px;
  border-top: 1px solid ${theme.color.alto};
`;

const InnerWrapper = styled.div`
  float: left;
  max-width: 768px;
`;

export default class CustomerDetails extends PureComponent {
  static propTypes = {
    onChangeCustomerReference: func,
    onChangeMachineLocation: func,
    onSubmit: func,
    regionCode: string,
    machineLocation: string,
    customerReference: string,
    customerReferenceError: string,
    canSubmit: bool,
  };

  componentWillReceiveProps({ customerReferenceError }) {
    Tooltip.changeVisibility('customerReferenceError', !!customerReferenceError);
  }

  render() {
    const {
      onChangeCustomerReference,
      onChangeMachineLocation,
      onSubmit,
      canSubmit,
      regionCode,
      machineLocation,
      customerReference,
      customerReferenceError,
    } = this.props;

    return (
      <Wrapper>
        <Form onSubmit={onSubmit}>
          <InnerWrapper>
            <Title big>{translate('PRODUCT_PAGE.CUSTOMER_DETAILS')}</Title>
            <div>
              <FormGroup>
                <Label html For="customerReference">{translate('PRODUCT_PAGE.CUSTOMER_REFERENCE')}</Label>
                <Tooltip placement="right" type="error" id="customerReferenceError" title="" overlay={customerReferenceError} >
                  <Input
                    id="customerReference"
                    value={customerReference}
                    name="customerReference"
                    placeholder={translate('PRODUCT_PAGE.REFERENCE_PLACEHOLDER')}
                    onChange={(event) => onChangeCustomerReference(event, regionCode)}
                  />
                </Tooltip>
              </FormGroup>
              <FormGroup>
                <Label htmlFor="machineLocation">{translate('PRODUCT_PAGE.MACHINE_LOCATION')}</Label>
                <Select
                  id="machineLocation"
                  options={COUNTRIES}
                  placeholder={translate('PRODUCT_PAGE.MACHINE_PLACEHOLDER')}
                  value={machineLocation}
                  onChange={onChangeMachineLocation}
                />
              </FormGroup>
            </div>
            <Description>
              {translate('PRODUCT_PAGE.REFERENCE_HELP')} <br />
              <Choose>
                <When condition={equals(regionCode, 'EAME')} >
                  {translate('PRODUCT_PAGE.REF_CHAR_LIMIT_17')}
                </When>
                <When condition={any(equals(regionCode), ['NA', 'SA'])} >
                  {translate('PRODUCT_PAGE.REF_CHAR_LIMIT_8')}
                </When>
              </Choose>
            </Description>
          </InnerWrapper>
          <StyledButton aria-label="checkout" color="sanJuan" disabled={!canSubmit} filled>{translate('PRODUCT_PAGE.CHECKOUT')}</StyledButton>
        </Form>
      </Wrapper>
    );
  }
}
