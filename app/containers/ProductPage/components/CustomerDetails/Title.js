import styled from 'styled-components';
import Title from 'components/Title';

export default styled(Title)`
  float: left;
  width: 100%;
  margin-bottom: 10px;
`;
