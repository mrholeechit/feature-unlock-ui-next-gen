import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.p`
  font-size: ${theme.font.tiny};
  line-height: ${theme.font.small};
`;
