import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.strong`
  display: block;
  margin-bottom: 20px;
  font-size: ${theme.font.tiny};
`;
