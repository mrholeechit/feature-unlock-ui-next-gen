import styled from 'styled-components';

export default styled.img`
  max-width: 250px;
  width: 100%;
  margin-bottom: 20px;
`;

