import React from 'react';
import styled from 'styled-components';
import theme from 'utils/styles';
import { string } from 'prop-types';
import translate from 'utils/translate';
import Image from './Image';
import Title from './Title';
import SerialNumber from './SerialNumber';
import Vin from './Vin';
import Description from './Description';
import { isAgcoVin } from '../../../../config';

const Wrapper = styled.header`
  float: left;
  width: 100%;
  text-align: center;
  padding-bottom: 35px;
  margin-bottom: 35px;
  border-bottom: 1px solid ${theme.color.alto};
`;

const Info = ({
  title,
  serialNumber,
  image,
  description,
}) => (
  <Wrapper>
    <If condition={image}>
      <Image src={image} />
    </If>
    <Title big>{title}</Title>
    <Choose>
      <When condition={isAgcoVin(serialNumber)}>
        <Vin>{(description ? 'VIN #' : 'VIN #')}{serialNumber}</Vin>
      </When>
      <Otherwise>
        <SerialNumber>{translate(description ? 'SERIAL' : 'SEARCH')} #{serialNumber}</SerialNumber>
      </Otherwise>
    </Choose>
    <If condition={description}>
      <Description>{description}</Description>
    </If>
  </Wrapper>
);

Info.propTypes = {
  title: string,
  serialNumber: string,
  image: string,
  description: string,
};

export default Info;
