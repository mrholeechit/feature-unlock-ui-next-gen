import styled from 'styled-components';
import theme from 'utils/styles';

export default styled.strong`
  display: block;
  font-size: ${theme.font.tiny};
`;
