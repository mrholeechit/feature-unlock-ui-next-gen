import Title from 'components/Title';

export default Title.withComponent('h1').extend`
  display: block;
`;
