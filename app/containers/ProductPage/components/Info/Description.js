import styled from 'styled-components';

export default styled.p`
  max-width: 735px;
  display: block;
  margin: 0 auto;
`;
