import React from 'react';
import { arrayOf, object, func } from 'prop-types';
import Nav from 'containers/ProductPage/components/Nav';
import translate from 'utils/translate';
import { ORDER_TYPES } from 'utils/constants';
import { length, any, propEq, find } from 'ramda';
import Table from '../Table';
import { createOptions } from '../../utils';
import FeatureInfo from '../FeatureInfo';

const List = ({
  items,
  cart,
  onToggleRenew,
  onClickTransfer,
  onClickOption,
}) => {
  const findItem = (id) => find(propEq('id', id), cart);
  const existsInCart = (id) => any(propEq('id', id), cart);

  const fields = [
    {
      field: 'description',
      name: translate('PRODUCT_TABLE.OPTIONS'),
      format: ({ id, feature }) => ( // eslint-disable-line
        <FeatureInfo {...{ name: id, ...feature }} withoutCheckbox withoutPadding />
      ),
    },
    {
      field: 'activationDate',
      name: translate('TABLES.ACTIVATION'),
      format: ({ order, feature, id }) => ( // eslint-disable-line
        <Choose>
          <When condition={feature.kind === ORDER_TYPES.subscription && !existsInCart(id)}>
            {order.activationDate}
          </When>
          <When condition={feature.kind === ORDER_TYPES.subscription && existsInCart(id)} >
            {findItem(id).activationDate}
          </When>
          <When condition={feature.kind === ORDER_TYPES.upgrade}>
            {order.createdOn}
          </When>
        </Choose>
      ),
      width: 110,
    },
    {
      field: 'expirationDate',
      name: translate('TABLES.EXPIRATION'),
      format: ({ expirationDate, feature, id }) => ( // eslint-disable-line
        <Choose>
          <When condition={feature.kind === ORDER_TYPES.subscription && !existsInCart(id)} >
            {expirationDate}
          </When>
          <When condition={feature.kind === ORDER_TYPES.subscription && existsInCart(id)} >
            {findItem(id).expirationDate}
          </When>
          <When condition={feature.kind === ORDER_TYPES.upgrade}>
            N/A
          </When>
        </Choose>
      ),
      width: 130,
    },
    {
      field: 'activationStatus',
      name: translate('PRODUCT_TABLE.FEATURE_STATUS'),
      format: ({ activationStatus }) => translate(`API.STATUS.${activationStatus}`),
      width: 200,
    },
    {
      field: 'actions',
      name: translate('PRODUCT_TABLE.ACTIONS'),
      format: ({ id, authorizationCode, vendor, ...deviceFeature}) => { // eslint-disable-line
        const options = createOptions({
          onToggleRenew,
          existsInCart: existsInCart(id),
          onClickOption,
          onClickTransfer,
        });

        return (
          <Nav>
            <For each="option" index="idx" of={deviceFeature.options}>
              {options[option.name]({
                authorizationCode,
                id,
                option,
                vendor,
              })}
            </For>
          </Nav>
        );
      },
      width: 210,
    },
  ];

  return (
    <Table noDataText="" minRows={length(items)} items={items} fields={fields} />
  );
};

List.propTypes = {
  items: arrayOf(object),
  cart: arrayOf(object),
  onToggleRenew: func,
  onClickOption: func,
  onClickTransfer: func,
};

export default List;
