import React from 'react';
import styled from 'styled-components';
import Link from 'components/Link';
import { node } from 'prop-types';

const Wrapper = styled.div`
  padding: 0 5px;
`;

const Action = ({ children, ...props }) => (
  <Wrapper>
    <Link underlined {...props}>{children}</Link>
  </Wrapper>
);

Action.propTypes = {
  children: node,
};

export default Action;
