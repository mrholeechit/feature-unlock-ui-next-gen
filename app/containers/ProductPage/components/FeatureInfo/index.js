import React from 'react';
import styled from 'styled-components';
import { string, bool, func } from 'prop-types';
import Checkbox from 'components/Checkbox';
import Message from '../Message';

const Available = styled.label`
  position: relative;
  padding-left: ${({ withoutPadding }) => withoutPadding ? '0' : '35px'};
  margin: 0;
  cursor: pointer;
`;

const Unavailable = Available.withComponent('div').extend`
  cursor: inherit;
`;

const StyledCheckbox = styled(Checkbox)`
  position: absolute;
  left: 0;
  top: 2px;
`;

const FeatureInfo = ({
  name,
  description,
  prerequisites,
  warning,
  partNumber,
  available,
  onChange,
  checked,
  withoutCheckbox,
  withoutPadding,
  ...props
}) => {
  const info = [
    <If condition={partNumber}>
      <strong>{partNumber}</strong>
      <br />
    </If>,
    <strong>{description}</strong>,
    <If condition={prerequisites || warning}>
      <br />
    </If>,
    <If condition={prerequisites}>
      <Message>{prerequisites}</Message>
    </If>,
    <If condition={warning}>
      <Message>{warning}</Message>
    </If>,
  ];

  return (
    <Choose>
      <When condition={available && !withoutCheckbox}>
        <Available htmlFor={name} withoutPadding={withoutPadding}>
          <StyledCheckbox name={name} checked={checked} onChange={() => onChange(name)} {...props} />
          {info}
        </Available>
      </When>
      <Otherwise>
        <Unavailable withoutPadding={withoutPadding}>
          {info}
        </Unavailable>
      </Otherwise>
    </Choose>
  );
};

FeatureInfo.propTypes = {
  name: string,
  description: string,
  prerequisites: string,
  warning: string,
  partNumber: string,
  available: bool,
  onToggleFeature: func,
  onChange: func,
  withoutCheckbox: bool,
  checked: bool,
  withoutPadding: bool,
};

export default FeatureInfo;
