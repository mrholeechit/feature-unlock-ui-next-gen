import React from 'react';
import { arrayOf, object, func } from 'prop-types';
import Datepicker from 'components/Datepicker';
import translate from 'utils/translate';
import { find, propEq, length } from 'ramda';
import Table from '../Table';
import FeatureInfo from '../FeatureInfo';

const getItem = (id, cart) => find(propEq('id', id), cart);

const List = ({
  items,
  onChangeActivationDate,
  onToggleFeature,
  cart,
}) => {
  const fields = [
    {
      field: 'description',
      name: translate('PRODUCT_TABLE.OPTIONS'),
      format: ({ id, ...props }) => { // eslint-disable-line
        const exist = !!getItem(id, cart);

        return (
          <FeatureInfo {...props} name={id} checked={exist} onChange={onToggleFeature} />
        );
      },
    },
    {
      field: 'activation',
      name: translate('TABLES.ACTIVATION'),
      format: ({ id }) => { // eslint-disable-line
        const item = getItem(id, cart);

        return (
          <If condition={item}>
            <Datepicker
              name="activationDate"
              onChange={(event) => onChangeActivationDate({ id, event })}
              data-enable-time
              placeholder={translate('PRODUCT_TABLE.ACTIVATION_DATE')}
              value={item.activationDate}
            />
          </If>
        );
      },
      width: 180,
    },
    {
      field: 'expiration',
      name: translate('TABLES.EXPIRATION'),
      width: 160,
      format: ({ id }) => {
        const item = getItem(id, cart);
        return item ? item.expirationDate : '';
      },
    },
  ];

  return (
    <Table noDataText="" minRows={length(items)} items={items} fields={fields} />
  );
};

List.propTypes = {
  items: arrayOf(object),
  onChangeActivationDate: func,
  onToggleFeature: func,
  cart: arrayOf(object),
};

export default List;
