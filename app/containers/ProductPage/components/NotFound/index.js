import React from 'react';
import translate from 'utils/translate';
import { string } from 'prop-types';
import styled from 'styled-components';
import Info from '../Info';

const Message = styled.p`
  white-space: pre-wrap;
  text-align: center;
  float: left;
  width: 100%;
`;

const NotFound = ({ serialNumber }) => [
  <Info
    title={translate('PRODUCT_PAGE.NO_RESULTS')}
    serialNumber={serialNumber}
  />,
  <Message>
    {translate('PRODUCT_PAGE.NO_PRODUCT_FOUND')} &quot;{serialNumber}&quot;. <br />
    {translate('PRODUCT_PAGE.SEARCH_AGAIN')}
  </Message>,
];

NotFound.propTypes = {
  serialNumber: string,
};

export default NotFound;
