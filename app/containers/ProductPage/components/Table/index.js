import React from 'react';
import styled from 'styled-components';
import { pathOr } from 'ramda';
import Table from 'components/Table';
import theme from 'utils/styles';

const CustomTableStyle = styled(Table)`
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-top: 0;

  .input,
  .Select {
    width: 100%;
  }

  .-unavailable {
    background: ${theme.color.silver};
    cursor: not-allowed;
  }
`;

const isAvailable = pathOr(true, ['original', 'available']);

const CustomTable = (props) => (
  <CustomTableStyle
    getTrProps={(state, info) => ({
      className: isAvailable(info) ? '' : '-unavailable',
    })
    }
    {...props}
  />
);

export default CustomTable;
