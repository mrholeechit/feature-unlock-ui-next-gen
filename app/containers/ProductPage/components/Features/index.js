import React from 'react';
import styled from 'styled-components';
import theme from 'utils/styles';
import Accordion from 'components/Accordion';
import { node } from 'prop-types';
import Block from 'components/Block';
import translate from 'utils/translate';

const StyledAccordion = styled(Accordion)`
  .accordion__body[aria-hidden="false"] {
    border-bottom-left-radius: ${theme.border.radius};
    border-bottom-right-radius: ${theme.border.radius};
  }

  .accordion__title {
    background: #fff;
  }

  .title {
    color: ${theme.color.monza};
  }

  .alert {
    margin-top: 10px;
  }
`;

const Features = ({
  purchasesComponent,
  upgradesComponent,
  subscriptionsComponent,
}) => (
  <Block>
    <If condition={purchasesComponent}>
      <StyledAccordion expanded title={translate('PRODUCT_TABLE.PURCHASES')} className="purchases-list">
        {purchasesComponent}
      </StyledAccordion>
    </If>
    <If condition={upgradesComponent}>
      <StyledAccordion expanded title={`${translate('PRODUCT_TABLE.UPGRADES')} (${translate('PRODUCT_TABLE.AUTH_CODE')})`} className="upgrades-list">
        {upgradesComponent}
      </StyledAccordion>
    </If>
    <If condition={subscriptionsComponent}>
      <StyledAccordion expanded title={translate('PRODUCT_TABLE.SUBSCRIPTIONS')} className="subscriptions-list">
        {subscriptionsComponent}
      </StyledAccordion>
    </If>
  </Block>
);

Features.propTypes = {
  purchasesComponent: node,
  upgradesComponent: node,
  subscriptionsComponent: node,
};

export default Features;
