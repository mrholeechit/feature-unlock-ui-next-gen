import React from 'react';
import Title from 'components/Title';
import Modal from 'components/Modal';
import Table from 'components/Table';
import Button from 'components/Button';
import Block from 'components/Block';
import styled from 'styled-components';
import theme from 'utils/styles';
import { length } from 'ramda';
import { bool, arrayOf, object, func } from 'prop-types';
import translate from 'utils/translate';

const StyledTable = styled(Table)`
  margin: 25px 0;

  .rt-td {
    font-size: ${theme.font.small};
  }
`;

const StyledButton = styled(Button)`
  margin-right: 10px;

  &:last-child {
    margin-right: 0;
  }
`;

const CustomerReferenceConfirmation = ({
  isOpen,
  onClose,
  onConfirm,
  items,
}) => {
  const fields = [
    {
      field: 'description',
      name: translate('TABLES.FEATURES'),
      format: ({ partNumber, description }) => ( // eslint-disable-line
        `${partNumber} - ${description}`
      ),
    },
    {
      field: 'customerReference',
      name: translate('PRODUCT_PAGE.CUSTOMER_REFERENCE'),
    },
  ];

  return (
    <Modal isOpen={isOpen} title="Notice" onClose={onClose}>
      <Title>{translate('PRODUCT_PAGE.EAME_MULTIPLE_ORDERS')}</Title>
      <StyledTable noDataText="" minRows={length(items)} items={items} fields={fields} />
      <Block center>
        <StyledButton color="sanJuan" onClick={onClose}>{translate('CANCEL')}</StyledButton>
        <StyledButton color="sanJuan" filled onClick={onConfirm}>{translate('PRODUCT_PAGE.CHECKOUT')}</StyledButton>
      </Block>
    </Modal>
  );
};

CustomerReferenceConfirmation.propTypes = {
  isOpen: bool,
  onClose: func,
  onConfirm: func,
  items: arrayOf(object),
};

export default CustomerReferenceConfirmation;
