import React from 'react';
import { arrayOf, object, func } from 'prop-types';
import translate from 'utils/translate';
import { length, find, propEq } from 'ramda';
import Table from '../Table';
import FeatureInfo from '../FeatureInfo';

const getItem = (id, cart) => find(propEq('id', id), cart);

const List = ({
  items,
  onToggleFeature,
  cart,
}) => {
  const fields = [
    {
      field: 'description',
      name: translate('PRODUCT_TABLE.OPTIONS'),
      format: ({ id, ...props }) => { // eslint-disable-line


        return (
          <FeatureInfo {...props} name={id} checked={exist} onChange={onToggleFeature} />
        );
      },
      format: ({ id, ...props }) => { // eslint-disable-line
        const exist = !!getItem(id, cart);

        return (
          <FeatureInfo
            name={id}
            onChange={onToggleFeature}
            checked={exist}
            {...props}
          />
        );
      },
    },
  ];

  return (
    <Table noDataText="" minRows={length(items)} items={items} fields={fields} />
  );
};

List.propTypes = {
  items: arrayOf(object),
  onToggleFeature: func,
  cart: arrayOf(object),
};

export default List;
