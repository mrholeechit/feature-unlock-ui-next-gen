import genericListeners from './Generic/sagas';
import acmListeners from './ACM/sagas';
import deviceInsightListeners from './DeviceInsight/sagas';

export default [
  genericListeners,
  acmListeners,
  deviceInsightListeners,
];
