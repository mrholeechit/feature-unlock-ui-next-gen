import React from 'react';
import styled from 'styled-components';
import Tooltip from 'components/Tooltip';
import Button from 'components/Button';
import { length, equals } from 'ramda';
import { AC2_URL, AUTH_REDIRECT_URL } from 'config';
import translate from 'utils/translate';
import { disableOnClick } from 'components/Disable';
import Action from './components/Action';

const StyledButton = styled(Button)`
  margin-left: 5px;
  display: inline;
`;

const ActionDisableOnClick = disableOnClick(Action);

export const canSubmit = ({
  cart,
  customerReference,
  machineLocation,
  customerReferenceError,
}) => {
  const hasFeaturesInCart = !!length(cart);
  const hasCustomerReference = !!length(customerReference);
  const hasMachineLocation = !!length(machineLocation);
  const hasErrors = customerReferenceError;

  return hasFeaturesInCart && hasCustomerReference && hasMachineLocation && !hasErrors;
};

export const createOptions = ({
  onToggleRenew,
  onToggleUpgrade,
  onClickTransfer,
  onClickOption,
  canEnroll,
  showSubscriptions,
  existsInCart,
  vin,
}) => ({
  enroll: () => (
    <If condition={canEnroll}>
      <Action className="enroll-option" to={`${AC2_URL}admin/equipmentEdit?identificationNumber=${vin}&referrer=${AUTH_REDIRECT_URL}`}>Enroll</Action>
    </If>
  ),
  deactivate: ({ option: { href } }) => ( // eslint-disable-line
    <Tooltip
      id="deactivate"
      title={translate('TABLES.DEACTIVATE_CONFIRM')}
      overlay={[
        <StyledButton compact color="mineShaft" onClick={Tooltip.hide}>{translate('CANCEL')}</StyledButton>,
        <StyledButton compact color="monza" filled onClick={() => onClickOption({ method: 'post', resource: href })}>{translate('ADMIN.DEALER_USERS.DEACTIVATE')}</StyledButton>,
      ]}
    >
      <Action className="deactivate-option">{translate('ADMIN.DEALER_USERS.DEACTIVATE')}</Action>
    </Tooltip>
  ),
  upgrade: () => (
    <Action onClick={onToggleUpgrade} disabled={existsInCart} className="upgrade-option">
      {showSubscriptions ? translate('PRODUCT_TABLE.CANCEL_UPGRADE') : translate('PRODUCT_TABLE.UPGRADE')}
    </Action>
  ),
  renew: ({ id }) => ( // eslint-disable-line
    <Action onClick={() => onToggleRenew(id)} disabled={showSubscriptions} className="renew-option">
      { !existsInCart ? translate('PRODUCT_TABLE.RENEW') : translate('PRODUCT_TABLE.CANCEL_RENEW')}
    </Action>
  ),
  resend: ({ option: { href }, vendor }) => ( // eslint-disable-line
    <Tooltip
      id="resend"
      title="Resend request"
      overlay={
        <Choose>
          <When condition={equals(vendor, 'Trimble')}>
            {translate('PRODUCT_PAGE.VENDOR_TRIMBLE_RESEND_CONFIRMATION_MESSAGE')}
          </When>
          <When condition={equals(vendor, 'NovAtel')}>
            {translate('PRODUCT_PAGE.VENDOR_NOVATEL_RESEND_CONFIRMATION_MESSAGE')}
          </When>
        </Choose>
      }
    >
      <ActionDisableOnClick className="resend-option" onClick={() => onClickOption({ resource: `${href}?immediateActivation=true` })}>{translate('PRODUCT_TABLE.RESEND')}</ActionDisableOnClick>
    </Tooltip>
  ),
  retrieve: ({ authorizationCode }) => ( // eslint-disable-line
    <Tooltip
      id="retrieve"
      title="Authorization Code"
      overlay={authorizationCode}
    >
      <Action className="retrieve-option">{translate('PRODUCT_TABLE.RETRIEVE')}</Action>
    </Tooltip>
  ),
  transfer: ({ id }) => ( // eslint-disable-line
    <Action className="transfer-option" onClick={() => onClickTransfer(id)}>{translate('TRANSFER_SUBSCRIPTION')}</Action>
  ),
});
