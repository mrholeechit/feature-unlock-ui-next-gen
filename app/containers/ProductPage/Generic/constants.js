import { sortBy, prop } from 'ramda';

export const PRODUCT_FETCH_REQUESTED = 'FUN/PRODUCT_PAGE/PRODUCT_FETCH_REQUESTED';
export const PRODUCT_FETCH_SUCCEEDED = 'FUN/PRODUCT_PAGE/PRODUCT_FETCH_SUCCEEDED';
export const PRODUCT_FETCH_FAILED = 'FUN/PRODUCT_PAGE/PRODUCT_FETCH_FAILED';
export const CHANGE_ACTIVATION_DATE = 'FUN/PRODUCT_PAGE/CHANGE_ACTIVATION_DATE';
export const TOGGLE_FEATURE = 'FUN/PRODUCT_PAGE/TOGGLE_FEATURE';
export const RESOURCE_CALL = 'FUN/PRODUCT_PAGE/RESOURCE_CALL';
export const TOGGLE_RENEW = 'FUN/PRODUCT_PAGE/TOGGLE_RENEW';
export const CHANGE_CUSTOMER_REFERENCE = 'FUN/PRODUCT_PAGE/CHANGE_CUSTOMER_REFERENCE';
export const CHANGE_MACHINE_LOCATION = 'FUN/PRODUCT_PAGE/CHANGE_MACHINE_LOCATION';
export const CHECKOUT = 'FUN/PRODUCT_PAGE/CHECKOUT';
export const UPDATE_CART = 'FUN/PRODUCT_PAGE/UPDATE_CART';
export const CUSTOMER_REFERENCE_ERROR = 'FUN/PRODUCT_PAGE/CUSTOMER_REFERENCE_ERROR';
export const TOGGLE_CUSTOMER_REFERENCE_CONFIRMATION = 'FUN/PRODUCT_PAGE/TOGGLE_CUSTOMER_REFERENCE_CONFIRMATION';
export const ROUTE_TO_CHECKOUT = 'FUN/PRODUCT_PAGE/ROUTE_TO_CHECKOUT';
export const TOGGLE_FETCH = 'FUN/PRODUCT_PAGE/TOGGLE_FETCH';

export const COUNTRIES = sortBy(prop('label'), [
  { value: 'AL', label: 'Shqipëria' },
  { value: 'AM', label: 'Hayastán' },
  { value: 'AT', label: 'Österreich' },
  { value: 'BY', label: 'Bielaruś' },
  { value: 'BE', label: 'België' },
  { value: 'BO', label: 'Bolivia' },
  { value: 'BA', label: 'Bosna i Hercegovina' },
  { value: 'BR', label: 'Brasil' },
  { value: 'BG', label: 'Bulgariya' },
  { value: 'CL', label: 'Chile' },
  { value: 'CO', label: 'Colombia' },
  { value: 'CR', label: 'Costa Rica' },
  { value: 'HR', label: 'Hrvatska' },
  { value: 'CY', label: 'Kypros' },
  { value: 'CZ', label: 'Česká republika' },
  { value: 'DK', label: 'Danmark' },
  { value: 'DO', label: 'República Dominicana' },
  { value: 'EC', label: 'Ecuador' },
  { value: 'EE', label: 'Eesti' },
  { value: 'FI', label: 'Suomi' },
  { value: 'FR', label: 'France' },
  { value: 'GE', label: 'Sak\'art\'velo' },
  { value: 'DE', label: 'Deutschland' },
  { value: 'GR', label: 'Hellas' },
  { value: 'GT', label: 'Guatemala' },
  { value: 'HN', label: 'Honduras' },
  { value: 'HU', label: 'Magyarország' },
  { value: 'IS', label: 'Ísland' },
  { value: 'IE', label: 'Ireland' },
  { value: 'IT', label: 'Italia' },
  { value: 'KZ', label: 'Qazaqstan' },
  { value: 'KG', label: 'Kyrgyzstan' },
  { value: 'LV', label: 'Latvija' },
  { value: 'FL', label: 'Liechtenstein' },
  { value: 'LT', label: 'Lietuva' },
  { value: 'LU', label: 'Luxemburg' },
  { value: 'MK', label: 'Makedonija' },
  { value: 'MT', label: 'Malta' },
  { value: 'MD', label: 'Moldova' },
  { value: 'ME', label: 'Crna Gora' },
  { value: 'NL', label: 'Nederland' },
  { value: 'NI', label: 'Nicaragua' },
  { value: 'NO', label: 'Norge' },
  { value: 'PA', label: 'Panamá' },
  { value: 'PY', label: 'Paraguay' },
  { value: 'PE', label: 'Peru' },
  { value: 'PL', label: 'Polska' },
  { value: 'PT', label: 'Portugal' },
  { value: 'RO', label: 'România' },
  { value: 'RU', label: 'Rossiya' },
  // { value: 'RU', label: 'Rossiya - Region Central, Volga, Southern' },
  // { value: 'RU', label: 'Rossiya - Region North-West, Urals, Sibirien, Far Easter' },
  { value: 'SA', label: 'Al-Mamlaka Al-‘Arabiyyah as Sa‘ūdiyyah' },
  { value: 'RS', label: 'Srbija' },
  { value: 'SK', label: 'Slovensko' },
  { value: 'SI', label: 'Slovenija' },
  { value: 'ES', label: 'España' },
  { value: 'ZA', label: 'South Africa' },
  { value: 'SE', label: 'Sverige' },
  { value: 'CH', label: 'Schweiz' },
  { value: 'TR', label: 'Türkiye' },
  { value: 'TM', label: 'Türkmenistan' },
  { value: 'UA', label: 'Ukraїna' },
  { value: 'GB', label: 'United Kingdom' },
  { value: 'UZ', label: 'O‘zbekiston' },
  { value: 'UY', label: 'Uraguay' },
  { value: 'VE', label: 'Venezuela' },
  { value: 'US', label: 'USA' },
  { value: 'CA', label: 'Canada' },
  { value: 'MX', label: 'México' },
  { value: 'ZM', label: 'Zambia' },
]);
