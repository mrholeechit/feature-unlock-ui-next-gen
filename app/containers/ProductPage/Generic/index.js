import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { object, func } from 'prop-types';
// import { push } from 'react-router-redux';
import translate from 'utils/translate';
import { connect } from 'react-redux';
import Container from 'components/Container';
import { createStructuredSelector } from 'reselect';
import { convertInputToField } from 'utils/form';
import { length, equals } from 'ramda';
import { makeSelectApp } from 'containers/App/selectors';
import CustomerReferenceConfirmation from '../components/CustomerReferenceConfirmation';
import Info from '../components/Info';
import NotFound from '../components/NotFound';
import CustomerDetails from '../components/CustomerDetails';
import Subscriptions from '../components/Subscriptions';
import Upgrades from '../components/Upgrades';
import Purchases from '../components/Purchases';
import Features from '../components/Features';
import { makeSelectProduct } from '../selectors';
import { fetchProduct, calculateExpirationDate, toggleFetch, toggleFeature, changeMachineLocation, resourceCall, toggleCustomerReferenceConfirmation, toggleRenew, changeCustomerReference, checkout, routeToCheckout } from './actions';

export class GenericProductPage extends PureComponent {
  static propTypes = {
    data: object,
    app: object,
    location: object,
    onFetch: func,
    onToggleFetch: func,
    onChangeActivationDate: func,
    onToggleFeature: func,
    onToggleRenew: func,
    onClickOption: func,
    onChangeCustomerReference: func,
    onChangeMachineLocation: func,
    onCheckout: func,
    onRouteToCheckout: func,
    onToggleCustomerReferenceConfirmation: func,
  }

  componentDidMount() {
    const {
      onFetch,
      onToggleFetch,
      location: {
        query: {
          serialNumber,
          vendor,
        },
      },
      data: {
        shouldFetch,
      },
    } = this.props;

    if (shouldFetch) {
      onFetch({ serialNumber, vendor });
    } else {
      onToggleFetch();
    }
  }

  componentWillReceiveProps({
    onFetch,
    location: {
      query: {
        serialNumber,
        vendor,
      },
    },

  }) {
    const {
      location: {
        query: {
          serialNumber: previousSerialNumber,
        },
      },
    } = this.props;

    if (serialNumber !== previousSerialNumber) {
      onFetch({ serialNumber, vendor });
    }
  }

  render() {
    const {
      data: {
        vendor,
        serialNumber,
        product: {
          NT03AGCOserialNumber,
          vin,
          subscriptions,
          upgrades,
          purchases,
        },
        error,
        cart,
        customerReference,
        customerReferenceError,
        machineLocation,
        showCustomerReferenceConfirmation,
        canSubmit,
      },
      app: {
        dealer: {
          regionCode,
        },
      },
      onChangeActivationDate,
      onToggleFeature,
      onToggleRenew,
      onClickOption,
      onChangeCustomerReference,
      onChangeMachineLocation,
      onCheckout,
      onRouteToCheckout,
      onToggleCustomerReferenceConfirmation,
    } = this.props;

    const isNT03 = equals(vendor.value, 'NT03');
    return (
      <Container>
        <Choose>
          <When condition={error}>
            <Helmet>
              <title>{translate('PRODUCT')}</title>
            </Helmet>
            <NotFound serialNumber={serialNumber} />
          </When>
          <Otherwise>
            <Helmet>
              <title>{translate('PRODUCT')}</title>
            </Helmet>
            <Info
              title={vendor.title}
              image={vendor.image}
              description={vendor.description}
              serialNumber={isNT03 ? NT03AGCOserialNumber : serialNumber}
              vin={vin}
            />
            <Features
              purchasesComponent={
                <If condition={length(purchases)}>
                  <Purchases
                    items={purchases}
                    onClickOption={onClickOption}
                    onToggleRenew={onToggleRenew}
                    cart={cart}
                  />
                </If>
              }
              upgradesComponent={
                <If condition={length(upgrades)}>
                  <Upgrades
                    items={upgrades}
                    cart={cart}
                    onToggleFeature={onToggleFeature}
                  />
                </If>
              }
              subscriptionsComponent={
                <If condition={length(subscriptions)}>
                  <Subscriptions
                    items={subscriptions}
                    cart={cart}
                    onChangeActivationDate={onChangeActivationDate}
                    onToggleFeature={onToggleFeature}
                  />
                </If>
              }
            />
            <CustomerReferenceConfirmation
              isOpen={showCustomerReferenceConfirmation}
              onClose={onToggleCustomerReferenceConfirmation}
              onConfirm={onRouteToCheckout}
              items={cart}
            />
            <CustomerDetails
              regionCode={regionCode}
              machineLocation={machineLocation}
              customerReference={customerReference}
              customerReferenceError={customerReferenceError}
              onChangeMachineLocation={onChangeMachineLocation}
              onChangeCustomerReference={onChangeCustomerReference}
              onSubmit={onCheckout}
              canSubmit={canSubmit}
            />
          </Otherwise>
        </Choose>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeSelectProduct(),
  app: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    onFetch: ({ serialNumber, vendor }) => dispatch(fetchProduct({ serialNumber, vendor })),
    onChangeActivationDate: ({ id, event }) => dispatch(calculateExpirationDate({ id, ...convertInputToField(event) })),
    onToggleFeature: (id) => dispatch(toggleFeature(id)),
    onToggleFetch: () => dispatch(toggleFetch()),
    onToggleRenew: (id) => dispatch(toggleRenew(id)),
    onClickOption: (resource) => dispatch(resourceCall(resource)),
    onChangeCustomerReference: (event, regionCode) => dispatch(changeCustomerReference(convertInputToField(event), regionCode)),
    onChangeMachineLocation: (event) => dispatch(changeMachineLocation(convertInputToField(event))),
    onCheckout: () => dispatch(checkout()),
    onRouteToCheckout: () => dispatch(routeToCheckout()),
    onToggleCustomerReferenceConfirmation: () => dispatch(toggleCustomerReferenceConfirmation()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GenericProductPage);
