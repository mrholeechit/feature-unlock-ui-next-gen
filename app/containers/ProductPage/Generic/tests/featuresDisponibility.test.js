import React from 'react';
import { expect } from 'chai';
import { render } from 'enzyme';
import Subscriptions from '../../components/Subscriptions';
import { featuresAvailablesAndUnavailables } from './fixture';

describe('<Generic Product page />', () => {
  describe('<Subscriptions showing available and unavailable features  />', () => {
    it('Should show 9 features but only 6 available for purchase', () => {
      const component = (
        <Subscriptions items={featuresAvailablesAndUnavailables} cart={[]} />
      );

      const wrapper = render(component);
      expect(wrapper.find('.rt-tr-group')).to.have.length(9);
      expect(wrapper.find('[type=checkbox]')).to.have.length(6);
    });
  });
});
