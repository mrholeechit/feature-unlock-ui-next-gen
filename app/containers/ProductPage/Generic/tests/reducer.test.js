import { fromJS } from 'immutable';
import { expect } from 'chai';
import reducer from '../reducer';
import { purchase, subscription, upgrade } from './fixture';
import { TOGGLE_FEATURE, TOGGLE_RENEW, PRODUCT_FETCH_REQUESTED } from '../constants';
import { toggleFeature, toggleRenew, fetchProduct } from '../actions';

const initialState = fromJS({
  product: {
    purchases: [],
    subscriptions: [],
    upgrades: [],
    deviceFeatures: [],
    orders: [],
  },
  acm: {
    subscriptions: [],
  },
  serialNumber: '',
  vendor: {
    value: '',
    label: '',
    description: '',
    image: '',
  },
  cart: [],
});

describe('Generic reducers', () => {
  it('Selecting an upgrade should add the respective ID to the cart', () => {
    const stateWithUpgrade = initialState.setIn(['product', 'upgrades', '0'], upgrade);
    const stateWithToggle = reducer[TOGGLE_FEATURE](stateWithUpgrade, toggleFeature(upgrade.id));
    const cartState = stateWithToggle.get('cart').toJS();
    expect(cartState[0].id).to.be.equal('fcb3b82a-12da-4cd5-a9f6-68b194d081e5');
  });
  it('Selecting a subscription should add the respective ID to the cart', () => {
    const stateWithSubscription = initialState
      .setIn(['product', 'subscriptions', '0'], subscription);
    const stateWithToggle = reducer[TOGGLE_FEATURE](stateWithSubscription, toggleFeature(subscription.id));
    const cartState = stateWithToggle.get('cart').toJS();
    expect(cartState[0].id).to.be.equal('b222c411-66c1-4270-a8c6-1febc04d5ba0');
  });
  it('Selecting a renew and a subscription should add the respective ID to the cart', () => {
    const stateWithPurchase = initialState
      .setIn(['product', 'purchases', '0'], purchase);
    const stateWithToggle = reducer[TOGGLE_RENEW](stateWithPurchase, toggleRenew(purchase.id));
    const cartState = stateWithToggle.get('cart').toJS();
    expect(cartState[0].id).to.be.equal('e3b46022-2526-4906-a82f-e6c201742d2b');
  });

  it('Searching a serial number should change it\'s value to uppercase', () => {
    const fakeSerialNumber = 'abc123';
    const state = reducer[PRODUCT_FETCH_REQUESTED](initialState, fetchProduct({ serialNumber: fakeSerialNumber, vendor: 'NovAtel' }));
    const { serialNumber } = state.toJS();
    expect(serialNumber).to.be.equal(fakeSerialNumber.toUpperCase());
  });
});
