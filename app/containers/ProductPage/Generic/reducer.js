import { fromJS } from 'immutable';
import { reject, append, propEq, find, pathOr, map, merge, head, any, propOr, prop } from 'ramda';
import addDays from 'date-fns/add_days';
import addMonths from 'date-fns/add_months';
import addYears from 'date-fns/add_years';
import { format } from 'date-fns';
import { DATE_FORMAT, DATE_TIME_FORMAT } from 'utils/constants';
import { canSubmit } from '../utils';
import { PRODUCT_FETCH_REQUESTED, UPDATE_CART, TOGGLE_CUSTOMER_REFERENCE_CONFIRMATION, CUSTOMER_REFERENCE_ERROR, PRODUCT_FETCH_SUCCEEDED, CHECKOUT, CHANGE_MACHINE_LOCATION, CHANGE_ACTIVATION_DATE, TOGGLE_FEATURE, TOGGLE_RENEW, PRODUCT_FETCH_FAILED, CHANGE_CUSTOMER_REFERENCE, TOGGLE_FETCH } from './constants';

const findItem = (id, items) => find(propEq('id', id), items);

const removeItem = (id, items) => reject(propEq('id', id), items);

const hasItem = (id, items) => any(propEq('id', id), items);

const toggleItem = ({ id, ...data }, items) => hasItem(id, items) ? removeItem(id, items) : [{ id, ...data }, ...items];

const unitOperations = {
  day: addDays,
  month: addMonths,
  year: addYears,
};

const calculateExpirationDate = (activationDate, feature) => {
  const unit = pathOr(null, ['calendarExpiration', 'unit'], feature);
  const value = pathOr(null, ['calendarExpiration', 'value'], feature);

  if (!activationDate || (!unit && !value)) return '';

  return format(prop(unit, unitOperations)(activationDate, value), DATE_FORMAT);
};

const adjustActivationType = ({ activationDate, expirationDate, ...feature }) => ({
  ...feature,
  immediateActivation: !activationDate,
  activationDate: !activationDate ? '' : activationDate,
  expirationDate: calculateExpirationDate(activationDate || new Date(), feature),
});

const addActivationDate = ({
  id,
  activationDate,
  cart,
  features,
}) => {
  const feature = findItem(id, features);
  const item = findItem(id, cart);
  const expirationDate = calculateExpirationDate(activationDate || new Date(), feature);

  return append({
    ...item,
    activationDate: activationDate ? format(activationDate, DATE_TIME_FORMAT) : '',
    expirationDate,
  }, removeItem(id, cart));
};

export default {
  [PRODUCT_FETCH_REQUESTED]: (state, action) => state
    .set('serialNumber', action.serialNumber.toUpperCase())
    .set('error', false)
    .setIn(['am50', 'showWarning'], true)
    .set('showCustomerReferenceConfirmation', false)
    .set('canSubmit', canSubmit(state.toJS()))
    .setIn(['vendor', 'value'], action.vendor),
  [PRODUCT_FETCH_SUCCEEDED]: (state, action) => state
    .set('vendor', fromJS(action.vendor))
    .set('cart', fromJS([]))
    .set('customerReference', '')
    .set('customerReferenceError', '')
    .set('canSubmit', false)
    .set('machineLocation', action.machineLocation)
    .set('product', fromJS(action.product)),
  [PRODUCT_FETCH_FAILED]: (state, __, initialState) => initialState
    .set('error', true)
    .set('serialNumber', state.get('serialNumber')),
  [CHANGE_ACTIVATION_DATE]: (state, action) => {
    const features = state.getIn(['product', 'subscriptions']).toJS();
    const activationDate = head(action.value);

    const nextState = state
      .set('cart', fromJS(addActivationDate({
        id: action.id,
        activationDate,
        cart: state.get('cart').toJS(),
        features,
      })));

    return nextState
      .set('canSubmit', canSubmit(nextState.toJS()));
  },
  [TOGGLE_FETCH]: (state) => state
    .set('shouldFetch', !state.get('shouldFetch')),
  [TOGGLE_FEATURE]: (state, action) => {
    const cart = state.get('cart').toJS();

    const feature = findItem(action.id, [
      ...state.getIn(['product', 'subscriptions']).toJS(),
      ...state.getIn(['product', 'purchases']).toJS(),
      ...state.getIn(['product', 'upgrades']).toJS(),
      ...state.getIn(['acm', 'subscriptions']).toJS(),
    ]);

    const nextState = state
      .set('cart', fromJS(toggleItem({
        id: action.id,
        expirationDate: calculateExpirationDate(new Date(), feature),
      }, cart)));

    return nextState
      .set('canSubmit', canSubmit(nextState.toJS()));
  },
  [TOGGLE_RENEW]: (state, action) => {
    const purchases = state.getIn(['product', 'purchases']).toJS();
    const feature = propOr({}, 'feature', findItem(action.id, purchases));
    const newActivationDate = format(new Date(), DATE_FORMAT);

    const cart = toggleItem({
      id: action.id,
      activationDate: newActivationDate,
      expirationDate: calculateExpirationDate(newActivationDate, feature),
    }, state.get('cart').toJS());

    const nextState = state
      .set('cart', fromJS(cart));

    return nextState
      .set('canSubmit', canSubmit(nextState.toJS()));
  },
  [CHANGE_CUSTOMER_REFERENCE]: (state, action) => state
    .set('customerReference', action.value),
  [CHANGE_MACHINE_LOCATION]: (state, action) => {
    const nextState = state
      .set('machineLocation', action.value);

    return nextState
      .set('canSubmit', canSubmit(nextState.toJS()));
  },
  [CUSTOMER_REFERENCE_ERROR]: (state, action) => {
    const nextState = state
      .set('customerReferenceError', action.message);

    return nextState.set('canSubmit', canSubmit(nextState.toJS()));
  },
  [UPDATE_CART]: (state, action) => state
    .set('cart', fromJS(action.cart)),
  [TOGGLE_CUSTOMER_REFERENCE_CONFIRMATION]: (state) => state
    .set('showCustomerReferenceConfirmation', !state.get('showCustomerReferenceConfirmation')),
  [CHECKOUT]: (state) => {
    const cart = map(({ activationDate, expirationDate, ...item }) => {
      const feature = findItem(item.id, [
        ...state.getIn(['product', 'subscriptions']).toJS(),
        ...state.getIn(['product', 'purchases']).toJS(),
        ...state.getIn(['product', 'upgrades']).toJS(),
        ...state.getIn(['acm', 'subscriptions']).toJS(),
      ]);

      return merge({
        ...item,
        machineLocation: state.get('machineLocation'),
        equipmentSerialNumber: state.get('serialNumber'),
      }, adjustActivationType({ ...feature, expirationDate, activationDate }));
    }, state.get('cart').toJS());

    return state
      .set('cart', fromJS(cart));
  },
};
