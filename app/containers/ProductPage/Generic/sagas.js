import { call, select, put, takeLatest, fork } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { transformFeatures, transformVendor } from 'transformers/features';
import API from 'api';
import { prop, map, propOr, length, merge, __ } from 'ramda';
import { toggleLoading } from 'containers/App/actions';
import { makeSelectApp } from 'containers/App/selectors';
import validate, { RULES } from 'utils/validations';
import { PRODUCT_FETCH_REQUESTED, CHECKOUT, RESOURCE_CALL, ROUTE_TO_CHECKOUT, CHANGE_CUSTOMER_REFERENCE } from './constants';
import { hideCustomerReferenceError, productFetchSucceeded, productFetchFailed, showCustomerReferenceError, toggleCustomerReferenceConfirmation, updateCart } from './actions';
import { makeSelectProduct } from '../selectors';
import { resetToDefaultValues } from '../../CheckoutPage/actions';

function* mapOptionsToDeviceFeatures(feature) {
  const options = yield call(API.features.actions, { id: prop('id', feature) });
  return {
    ...feature,
    options,
  };
}

function* fetchProduct() {
  yield put(toggleLoading(true));

  try {
    const { serialNumber, vendor: { value: vendor } } = yield select(makeSelectProduct());
    const { dealer: { countryCode } } = yield select(makeSelectApp());

    const data = transformFeatures(yield call(API.features.info, { serialNumber, vendor }), vendor);
    const purchases = propOr([], 'purchases', data);

    data.purchases = yield map(
      mapOptionsToDeviceFeatures,
      purchases
    );

    const hasFeatures = (type) => length(propOr([], type, data));

    if (!hasFeatures('purchases') && !hasFeatures('subscriptions') && !hasFeatures('upgrades')) {
      throw new Error('Doesn\'t have features.');
    }

    yield put(productFetchSucceeded({
      product: data,
      machineLocation: countryCode,
      vendor: transformVendor(serialNumber, vendor),
    }));
  } catch (e) {
    yield put(productFetchFailed());
  } finally {
    yield put(toggleLoading(false));
  }
}

function* resourceCall({ method, resource }) { // eslint-disable-line no-unused-vars
  yield call(API.utils.call, { method, resource });
  yield fork(fetchProduct);
}

function* existCustomerReference({ customerReference, dealerCode }) {
  try {
    yield validate(RULES.EXIST_CUSTOMER_REFERENCE, 'EAME', { customerReference, dealerCode });

    return false;
  } catch (e) {
    return true;
  }
}

const customerReferenceSuffix = (customerReference) => {
  let i = 0;

  return () => `${customerReference}_${++i}`; // eslint-disable-line
};


function* routeToCheckoutPage() {
  yield put(resetToDefaultValues());
  yield put(push({
    pathname: 'checkout',
  }));
}

function* checkout() {
  yield put(toggleLoading(true));

  const { dealer: { regionCode, code: dealerCode } } = yield select(makeSelectApp());
  const { cart, customerReference } = yield select(makeSelectProduct()); //eslint-disable-line
  const cartWithCustomerReference = map(merge(__, { customerReference }), cart);

  yield put(updateCart(cartWithCustomerReference));

  if (regionCode === 'EAME') {
    try {
      yield validate(RULES.EXIST_CUSTOMER_REFERENCE, 'EAME', { customerReference, dealerCode });

      if (length(cartWithCustomerReference) === 1) {
        yield routeToCheckoutPage();
      } else {
        const generateCustomerReference = customerReferenceSuffix(customerReference);

        /* eslint-disable */
        for (let key in cartWithCustomerReference) {
          if (key == 0) continue;

          do {
            cartWithCustomerReference[key] = merge(cartWithCustomerReference[key], { customerReference: generateCustomerReference() });
          } while (yield existCustomerReference({ customerReference: cartWithCustomerReference[key].customerReference, dealerCode }));
        }
        /* eslint-enable */

        yield put(updateCart(cartWithCustomerReference));
        yield put(toggleCustomerReferenceConfirmation());
      }
    } catch ({ message }) {
      yield put(showCustomerReferenceError(message));
    }
  } else {
    yield routeToCheckoutPage();
  }
  yield put(toggleLoading(false));
}

function* validateCustomerReference() {
  const { dealer: { regionCode } } = yield select(makeSelectApp());
  const { customerReference } = yield select(makeSelectProduct());

  try {
    yield validate([
      RULES.SPACE,
      RULES.ESPECIAL_CHARS,
      RULES.LENGTH,
    ], regionCode, { customerReference });
    yield put(hideCustomerReferenceError());
  } catch ({ message }) {
    yield put(showCustomerReferenceError(message));
  }
}

export default function* genericlisteners() {
  yield takeLatest(PRODUCT_FETCH_REQUESTED, fetchProduct);
  yield takeLatest(RESOURCE_CALL, resourceCall);
  yield takeLatest(CHECKOUT, checkout);
  yield takeLatest(ROUTE_TO_CHECKOUT, routeToCheckoutPage);
  yield takeLatest(CHANGE_CUSTOMER_REFERENCE, validateCustomerReference);
}
