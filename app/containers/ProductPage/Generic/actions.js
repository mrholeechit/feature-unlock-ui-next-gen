import { PRODUCT_FETCH_REQUESTED, UPDATE_CART, TOGGLE_CUSTOMER_REFERENCE_CONFIRMATION, CUSTOMER_REFERENCE_ERROR, CHECKOUT, RESOURCE_CALL, PRODUCT_FETCH_SUCCEEDED, PRODUCT_FETCH_FAILED, CHANGE_ACTIVATION_DATE, CHANGE_MACHINE_LOCATION, TOGGLE_FEATURE, TOGGLE_RENEW, CHANGE_CUSTOMER_REFERENCE, ROUTE_TO_CHECKOUT, TOGGLE_FETCH } from './constants';

export function fetchProduct({
  serialNumber,
  vendor,
}) {
  return {
    type: PRODUCT_FETCH_REQUESTED,
    serialNumber,
    vendor,
  };
}

export function checkout() {
  return {
    type: CHECKOUT,
  };
}

export function toggleFetch() {
  return {
    type: TOGGLE_FETCH,
  };
}

export function routeToCheckout() {
  return {
    type: ROUTE_TO_CHECKOUT,
  };
}

export function updateCart(cart) {
  return {
    type: UPDATE_CART,
    cart,
  };
}

export function showCustomerReferenceError(message) {
  return {
    type: CUSTOMER_REFERENCE_ERROR,
    message,
  };
}

export function hideCustomerReferenceError() {
  return {
    type: CUSTOMER_REFERENCE_ERROR,
    message: '',
  };
}

export function productFetchSucceeded({ vendor, product, machineLocation }) {
  return {
    type: PRODUCT_FETCH_SUCCEEDED,
    vendor,
    product,
    machineLocation,
  };
}

export function productFetchFailed() {
  return {
    type: PRODUCT_FETCH_FAILED,
  };
}

export function calculateExpirationDate({ id, value }) {
  return {
    type: CHANGE_ACTIVATION_DATE,
    id,
    value,
  };
}

export function toggleFeature(id) {
  return {
    type: TOGGLE_FEATURE,
    id,
  };
}

export function toggleCustomerReferenceConfirmation() {
  return {
    type: TOGGLE_CUSTOMER_REFERENCE_CONFIRMATION,
  };
}

export function toggleRenew(id) {
  return {
    type: TOGGLE_RENEW,
    id,
  };
}

export function resourceCall({ method, resource }) {
  return {
    type: RESOURCE_CALL,
    method,
    resource,
  };
}

export function changeCustomerReference({ value }, regionCode) {
  return {
    type: CHANGE_CUSTOMER_REFERENCE,
    value,
    regionCode,
  };
}

export function changeMachineLocation({ value }) {
  return {
    type: CHANGE_MACHINE_LOCATION,
    value,
  };
}
