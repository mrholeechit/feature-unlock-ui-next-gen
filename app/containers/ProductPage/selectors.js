import { createSelector } from 'reselect';

export const makeSelectProduct = () => createSelector(
  (state) => state.get('product'),
  (state) => state.toJS()
);
