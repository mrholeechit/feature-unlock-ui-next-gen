import { CLOSE_AM50_WARNING } from './constants';

export function closeWarning() {
  return {
    type: CLOSE_AM50_WARNING,
  };
}
