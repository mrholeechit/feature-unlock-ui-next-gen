import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { object, func } from 'prop-types';
import translate from 'utils/translate';
import { connect } from 'react-redux';
import Container from 'components/Container';
import { createStructuredSelector } from 'reselect';
import { convertInputToField } from 'utils/form';
import Button from 'components/Button';
import Block from 'components/Block';
import Modal from 'components/Modal';
import { length } from 'ramda';
import { makeSelectApp } from 'containers/App/selectors';
import Info from '../components/Info';
import CustomerDetails from '../components/CustomerDetails';
import CustomerReferenceConfirmation from '../components/CustomerReferenceConfirmation';
import NotFound from '../components/NotFound';
import Subscriptions from '../components/Subscriptions';
import Upgrades from '../components/Upgrades';
import Purchases from '../components/Purchases';
import Features from '../components/Features';
import { makeSelectProduct } from '../selectors';
import { fetchProduct, calculateExpirationDate, toggleFetch, toggleFeature, changeMachineLocation, routeToCheckout, resourceCall, checkout, toggleCustomerReferenceConfirmation, toggleRenew, changeCustomerReference } from '../Generic/actions';
import { closeWarning } from './actions';

export class AM50ProductPage extends PureComponent {
  static propTypes = {
    data: object,
    app: object,
    location: object,
    onFetch: func,
    onChangeActivationDate: func,
    onToggleFeature: func,
    onCloseWarning: func,
    onClickOption: func,
    onToggleRenew: func,
    onChangeCustomerReference: func,
    onChangeMachineLocation: func,
    onToggleFetch: func,
    onCheckout: func,
    onRouteToCheckout: func,
    onToggleCustomerReferenceConfirmation: func,
  }

  componentDidMount() {
    const {
      onFetch,
      onToggleFetch,
      location: {
        query: {
          serialNumber,
          vendor,
        },
      },
      data: {
        shouldFetch,
      },
    } = this.props;

    if (shouldFetch) {
      onFetch({ serialNumber, vendor });
    } else {
      onToggleFetch();
    }
  }

  componentWillReceiveProps({
    onFetch,
    location: {
      query: {
        serialNumber,
        vendor,
      },
    },
  }) {
    const {
      location: {
        query: {
          serialNumber: previousSerialNumber,
        },
      },
    } = this.props;

    if (serialNumber !== previousSerialNumber) {
      onFetch({ serialNumber, vendor });
    }
  }

  render() {
    const {
      data: {
        vendor,
        serialNumber,
        product: {
          subscriptions,
          upgrades,
          purchases,
        },
        cart,
        error,
        customerReference,
        machineLocation,
        customerReferenceError,
        showCustomerReferenceConfirmation,
        canSubmit,
        am50: {
          showWarning,
        },
      },
      app: {
        dealer: {
          regionCode,
        },
      },
      onChangeActivationDate,
      onToggleFeature,
      onCloseWarning,
      onToggleRenew,
      onClickOption,
      onChangeCustomerReference,
      onChangeMachineLocation,
      onCheckout,
      onRouteToCheckout,
      onToggleCustomerReferenceConfirmation,
    } = this.props;
    return (
      <Container>
        <Choose>
          <When condition={error}>
            <Helmet>
              <title>{translate('PRODUCT')}</title>
            </Helmet>
            <NotFound serialNumber={serialNumber} />
          </When>
          <Otherwise>
            <Helmet>
              <title>{translate('PRODUCT')}</title>
            </Helmet>
            <Info
              title={vendor.label}
              image={vendor.image}
              description={vendor.description}
              serialNumber={serialNumber}
            />
            <Modal isOpen={showWarning} onClose={onCloseWarning} small title={translate('ERRORS.WARNING')}>
              <p>{translate('ERRORS.AM50_1_YEAR_WARNING')}</p>
              <Block center>
                <Button color="mineShaft" filled onClick={onCloseWarning}>{translate('ERRORS.DISMISS')}</Button>
              </Block>
            </Modal>
            <Features
              purchasesComponent={
                <If condition={length(purchases)}>
                  <Purchases
                    items={purchases}
                    cart={cart}
                    onClickOption={onClickOption}
                    onToggleRenew={onToggleRenew}
                  />
                </If>
              }
              upgradesComponent={
                <If condition={length(upgrades)}>
                  <Upgrades
                    items={upgrades}
                    cart={cart}
                    onToggleFeature={onToggleFeature}
                  />
                </If>
              }
              subscriptionsComponent={
                <If condition={length(subscriptions)}>
                  <Subscriptions
                    items={subscriptions}
                    cart={cart}
                    onChangeActivationDate={onChangeActivationDate}
                    onToggleFeature={onToggleFeature}
                  />
                </If>
              }
            />
            <CustomerReferenceConfirmation
              isOpen={showCustomerReferenceConfirmation}
              onClose={onToggleCustomerReferenceConfirmation}
              onConfirm={onRouteToCheckout}
              items={cart}
            />
            <CustomerDetails
              customerReferenceError={customerReferenceError}
              regionCode={regionCode}
              machineLocation={machineLocation}
              customerReference={customerReference}
              canSubmit={canSubmit}
              onChangeCustomerReference={onChangeCustomerReference}
              onChangeMachineLocation={onChangeMachineLocation}
              onSubmit={onCheckout}
            />
          </Otherwise>
        </Choose>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeSelectProduct(),
  app: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    onFetch: ({ serialNumber, vendor }) => dispatch(fetchProduct({ serialNumber, vendor })),
    onChangeActivationDate: ({ id, event }) => dispatch(calculateExpirationDate({ id, ...convertInputToField(event) })),
    onToggleFeature: (event) => dispatch(toggleFeature(event)),
    onToggleRenew: (id) => dispatch(toggleRenew(id)),
    onClickOption: (resource) => dispatch(resourceCall(resource)),
    onCloseWarning: () => dispatch(closeWarning()),
    onToggleFetch: () => dispatch(toggleFetch()),
    onChangeCustomerReference: (event, regionCode) => dispatch(changeCustomerReference(convertInputToField(event), regionCode)),
    onChangeMachineLocation: (event) => dispatch(changeMachineLocation(convertInputToField(event))),
    onCheckout: () => dispatch(checkout()),
    onRouteToCheckout: () => dispatch(routeToCheckout()),
    onToggleCustomerReferenceConfirmation: () => dispatch(toggleCustomerReferenceConfirmation()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AM50ProductPage);
