import { CLOSE_AM50_WARNING } from './constants';

export default {
  [CLOSE_AM50_WARNING]: (state) => state.setIn(['am50', 'showWarning'], false),
};
