import {
  fetchProduct,
  // productFetchSucceeded,
  // productFetchFailed,
  // calculateExpirationDate,
  // toggleFeature
} from '../Generic/actions';
import {
  PRODUCT_FETCH_REQUESTED,
  // PRODUCT_FETCH_SUCCEEDED,
  // PRODUCT_FETCH_FAILED,
  // CHANGE_ACTIVATION_DATE,
  // TOGGLE_FEATURE
} from '../Generic/constants';

describe('ProductPage actions', () => {
  describe('An action of fetching a product', () => {
    it('has a vendor and a equipment serial number', () => {
      const serialNumber = '123456';
      const vendor = 'AGCO';
      const expected = {
        serialNumber,
        vendor,
        type: PRODUCT_FETCH_REQUESTED,
      };
      expect(fetchProduct({ serialNumber, vendor })).toEqual(expected);
    });
  });
});
