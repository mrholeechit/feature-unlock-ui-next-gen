import { fromJS } from 'immutable';
import { mergeAll, propOr } from 'ramda';
import genericReducer from './Generic/reducer';
import acmReducer from './ACM/reducer';
import am50Reducer from './AM50/reducer';
import deviceInsightReducer from './DeviceInsight/reducer';

const initialState = fromJS({
  product: {
    serialNumber: '',
    vin: '',
    subscriptions: [],
    purchases: [],
    upgrades: [],
    deviceFeatures: [],
    orders: [],
  },
  acm: {
    subscriptions: [],
    estimatedEngineHour: '',
    estimatedDate: '',
    subscription: '',
    partNumber: '',
    engineHour: '',
    vin: '',
    numberOfYear: '',
    currentEngineHours: 0,
    canUserEnroll: false,
    showSubscriptions: false,
    hasActivatedSubscription: false,
    hasAvailableSubscription: false,
  },
  deviceInsight: {
    showTransferForm: false,
    transferForm: {
      customerName: '',
      customerFirstname: '',
      customerLastname: '',
      customerEmail: '',
      customerPreferredLanguage: '',
      previousSerialNumber: '',
      serialNumber: '',
      fisUsername: '',
      fisPassword: '',
      terms: false,
      canSubmit: false,
    },
  },
  serialNumber: '',
  customerReference: '',
  customerReferenceError: '',
  showCustomerReferenceConfirmation: false,
  machineLocation: '',
  canSubmit: false,
  shouldFetch: true,
  error: false,
  vendor: {
    value: '',
    title: '',
    label: '',
    description: '',
    image: '',
  },
  cart: [],
  am50: {
    showWarning: true,
  },
});

export default (state = initialState, action) => {
  const reducers = mergeAll([
    genericReducer,
    acmReducer,
    am50Reducer,
    deviceInsightReducer,
  ]);

  const reducer = propOr((s) => s, action.type, reducers);

  return reducer(state, action, initialState);
};
