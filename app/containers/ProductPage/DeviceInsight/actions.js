import { OPEN_TRANSFER_FORM, CLOSE_TRANSFER_FORM, CHANGE_FIELD, TRANSFER } from './constants';

export function openTransferForm(id) {
  return {
    type: OPEN_TRANSFER_FORM,
    id,
  };
}

export function closeTransferForm() {
  return {
    type: CLOSE_TRANSFER_FORM,
  };
}

export function transfer() {
  return {
    type: TRANSFER,
  };
}

export function changeField({ name, value }) {
  return {
    type: CHANGE_FIELD,
    name,
    value,
  };
}
