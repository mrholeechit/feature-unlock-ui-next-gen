import { find, path, propEq, and, not, isEmpty } from 'ramda';
import { OPEN_TRANSFER_FORM, CHANGE_FIELD, CLOSE_TRANSFER_FORM } from './constants';

export default {
  [OPEN_TRANSFER_FORM]: (state, action, initialState) => {
    const feature = find(propEq('id', action.id), state.getIn(['product', 'purchases']).toJS());
    const extraAttributes = path(['order', 'extraAttributes'], feature);

    const transferForm = {
      ...initialState.getIn(['deviceInsight', 'transferForm']).toJS(),
      ...extraAttributes,
      previousSerialNumber: state.get('serialNumber'),
      id: action.id,
    };

    return state
      .setIn(['deviceInsight', 'showTransferForm'], true)
      .mergeIn(['deviceInsight', 'transferForm'], transferForm);
  },
  [CLOSE_TRANSFER_FORM]: (state) => state
    .setIn(['deviceInsight', 'showTransferForm'], false),
  [CHANGE_FIELD]: (state, action) => {
    const nextState = state.setIn(['deviceInsight', 'transferForm', action.name], action.value);
    const canSubmit = and(
      not(isEmpty(nextState.getIn(['deviceInsight', 'transferForm', 'serialNumber']))),
      nextState.getIn(['deviceInsight', 'transferForm', 'terms']) === true
    );

    return nextState.setIn(['deviceInsight', 'transferForm', 'canSubmit'], canSubmit);
  },
};
