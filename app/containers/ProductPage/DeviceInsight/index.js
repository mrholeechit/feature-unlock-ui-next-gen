import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { object, func } from 'prop-types';
import translate from 'utils/translate';
import { connect } from 'react-redux';
import Container from 'components/Container';
import { createStructuredSelector } from 'reselect';
import { convertInputToField } from 'utils/form';
import { length } from 'ramda';
import Modal from 'components/Modal';
import { makeSelectApp } from 'containers/App/selectors';
import Info from '../components/Info';
import CustomerReferenceConfirmation from '../components/CustomerReferenceConfirmation';
import TransferForm from './TransferForm';
import CustomerDetails from '../components/CustomerDetails';
import NotFound from '../components/NotFound';
import Subscriptions from '../components/Subscriptions';
import Upgrades from '../components/Upgrades';
import Purchases from '../components/Purchases';
import Features from '../components/Features';
import { openTransferForm, closeTransferForm, transfer, changeField } from './actions';
import { makeSelectProduct } from '../selectors';
import { fetchProduct, calculateExpirationDate, toggleFetch, toggleFeature, routeToCheckout, resourceCall, changeMachineLocation, checkout, toggleCustomerReferenceConfirmation, toggleRenew, changeCustomerReference } from '../Generic/actions';

export class DeviceInsightProductPage extends PureComponent {
  static propTypes = {
    data: object,
    app: object,
    location: object,
    onFetch: func,
    onChangeActivationDate: func,
    onToggleFeature: func,
    onClickOption: func,
    onToggleRenew: func,
    onClickTransfer: func,
    onCloseTransferForm: func,
    onToggleFetch: func,
    onSubmitTransferForm: func,
    onChangeTransferFormField: func,
    onChangeCustomerReference: func,
    onChangeMachineLocation: func,
    onCheckout: func,
    onRouteToCheckout: func,
    onToggleCustomerReferenceConfirmation: func,
  }

  componentDidMount() {
    const {
      onFetch,
      onToggleFetch,
      location: {
        query: {
          serialNumber,
          vendor,
        },
      },
      data: {
        shouldFetch,
      },
    } = this.props;

    if (shouldFetch) {
      onFetch({ serialNumber, vendor });
    } else {
      onToggleFetch();
    }
  }

  componentWillReceiveProps({
    onFetch,
    location: {
      query: {
        serialNumber,
        vendor,
      },
    },
  }) {
    const {
      location: {
        query: {
          serialNumber: previousSerialNumber,
        },
      },
    } = this.props;

    if (serialNumber !== previousSerialNumber) {
      onFetch({ serialNumber, vendor });
    }
  }

  render() {
    const {
      data: {
        vendor,
        serialNumber,
        product: {
          subscriptions,
          upgrades,
          purchases,
        },
        deviceInsight: {
          showTransferForm,
          transferForm,
        },
        cart,
        error,
        machineLocation,
        canSubmit,
        customerReference,
        customerReferenceError,
        showCustomerReferenceConfirmation,
      },
      app: {
        dealer: {
          regionCode,
        },
      },
      onChangeActivationDate,
      onToggleFeature,
      onToggleRenew,
      onClickOption,
      onClickTransfer,
      onCloseTransferForm,
      onSubmitTransferForm,
      onChangeTransferFormField,
      onChangeCustomerReference,
      onChangeMachineLocation,
      onCheckout,
      onRouteToCheckout,
      onToggleCustomerReferenceConfirmation,
    } = this.props;

    return (
      <Container>
        <Choose>
          <When condition={error}>
            <Helmet>
              <title>{translate('PRODUCT')}</title>
            </Helmet>
            <NotFound serialNumber={serialNumber} />
          </When>
          <Otherwise>
            <Helmet>
              <title>{translate('PRODUCT')}</title>
            </Helmet>
            <Info
              title={vendor.title}
              image={vendor.image}
              description={vendor.description}
              serialNumber={serialNumber}
            />
            <Modal
              isOpen={showTransferForm}
              title={translate('CREDENTIALS.TRANSFER_DI_SUBSCRIPTION')}
              onClose={onCloseTransferForm}
            >
              <TransferForm
                values={transferForm}
                onChange={onChangeTransferFormField}
                onCheckout={onSubmitTransferForm}
                onCancel={onCloseTransferForm}
              />
            </Modal>
            <Features
              purchasesComponent={
                <If condition={length(purchases)}>
                  <Purchases
                    items={purchases}
                    onClickOption={onClickOption}
                    onToggleRenew={onToggleRenew}
                    onClickTransfer={onClickTransfer}
                    cart={cart}
                  />
                </If>
              }
              upgradesComponent={
                <If condition={length(upgrades)}>
                  <Upgrades
                    items={upgrades}
                    cart={cart}
                    onToggleFeature={onToggleFeature}
                  />
                </If>
              }
              subscriptionsComponent={
                <If condition={length(subscriptions)}>
                  <Subscriptions
                    items={subscriptions}
                    cart={cart}
                    onChangeActivationDate={onChangeActivationDate}
                    onToggleFeature={onToggleFeature}
                  />
                </If>
              }
            />
            <CustomerReferenceConfirmation
              isOpen={showCustomerReferenceConfirmation}
              onClose={onToggleCustomerReferenceConfirmation}
              onConfirm={onRouteToCheckout}
              items={cart}
            />
            <CustomerDetails
              customerReferenceError={customerReferenceError}
              regionCode={regionCode}
              canSubmit={canSubmit}
              machineLocation={machineLocation}
              customerReference={customerReference}
              onChangeCustomerReference={onChangeCustomerReference}
              onChangeMachineLocation={onChangeMachineLocation}
              onSubmit={onCheckout}
            />
          </Otherwise>
        </Choose>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeSelectProduct(),
  app: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    onFetch: ({ serialNumber, vendor }) => dispatch(fetchProduct({ serialNumber, vendor })),
    onChangeActivationDate: ({ id, event }) => dispatch(calculateExpirationDate({ id, ...convertInputToField(event) })),
    onToggleFeature: (id) => dispatch(toggleFeature(id)),
    onToggleRenew: (id) => dispatch(toggleRenew(id)),
    onClickTransfer: (id) => dispatch(openTransferForm(id)),
    onClickOption: (resource) => dispatch(resourceCall(resource)),
    onCloseTransferForm: () => dispatch(closeTransferForm()),
    onSubmitTransferForm: () => dispatch(transfer()),
    onToggleFetch: () => dispatch(toggleFetch()),
    onChangeTransferFormField: (event) => dispatch(changeField(convertInputToField(event))),
    onChangeCustomerReference: (event, regionCode) => dispatch(changeCustomerReference(convertInputToField(event), regionCode)),
    onChangeMachineLocation: (event) => dispatch(changeMachineLocation(convertInputToField(event))),
    onCheckout: () => dispatch(checkout()),
    onRouteToCheckout: () => dispatch(routeToCheckout()),
    onToggleCustomerReferenceConfirmation: () => dispatch(toggleCustomerReferenceConfirmation()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DeviceInsightProductPage);
