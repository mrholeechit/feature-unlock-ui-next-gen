import React from 'react';
import { func, object } from 'prop-types';
import styled from 'styled-components';
import Button from 'components/Button';
import Block from 'components/Block';
import Title from 'components/Title';
import Link from 'components/Link';
import translate from 'utils/translate';
import Form from 'components/Form';
import Label from 'components/Label';
import Input from 'components/Input';
import Checkbox from 'components/Checkbox';

const StyledButton = styled(Button)`
  margin-right: 10px;

  &:last-child {
    margin-right: 0;
  }
`;

const Field = Label.withComponent('strong');

const Col = styled(Block)`
  width: 50%;
  margin-top: 20px;
  padding-right: 20px;

  &:nth-child(even) {
    padding-right: 0;
  }

  .input {
    width: 100%;
  }
`;

const Divider = styled.hr`
  float: left;
  width: 100%;
  margin: 25px 0;
`;

const TransferForm = ({
  onSubmit,
  onChange,
  onCancel,
  values,
}) => (
  <Form onSubmit={onSubmit}>
    <Title big block>{translate('CREDENTIALS.CURRENT_FMIS_INFO')}</Title>
    <Block>
      <Col>
        <Field>{translate('CREDENTIALS.COMPANY_NAME')}</Field>
        {values.customerName}
      </Col>
      <Col>
        <Field>{translate('ADMIN.DEALER_USERS.NAME')}</Field>
        {values.customerFirstname} {values.customerLastname}
      </Col>
      <Col>
        <Field>{translate('CREDENTIALS.VIN_C_SERIES')}</Field>
        {values.previousSerialNumber}
      </Col>
      <Col>
        <Field>{translate('CREDENTIALS.EMAIL_ADDRESS')}</Field>
        {values.customerEmail}
      </Col>
      <Col>
        <Field>{translate('CREDENTIALS.FMIS_USER_ID')}</Field>
        {values.fisUsername}
      </Col>
      <Col>
        <Field>{translate('CREDENTIALS.FMIS_PASS')}</Field>
        {values.fisPassword}
      </Col>
    </Block>
    <Divider />
    <Title big block>{translate('CREDENTIALS.UPDATE_VIN')}</Title>
    <Block>
      <Col>
        <Label>{translate('CREDENTIALS.VIN_C_SERIES')}</Label>
        <Input placeholder={translate('CREDENTIALS.VIN_C_SERIES')} name="serialNumber" onChange={onChange} value={values.serialNumber} />
      </Col>
      <Col>
        <Link small to={`${window.location.origin}/terms/${values.customerPreferredLanguage}`} underlined target="_blank">{translate('TERMS_AND_CONDITIONS')}</Link>
        <Block>
          <Checkbox name="terms" checked={values.terms} onChange={onChange}>{translate('I_AGREE')}</Checkbox>
        </Block>
      </Col>
    </Block>
    <Divider />
    <Block center>
      <StyledButton color="sanJuan" type="button" onClick={onCancel}>{translate('CANCEL')}</StyledButton>
      <StyledButton color="sanJuan" filled disabled={!values.canSubmit} type="submit">{translate('TRANSFER')}</StyledButton>
    </Block>
  </Form>
);

TransferForm.propTypes = {
  onSubmit: func,
  onChange: func,
  onCancel: func,
  values: object,
};

export default TransferForm;
