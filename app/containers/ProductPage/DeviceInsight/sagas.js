import { call, select, put, takeLatest } from 'redux-saga/effects';
import API from 'api';
import { toggleLoading } from 'containers/App/actions';
import { TRANSFER } from './constants';
import { makeSelectProduct } from '../selectors';
import { closeTransferForm } from './actions';

function* transfer() {
  yield put(toggleLoading(true));

  try {
    const {
      deviceInsight: {
        transferForm: {
          id,
          previousSerialNumber,
          serialNumber,
        },
      },
    } = yield select(makeSelectProduct());

    yield call(API.deviceFeatures.transfer, {
      id,
      from: previousSerialNumber,
      to: serialNumber,
    });

    yield put(closeTransferForm());
  } catch (e) {
    console.error(e); // eslint-disable-line
  } finally {
    yield put(toggleLoading(false));
  }
}

export default function* deviceInsightlisteners() {
  yield takeLatest(TRANSFER, transfer);
}
