import API from 'api';
import { call, select, takeLatest, put } from 'redux-saga/effects';
import { contains, head, path, filter, pathEq } from 'ramda';
import { groupSubscriptions, transformACMPurchases, transformEngineHoursResponse, transformCanUserEnroll, transformVINResponse } from 'transformers/features';
import { PRODUCT_FETCH_SUCCEEDED } from '../Generic/constants';
import { subscriptionsGrouped, purchasesTransformed, engineHoursFetchSuceeded, canUserEnrollFetched, vinFetchSuceeded } from './actions';
import { makeSelectProduct } from '../selectors';

export function* transformSubscriptions({ product: { subscriptions, purchases }, vendor: { value: vendor } }) {
  if (vendor === 'AGCO') {
    const purchase = head(filter(pathEq(['feature', 'vendor'], 'AGCO'), purchases));
    yield put(subscriptionsGrouped(groupSubscriptions(subscriptions, purchase)));
  }
}

export function* transformPuchases({ product: { purchases } }) {
  const vendor = path(['feature', 'vendor'], head(purchases));
  if (contains(vendor, ['AGCO', 'CS310'])) {
    yield put(purchasesTransformed(transformACMPurchases(purchases)));
  }
}

export function* checkCanUserEnroll() {
  const canUserEnroll = transformCanUserEnroll(yield call(API.canUserEnroll.get, {}));
  yield put(canUserEnrollFetched(canUserEnroll));
}

function* fetchCurrentEngineHours() {
  const { serialNumber } = yield select(makeSelectProduct());
  const currentEngineHours = transformEngineHoursResponse(yield call(API.engineHours.get, { serialNumber }));
  yield put(engineHoursFetchSuceeded(currentEngineHours));
}

function* fetchVIN() {
  const { serialNumber } = yield select(makeSelectProduct());
  const vin = transformVINResponse(yield call(API.vin.get, { serialNumber }));
  yield put(vinFetchSuceeded(vin));
}

export default function* acmListeners() {
  yield takeLatest(PRODUCT_FETCH_SUCCEEDED, transformSubscriptions);
  yield takeLatest(PRODUCT_FETCH_SUCCEEDED, fetchCurrentEngineHours);
  yield takeLatest(PRODUCT_FETCH_SUCCEEDED, fetchVIN);
  yield takeLatest(PRODUCT_FETCH_SUCCEEDED, transformPuchases);
  yield takeLatest(PRODUCT_FETCH_SUCCEEDED, checkCanUserEnroll);
}
