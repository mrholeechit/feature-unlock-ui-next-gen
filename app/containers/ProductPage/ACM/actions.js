import { SUBSCRIPTIONS_GROUPED_SUCCEEDED, TOGGLE_FEATURE_WITH_PARTNUMBER, CHANGE_ENGINE_HOURS, TOGGLE_UPGRADE, CHANGE_NUMBER_OF_YEARS, CHANGE_FEATURE_SUBSCRIPTION, ENGINE_HOURS_FETCH_SUCCEEDED, PURCHASES_TRANFORMED_SUCCEEDED, CAN_USER_ENROLL_FETCHED, TOGGLE_RENEW, VIN_FETCH_SUCCEEDED, CLEAR_ACM_SUBSCRIPTION } from './constants';

export function subscriptionsGrouped({ subscriptions }) {
  return ({
    type: SUBSCRIPTIONS_GROUPED_SUCCEEDED,
    subscriptions,
  });
}

export function changeEngineHours(value) {
  return ({
    type: CHANGE_ENGINE_HOURS,
    value,
  });
}

export function changeNumberOfYears(value) {
  return ({
    type: CHANGE_NUMBER_OF_YEARS,
    value,
  });
}

export function toggleFeatureWithPartNumber(id) {
  return ({
    type: TOGGLE_FEATURE_WITH_PARTNUMBER,
    id,
  });
}

export function toggleFeature(description) {
  return {
    type: CHANGE_FEATURE_SUBSCRIPTION,
    description,
  };
}
export function engineHoursFetchSuceeded(currentEngineHours) {
  return ({
    type: ENGINE_HOURS_FETCH_SUCCEEDED,
    currentEngineHours,
  });
}

export function purchasesTransformed({ items, hasActivatedSubscription, hasAvailableSubscription }) {
  return ({
    type: PURCHASES_TRANFORMED_SUCCEEDED,
    purchases: items,
    hasActivatedSubscription,
    hasAvailableSubscription,
  });
}

export function canUserEnrollFetched(canUserEnroll) {
  return ({
    type: CAN_USER_ENROLL_FETCHED,
    canUserEnroll,
  });
}

export function toggleRenew(id) {
  return ({
    type: TOGGLE_RENEW,
    id,
  });
}

export function toggleUpgrade() {
  return ({
    type: TOGGLE_UPGRADE,
  });
}
export function vinFetchSuceeded(vin) {
  return ({
    type: VIN_FETCH_SUCCEEDED,
    vin,
  });
}
export function clearAcmSubscription() {
  return {
    type: CLEAR_ACM_SUBSCRIPTION,
  };
}
