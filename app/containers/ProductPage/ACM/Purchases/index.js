import { length, any, propEq, find } from 'ramda';
import React from 'react';
import { arrayOf, object, func, number, bool, string } from 'prop-types';
import Nav from 'containers/ProductPage/components/Nav';
import translate from 'utils/translate';
import { ORDER_TYPES, VENDORS } from 'utils/constants';
import Block from 'components/Block';
import Alert from 'components/Alert';
import Table from '../../components/Table';
import { createOptions } from '../../utils';
import FeatureInfo from '../../components/FeatureInfo';
const buildEngineHoursTitle = (hasActivatedSubscription) => {
  const text = hasActivatedSubscription ? 'AVAILABLE_ENGINE_HOURS' : 'ENGINE_HOURS';
  return translate(`TABLES.${text}`);
};

const buildExpirationDateTitle = (hasActivatedSubscription) => {
  const text = hasActivatedSubscription ? 'EXPIRATION' : 'NUMBER_OF_YEARS';
  return translate(`TABLES.${text}`);
};

const List = ({
  items,
  cart,
  onToggleRenew,
  currentEngineHours,
  hasActivatedSubscription,
  onClickOption,
  onToggleUpgrade,
  canUserEnroll,
  showSubscriptions,
  vin,
}) => {
  const existsInCart = (id) => any(propEq('id', id), cart);

  const fields = [
    {
      field: 'description',
      name: translate('PRODUCT_TABLE.OPTIONS'),
      format: ({ id, feature }) => ( // eslint-disable-line
        <FeatureInfo {... { name: id, ...feature }} withoutCheckbox withoutPadding />
      ),
    },
    {
      field: 'engineHours',
      name: buildEngineHoursTitle(hasActivatedSubscription),
      format: ({ order, feature, expirationEngineHours, expirationDate }) => [ // eslint-disable-line
        <If condition={feature.kind === ORDER_TYPES.subscription}>
          <If condition={hasActivatedSubscription}>
            {expirationEngineHours - currentEngineHours}
          </If>
          <If condition={!hasActivatedSubscription}>
            {feature.engineHours}
          </If>
        </If>,
        <If condition={feature.kind === ORDER_TYPES.upgrade}>
          N/A
        </If>,
      ],
      width: 220,
    },
    {
      field: 'expirationDate',
      name: buildExpirationDateTitle(hasActivatedSubscription),
      format: ({ expirationDate, feature }) => [ // eslint-disable-line
        <If condition={feature.kind === ORDER_TYPES.subscription}>
          <If condition={hasActivatedSubscription}>
            {expirationDate}
          </If>
          <If condition={!hasActivatedSubscription}>
            {feature.calendarExpiration.value}
          </If>
        </If>,
        <If condition={feature.kind === ORDER_TYPES.upgrade}>
          N/A
        </If>,
      ],
      width: 170,
    },
    {
      field: 'activationStatus',
      name: translate('PRODUCT_TABLE.FEATURE_STATUS'),
      format: ({ activationStatus }) => translate(`API.STATUS.${activationStatus}`),
      width: 180,
    },
    {
      field: 'actions',
      name: translate('PRODUCT_TABLE.ACTIONS'),
      format: ({ id, authorizationCode, vendor, ...deviceFeature}) => { // eslint-disable-line
        const options = createOptions({
          onToggleRenew,
          onToggleUpgrade,
          existsInCart: existsInCart(id),
          onClickOption,
          canEnroll: canUserEnroll,
          showSubscriptions,
          vin,
        });

        return (
          <Nav>
            <For each="option" index="idx" of={deviceFeature.options}>
              {options[option.name]({
                authorizationCode,
                id,
                option,
                vendor,
              })}
            </For>
          </Nav>
        );
      },
      width: 170,
    },
  ];

  const cartHasAcm = () => any(propEq('vendor', VENDORS.agco), cart);

  const findAcmCart = () => find(propEq('vendor', VENDORS.agco), cart);

  return [
    <Table noDataText="" minRows={length(items)} items={items} fields={fields} />,
    <If condition={cartHasAcm(cart)}>
      <Block>
        <Alert>
          <strong>* {translate('PRODUCT_PAGE.NEW_EXPIRATION', {
            estimatedDate: findAcmCart(cart).estimatedDate,
            estimatedHour: findAcmCart(cart).estimatedHour,
          })}
          </strong>
        </Alert>
      </Block>
    </If>,
  ];
};

List.propTypes = {
  items: arrayOf(object),
  cart: arrayOf(object),
  onToggleRenew: func,
  currentEngineHours: number,
  hasActivatedSubscription: bool,
  onClickOption: func,
  canUserEnroll: bool,
  showSubscriptions: bool,
  vin: string,
};

export default List;
