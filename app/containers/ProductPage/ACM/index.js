import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { object, func } from 'prop-types';
import translate from 'utils/translate';
import { connect } from 'react-redux';
import Container from 'components/Container';
import { convertInputToField } from 'utils/form';
import { createStructuredSelector } from 'reselect';
import { length, find, isEmpty, propEq, equals, any } from 'ramda';
import { ORDER_TYPES } from 'utils/constants';
import { makeSelectApp } from 'containers/App/selectors';
import Features from '../components/Features';
import Info from '../components/Info';
import CustomerDetails from '../components/CustomerDetails';
import NotFound from '../components/NotFound';
import CustomerReferenceConfirmation from '../components/CustomerReferenceConfirmation';
import Upgrades from '../components/Upgrades';
import Purchases from './Purchases';
import { makeSelectProduct } from '../selectors';
import { fetchProduct, resourceCall, toggleFetch, toggleFeature as toggleFeatureUpgrade, checkout, routeToCheckout, changeCustomerReference, toggleCustomerReferenceConfirmation, changeMachineLocation } from '../Generic/actions';
import Subscriptions from './Subscriptions';
import EnrollWarning from './EnrollWarning';
import { changeEngineHours, changeNumberOfYears, toggleFeature, toggleUpgrade, toggleRenew, toggleFeatureWithPartNumber, clearAcmSubscription } from './actions';

export class ACMProductPage extends PureComponent {
  static propTypes = {
    data: object,
    app: object,
    location: object,
    onFetch: func,
    onClearAcmSubscription: func,
    onToggleFeature: func,
    onChangeEngineHours: func,
    onChangeNumberOfYears: func,
    onClickOption: func,
    onToggleRenew: func,
    onToggleUpgrade: func,
    onToggleFeatureUpgrade: func,
    onToggleFeatureWithPartNumber: func,
    onChangeCustomerReference: func,
    onChangeMachineLocation: func,
    onCheckout: func,
    onToggleFetch: func,
    onRouteToCheckout: func,
    onToggleCustomerReferenceConfirmation: func,
  }

  componentDidMount() {
    const {
      onFetch,
      onToggleFetch,
      onClearAcmSubscription,
      location: {
        query: {
          serialNumber,
          vendor,
        },
      },
      data: {
        shouldFetch,
      },
    } = this.props;

    if (shouldFetch) {
      onClearAcmSubscription();
      onFetch({ serialNumber, vendor });
    } else {
      onToggleFetch();
    }
  }

  componentWillReceiveProps({
    onFetch,
    onToggleFeatureWithPartNumber,
    onClearAcmSubscription,
    location: {
      query: {
        serialNumber,
        vendor,
      },
    },
    data: {
      acm: {
        partNumber,
        engineHour,
        numberOfYear,
      },
      product: {
        subscriptions,
      },
    },
  }) {
    const {
      location: {
        query: {
          serialNumber: previousSerialNumber,
        },
      },
      data: {
        acm: {
          partNumber: previousPartNumber,
          engineHour: previousEngineHour,
          numberOfYear: previousNumberOfYear,
        },
      },
    } = this.props;

    const hasChangedEngineHours = engineHour !== previousEngineHour;
    const hasChangedNumberOfYears = numberOfYear !== previousNumberOfYear;
    const shouldAddSubscriptionToCart = !isEmpty(partNumber) && !equals(partNumber, previousPartNumber);
    const shouldRemoveSubscriptionFromCart = (isEmpty(partNumber) && !isEmpty(previousPartNumber)) || ((!isEmpty(previousPartNumber) && (hasChangedEngineHours || hasChangedNumberOfYears)));

    if (serialNumber !== previousSerialNumber) {
      onClearAcmSubscription();
      onFetch({ serialNumber, vendor });
    }

    if (shouldAddSubscriptionToCart) {
      const { id } = find(propEq('partNumber', partNumber), subscriptions);
      onToggleFeatureWithPartNumber(id);
    }

    if (shouldRemoveSubscriptionFromCart) {
      const { id } = find(propEq('partNumber', previousPartNumber), subscriptions);
      onToggleFeatureWithPartNumber(id);
    }
  }

  render() {
    const {
      data: {
        vendor,
        serialNumber,
        product: {
          NT03AGCOserialNumber,
          vin,
          upgrades,
          purchases,
        },
        error,
        cart,
        acm,
        customerReference,
        machineLocation,
        canSubmit,
        customerReferenceError,
        showCustomerReferenceConfirmation,
      },
      app: {
        dealer: {
          regionCode,
        },
      },
      onToggleFeature,
      onChangeEngineHours,
      onChangeNumberOfYears,
      onClickOption,
      onToggleRenew,
      onToggleUpgrade,
      onToggleFeatureUpgrade,
      onChangeCustomerReference,
      onChangeMachineLocation,
      onCheckout,
      onRouteToCheckout,
      onToggleCustomerReferenceConfirmation,
    } = this.props;

    const hasBoughtSubscription = any((purchase) => purchase.feature.kind === ORDER_TYPES.subscription && purchase.active);
    return (
      <Container>
        <Choose>
          <When condition={error}>
            <Helmet>
              <title>{translate('PRODUCT')}</title>
            </Helmet>
            <NotFound serialNumber={serialNumber} />
          </When>
          <Otherwise>
            <Helmet>
              <title>{translate('PRODUCT')}</title>
            </Helmet>
            <Info
              title={vendor.label}
              image={vendor.image}
              description={vendor.description}
              serialNumber={NT03AGCOserialNumber}
              vin={vin}
            />
            <If condition={acm.hasAvailableSubscription}>
              <EnrollWarning>{translate('PRODUCT_PAGE.ENROLLMENT_MESSAGE')}</EnrollWarning>
            </If>
            <Features
              purchasesComponent={
                <If condition={length(purchases)}>
                  <Purchases
                    items={purchases}
                    cart={cart}
                    currentEngineHours={acm.currentEngineHours}
                    hasActivatedSubscription={acm.hasActivatedSubscription}
                    onClickOption={onClickOption}
                    canUserEnroll={acm.canUserEnroll}
                    onToggleRenew={onToggleRenew}
                    onToggleUpgrade={onToggleUpgrade}
                    showSubscriptions={acm.showSubscriptions}
                    vin={acm.vin}
                  />
                </If>
              }
              upgradesComponent={
                <If condition={length(upgrades)}>
                  <Upgrades
                    items={upgrades}
                    cart={cart}
                    onToggleFeature={onToggleFeatureUpgrade}
                  />
                </If>
              }
              subscriptionsComponent={
                <If condition={(length(acm.subscriptions) && !hasBoughtSubscription(purchases)) || acm.showSubscriptions}>
                  <Subscriptions
                    items={acm.subscriptions}
                    selectedItem={acm}
                    onToggleFeature={onToggleFeature}
                    onChangeEngineHours={onChangeEngineHours}
                    onChangeNumberOfYears={onChangeNumberOfYears}
                    cart={cart}
                  />
                </If>
              }
            />
            <CustomerReferenceConfirmation
              isOpen={showCustomerReferenceConfirmation}
              onClose={onToggleCustomerReferenceConfirmation}
              onConfirm={onRouteToCheckout}
              items={cart}
            />
            <CustomerDetails
              customerReferenceError={customerReferenceError}
              regionCode={regionCode}
              machineLocation={machineLocation}
              customerReference={customerReference}
              onChangeCustomerReference={onChangeCustomerReference}
              canSubmit={canSubmit}
              onSubmit={onCheckout}
              onChangeMachineLocation={onChangeMachineLocation}
            />
          </Otherwise>
        </Choose>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeSelectProduct(),
  app: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    onFetch: ({ serialNumber, vendor }) => dispatch(fetchProduct({ serialNumber, vendor })),
    onToggleFeature: (description, id) => dispatch(toggleFeature(description, id)),
    onToggleFeatureUpgrade: (id) => dispatch(toggleFeatureUpgrade(id)),
    onChangeEngineHours: ({ target: { value } }) => dispatch(changeEngineHours(value)),
    onChangeNumberOfYears: ({ target: { value } }) => dispatch(changeNumberOfYears(value)),
    onClickOption: (resource) => dispatch(resourceCall(resource)),
    onToggleRenew: (id) => dispatch(toggleRenew(id)),
    onToggleUpgrade: () => dispatch(toggleUpgrade()),
    onToggleFetch: () => dispatch(toggleFetch()),
    onToggleFeatureWithPartNumber: (id) => dispatch(toggleFeatureWithPartNumber(id)),
    onChangeCustomerReference: (event, regionCode) => dispatch(changeCustomerReference(convertInputToField(event), regionCode)),
    onChangeMachineLocation: (event) => dispatch(changeMachineLocation(convertInputToField(event))),
    onCheckout: () => dispatch(checkout()),
    onRouteToCheckout: () => dispatch(routeToCheckout()),
    onToggleCustomerReferenceConfirmation: () => dispatch(toggleCustomerReferenceConfirmation()),
    onClearAcmSubscription: () => dispatch(clearAcmSubscription()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ACMProductPage);
