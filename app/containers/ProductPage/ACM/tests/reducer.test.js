import { fromJS } from 'immutable';
import { map, find, propEq } from 'ramda';
import { expect } from 'chai';
import { groupBySubscriptionFixture } from './fixture';
import {
  SUBSCRIPTIONS_GROUPED_SUCCEEDED,
  CHANGE_ENGINE_HOURS,
  CHANGE_NUMBER_OF_YEARS,
  CHANGE_FEATURE_SUBSCRIPTION,
  TOGGLE_FEATURE_WITH_PARTNUMBER,
  VIN_FETCH_SUCCEEDED,
} from '../constants';
import {
  TOGGLE_FEATURE,
} from '../../Generic/constants';
import reducer, { selectPartNumber } from '../reducer';
import genericReducer from '../../Generic/reducer';
import { subscriptionsGrouped, changeEngineHours, changeNumberOfYears, toggleFeature, toggleFeatureWithPartNumber } from '../actions';
import { toggleFeature as genericToggleFeature } from '../../Generic/actions';

const initialState = fromJS({
  product: {
    subscriptions: groupBySubscriptionFixture,
    upgrades: [],
    deviceFeatures: [],
    orders: [],
    purchases: [],
  },
  acm: {
    engineHours: [],
    partNumber: '',
    engineHour: '',
    numberOfYear: '',
    subscription: '',
    subscriptions: [],
  },
  serialNumber: '',
  vendor: {
    value: '',
    label: '',
    description: '',
    image: '',
  },
  cart: [],
});

const groupedAction = () => subscriptionsGrouped({
  subscriptions: [{
    description: 'USA_AGCOMMAND ADVANCED',
    prerequisites: 'To fully utilize an AgCommand Advanced Subscription, the machine must be AgCommand Capable (Available CAN bus connection).',
  }, { description: 'USA_AGCOMMAND STANDARD SUBSCRIPTION', prerequisites: 'To fully utilize an AgCommand Advanced Subscription, the machine must be AgCommand Capable (Available CAN bus connection).' }, { description: 'ADVANCED PLUS', prerequisites: 'To fully utilize an AgCommand Advanced Subscription, the machine must be AgCommand Capable (Available CAN bus connection).' }],
  engineHours: [{
    label: '300',
    value: 300,
  }, {
    label: '600',
    value: 600,
  }, {
    label: '900',
    value: 900,
  }],
  numberOfYears: [{
    label: '1',
    value: 1,
  }],
});

describe('ACM reducer', () => {
  describe('Select part number function', () => {
    it('should select a part number when I select engine hours and number of years', () => {
      const partNumber = selectPartNumber({
        engineHours: 600,
        numberOfYears: 1,
        subscription: 'USA_AGCOMMAND STANDARD SUBSCRIPTION',
        subscriptions: groupBySubscriptionFixture,
      });
      expect(partNumber).to.be.equal('ACP0231430');
    });
    it('should be empty if there isn\'t a number of years selected', () => {
      const partNumber = selectPartNumber({
        engineHours: 600,
        subscription: 'USA_AGCOMMAND STANDARD SUBSCRIPTION',
        subscriptions: groupBySubscriptionFixture,
      });
      expect(partNumber).to.be.equal('');
    });
    it('should be empty if there isn\'t a engine yours selected', () => {
      const partNumber = selectPartNumber({
        numberOfYears: 1,
        subscription: 'USA_AGCOMMAND STANDARD SUBSCRIPTION',
        subscriptions: groupBySubscriptionFixture,
      });
      expect(partNumber).to.be.equal('');
    });
    it('should be empty if there isn\'t a subscription selected', () => {
      const partNumber = selectPartNumber({
        engineHours: 600,
        numberOfYears: 1,
        subscriptions: groupBySubscriptionFixture,
      });
      expect(partNumber).to.be.equal('');
    });
  });
});

describe('ACM reducers', () => {
  describe('Grouping subscriptions', () => {
    it('should fill the fields subscriptions, engineHours and numberOfYears when it receives SUBSCRIPTION_GROUPED_SUCCEEDED', () => {
      const state = reducer[SUBSCRIPTIONS_GROUPED_SUCCEEDED](initialState, groupedAction());
      const acmState = state.get('acm').toJS();
      expect(acmState).to.be.an('object');
      expect(acmState.engineHour).to.be.equal('');
      expect(acmState.subscription).to.be.equal('');
      expect(acmState.partNumber).to.be.equal('');
      expect(acmState.numberOfYear).to.be.equal('');
      expect(acmState.subscriptions).to.have.length(3);
      map((subscription) => {
        expect(subscription).to.have.property('description');
        expect(subscription).to.have.property('prerequisites');
      })(acmState.subscriptions);
    });
  });
  describe('Changing engine hours', () => {
    it('Should set engine hours', () => {
      const state = reducer[CHANGE_ENGINE_HOURS](initialState, changeEngineHours(127));
      const acmState = state.get('acm').toJS();
      expect(acmState.engineHour).to.be.equal(127);
      expect(acmState.numberOfYear).to.be.equal('');
      expect(acmState.partNumber).to.be.equal('');
    });
    it('Since the number of year and the subsription are filled it should select a part number', () => {
      const stateWithNumberOfYear = initialState
        .setIn(['acm', 'numberOfYear'], 1)
        .setIn(['acm', 'subscription'], 'USA_AGCOMMAND STANDARD SUBSCRIPTION');
      const state = reducer[CHANGE_ENGINE_HOURS](stateWithNumberOfYear, changeEngineHours(1200));
      const acmState = state.get('acm').toJS();
      expect(acmState.engineHour).to.be.equal(1200);
      expect(acmState.partNumber).to.be.equal('ACP0231490');
    });
    it('Since engine hours and number of year are filled out should add the partNumber ID to cart ', () => {
      const stateWithNumberOfYear = initialState
        .setIn(['acm', 'numberOfYear'], 1)
        .setIn(['acm', 'subscription'], 'USA_AGCOMMAND STANDARD SUBSCRIPTION');
      const acmState = reducer[CHANGE_ENGINE_HOURS](stateWithNumberOfYear, changeEngineHours(1200));
      const getAcmPartNumber = acmState.get('acm').toJS();
      const getSubscriptions = acmState.getIn(['product', 'subscriptions']).toJS();
      const { id } = find(propEq('partNumber', getAcmPartNumber.partNumber), getSubscriptions);
      const state = reducer[TOGGLE_FEATURE_WITH_PARTNUMBER](acmState, toggleFeatureWithPartNumber(id));
      const cartState = state.get('cart').toJS();
      expect(cartState[0].id).to.be.equal('efebda96-5f57-44b7-b436-1d001a6906ea');
    });
  });
  describe('Changing number of years', () => {
    it('Should set number of year', () => {
      const state = reducer[CHANGE_NUMBER_OF_YEARS](initialState, changeNumberOfYears(2));
      const acmState = state.get('acm').toJS();
      expect(acmState.numberOfYear).to.be.equal(2);
      expect(acmState.engineHour).to.be.equal('');
      expect(acmState.partNumber).to.be.equal('');
    });
    it('Since the engine hours is filled it should select a part number', () => {
      const stateWithNumberOfYear = initialState
        .setIn(['acm', 'engineHour'], 600)
        .setIn(['acm', 'subscription'], 'USA_AGCOMMAND STANDARD SUBSCRIPTION');
      const state = reducer[CHANGE_NUMBER_OF_YEARS](stateWithNumberOfYear, changeNumberOfYears(1));
      const acmState = state.get('acm').toJS();
      expect(acmState.engineHour).to.be.equal(600);
      expect(acmState.partNumber).to.be.equal('ACP0231430');
    });
    it('Since number of year and engine hours are filled ut should add the partNumber ID to cart ', () => {
      const stateWithNumberOfYear = initialState
        .setIn(['acm', 'numberOfYear'], 1)
        .setIn(['acm', 'subscription'], 'USA_AGCOMMAND STANDARD SUBSCRIPTION');
      const acmState = reducer[CHANGE_ENGINE_HOURS](stateWithNumberOfYear, changeEngineHours(1200));
      const getAcmPartNumber = acmState.get('acm').toJS();
      const getSubscriptions = acmState.getIn(['product', 'subscriptions']).toJS();
      const { id } = find(propEq('partNumber', getAcmPartNumber.partNumber), getSubscriptions);
      const state = reducer[TOGGLE_FEATURE_WITH_PARTNUMBER](acmState, toggleFeatureWithPartNumber(id));
      const cartState = state.get('cart').toJS();
      expect(cartState[0].id).to.be.equal('efebda96-5f57-44b7-b436-1d001a6906ea');
    });
  });
  describe('Changing the feature', () => {
    it('Should change the subscription selected', () => {
      const stateWithNumberOfYear = initialState
        .setIn(['acm', 'engineHour'], 600)
        .setIn(['acm', 'subscription'], 'USA_AGCOMMAND STANDARD SUBSCRIPTION');
      const state = reducer[CHANGE_NUMBER_OF_YEARS](stateWithNumberOfYear, changeNumberOfYears(1));
      const auxState = reducer[CHANGE_FEATURE_SUBSCRIPTION](state, toggleFeature('ADVANCED PLUS'));
      const acmState = auxState.get('acm').toJS();
      expect(acmState.numberOfYear).to.be.equal('');
      expect(acmState.engineHour).to.be.equal('');
      expect(acmState.partNumber).to.be.equal('');
      expect(acmState.subscription).to.be.equal('ADVANCED PLUS');
    });
  });
  describe('Selecting an upgrade', () => {
    it('Should add the upgrade to cart', () => {
      const stateWithoutUpgrade = initialState;
      const stateWithUpgrade = genericReducer[TOGGLE_FEATURE](stateWithoutUpgrade, genericToggleFeature('86b1f14c-69d6-4189-abc0-aa91a2e9e7b3'));
      const cartState = stateWithUpgrade.get('cart').toJS();
      expect(cartState[0].id).to.be.equal('86b1f14c-69d6-4189-abc0-aa91a2e9e7b3');
    });
    it('Should remove from the cart unselecting the upgrade', () => {
      const stateWithoutUpgrade = initialState;
      const stateWithUpgrade = genericReducer[TOGGLE_FEATURE](stateWithoutUpgrade, genericToggleFeature('86b1f14c-69d6-4189-abc0-aa91a2e9e7b3'));
      const stateWithUpgradeRemoved = genericReducer[TOGGLE_FEATURE](stateWithUpgrade, genericToggleFeature('86b1f14c-69d6-4189-abc0-aa91a2e9e7b3'));
      const cartState = stateWithUpgradeRemoved.get('cart').toJS();
      expect(cartState.length).to.be.equal(0);
    });
  });
  describe('Setting vin', () => {
    it('Should set vin', () => {
      const stateWithoutVin = initialState
        .setIn(['acm', 'vin'], '');
      const state = reducer[VIN_FETCH_SUCCEEDED](stateWithoutVin, { vin: 'AGC3204982' });
      const acmState = state.get('acm').toJS();
      expect(acmState.vin).to.be.equal('AGC3204982');
    });
  });
});
