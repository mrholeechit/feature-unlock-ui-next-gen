import { fromJS } from 'immutable';
import { expect } from 'chai';
import { path, map } from 'ramda';
import { transformSubscriptions, transformPuchases } from '../sagas';
import { PRODUCT_FETCH_SUCCEEDED } from '../../Generic/constants';
import { SUBSCRIPTIONS_GROUPED_SUCCEEDED } from '../constants';
import { groupBySubscriptionFixture, transformACMPurchases } from './fixture';

const initialState = fromJS({
  product: {
    subscriptions: groupBySubscriptionFixture,
    upgrades: [],
    purchases: [],
    deviceFeatures: [],
    orders: [],
  },
  acm: {
    engineHours: [],
    partNumber: '',
    engineHour: '',
    numberOfYear: '',
    subscription: '',
    subscriptions: [],
  },
  serialNumber: '123',
  vendor: {
    value: 'AGCO',
    label: '',
    description: '',
    image: '',
  },
  cart: [],
});

const groupSubscriptions = () => ({
  type: PRODUCT_FETCH_SUCCEEDED,
  product: initialState.get('product').toJS(),
  vendor: initialState.get('vendor').toJS(),
});

const getPurchases = () => ({
  product: {
    purchases: transformACMPurchases,
  },
});

describe('Testing ACM sagas', () => {
  describe('group subscriptions', () => {
    it('Should group subscriptions by service level', () => {
      const iterator = transformSubscriptions(groupSubscriptions());
      const actualYield = iterator.next().value;
      const state = path(['PUT', 'action'])(actualYield);
      expect(state).to.be.an('object');

      expect(state.subscriptions).to.have.length(3);
      map((subscription) => {
        expect(subscription).to.have.property('description');
        expect(subscription).to.have.property('prerequisites');
      })(state.subscriptions);
      expect(state.type).to.be.equal(SUBSCRIPTIONS_GROUPED_SUCCEEDED);
    });
  });

  describe('transform ACM purchases', () => {
    it('Should transform ACM purchases to get only the active one', () => {
      const iterator = transformPuchases(getPurchases());
      const actualYield = iterator.next().value;
      const state = path(['PUT', 'action'])(actualYield);
      expect(state).to.be.an('object');
      expect(state.hasActivatedSubscription).to.equal(true);
      expect(state.purchases.length).to.equal(2);
    });
  });
});
