import React from 'react';
import { expect } from 'chai';
import { shallow, render } from 'enzyme';
import { ACMProductPage } from '../index';
import Features from '../../components/Features';
import Info from '../../components/Info';
import { acmStateWithoutPurchase, acmStateWithPurchase } from './fixture';

describe('<ACMProductPage />', () => {
  describe('<ACMProductPage check rendering Vendor and Serial number info  />', () => {
    it('Should show acm vendor as the info of the product', () => {
      const component = (
        <ACMProductPage {...acmStateWithoutPurchase} />
      );

      const wrapper = shallow(component, { disableLifecycleMethods: true });
      expect(wrapper.find(Info)).to.have.length(1);
    });
  });

  describe('<ACMProductPage check rendering of products links  />', () => {
    it('Should render upgrade link in purchases list for activated ACM features', () => {
      const component = (
        <ACMProductPage {...acmStateWithPurchase} />
      );

      const wrapper = render(component);
      expect(wrapper.find('.purchases-list .upgrade-option')).to.have.length(1);
    });

    it('Should render deactivate link in purchases list for activated ACM features', () => {
      const component = (
        <ACMProductPage {...acmStateWithPurchase} />
      );

      const wrapper = render(component);
      expect(wrapper.find('.purchases-list .deactivate-option')).to.have.length(1);
    });
  });

  describe('<ACMProductPage check rendering of Subscriptions  />', () => {
    it('Should render subscriptions list, since it doesn\'t have purchases ', () => {
      const component = (
        <ACMProductPage {...acmStateWithoutPurchase} />
      );

      const wrapper = shallow(component, { disableLifecycleMethods: true });
      expect(wrapper.find(Features).prop('subscriptionsComponent')).to.not.equal(null);
    });

    it('Shouldn\'t render subscriptions list, since it has purchases ', () => {
      const component = (
        <ACMProductPage {...acmStateWithPurchase} />
      );

      const wrapper = shallow(component, { disableLifecycleMethods: true });
      expect(wrapper.find(Features).prop('subscriptionsComponent')).to.equal(null);
    });

    it('Should render purchases, since it has purchases', () => {
      const component = (
        <ACMProductPage {...acmStateWithPurchase} />
      );

      const wrapper = shallow(component, { disableLifecycleMethods: true });
      expect(wrapper.find(Features).prop('purchasesComponent')).to.not.equal(null);
    });

    it('Shouldnt render purchases, since it has no purchases', () => {
      const component = (
        <ACMProductPage {...acmStateWithoutPurchase} />
      );

      const wrapper = shallow(component, { disableLifecycleMethods: true });
      expect(wrapper.find(Features).prop('purchasesComponent')).to.equal(null);
    });
  });
});
