import React from 'react';
import styled from 'styled-components';
import theme from 'utils/styles';
import { node } from 'prop-types';

const Description = styled.p`
  font-size: ${theme.font.base};
  line-height: ${theme.font.large};
  font-style: italic;
  margin-bottom: 50px;
`;

const EnrollWarning = ({ children }) => (
  <Description>
    <strong>Warning: </strong>{children}
  </Description>
);

EnrollWarning.propTypes = {
  children: node,
};

export default EnrollWarning;
