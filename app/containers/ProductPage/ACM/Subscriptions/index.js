import React from 'react';
import { arrayOf, object, func } from 'prop-types';
import Select from 'components/Select';
import translate from 'utils/translate';
import { length } from 'ramda';
import Alert from 'components/Alert';
import Block from 'components/Block';
import Table from '../../components/Table';
import FeatureInfo from '../../components/FeatureInfo';

const List = ({
  items,
  onToggleFeature,
  onChangeEngineHours,
  onChangeNumberOfYears,
  selectedItem,
}) => {
  const fields = [
    {
      field: 'description',
      name: translate('PRODUCT_TABLE.OPTIONS'),
      format: ({ description, prerequisites }) => { // eslint-disable-line
        return (
          <FeatureInfo
            available
            description={description}
            onChange={() => onToggleFeature(description)}
            checked={description === selectedItem.subscription}
            {... { name: description, description, prerequisites }}
          />
        );
      },
    },
    {
      field: 'activation',
      name: translate('TABLES.ENGINE_HOURS'),
      format: ({ description, selectableEngineHours }) => ( // eslint-disable-line
        <If condition={description === selectedItem.subscription}>
          <Select
            name="engineHours"
            onChange={onChangeEngineHours}
            placeholder={translate('TABLES.ENGINE_HOURS')}
            options={selectableEngineHours}
            value={selectedItem.engineHour}
          />
        </If>
      ),
      width: 180,
    },
    {
      field: 'expiration',
      name: translate('TABLES.NUMBER_OF_YEARS'),
      width: 180,
      format: ({ description, selectableNumberOfYears }) => ( // eslint-disable-line
        <If condition={description === selectedItem.subscription}>
          <Select
            name="engineHours"
            searchable={false}
            onChange={onChangeNumberOfYears}
            placeholder={translate('TABLES.NUMBER_OF_YEARS')}
            value={selectedItem.numberOfYear}
            options={selectableNumberOfYears}
          />
        </If>
      ),
    },
  ];

  return [
    <Table noDataText="" minRows={length(items)} items={items} fields={fields} />,
    <If condition={selectedItem.partNumber}>
      <Block>
        <Alert>
          <strong>* {translate('PRODUCT_PAGE.PART_NUMBER_IS')} {selectedItem.partNumber}.</strong> {translate('PRODUCT_PAGE.LOGIN_FOR_PRICING')}
        </Alert>
        <Alert>
          <strong>** {translate('PRODUCT_PAGE.NEW_EXPIRATION', {
            estimatedDate: selectedItem.estimatedDate,
            estimatedHour: selectedItem.estimatedEngineHour,
          })}
          </strong>
        </Alert>
      </Block>
    </If>,
  ];
};

List.propTypes = {
  items: arrayOf(object),
  onToggleFeature: func,
  onChangeEngineHours: func,
  onChangeNumberOfYears: func,
  selectedItem: object,
};

export default List;
