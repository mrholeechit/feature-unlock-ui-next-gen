import { fromJS } from 'immutable';
import { format } from 'date-fns';
import addDays from 'date-fns/add_days';
import addMonths from 'date-fns/add_months';
import addYears from 'date-fns/add_years';
import { both, curry, equals, find, path, pipe, propEq, pathOr, propOr, __, prop, any, reject, add, and } from 'ramda';
import { DEVICE_FEATURE_STATUS, DATE_FORMAT } from 'utils/constants';
import { SUBSCRIPTIONS_GROUPED_SUCCEEDED, TOGGLE_FEATURE_WITH_PARTNUMBER, CHANGE_ENGINE_HOURS, TOGGLE_UPGRADE, CHANGE_NUMBER_OF_YEARS, CHANGE_FEATURE_SUBSCRIPTION, ENGINE_HOURS_FETCH_SUCCEEDED, PURCHASES_TRANFORMED_SUCCEEDED, CAN_USER_ENROLL_FETCHED, TOGGLE_RENEW, VIN_FETCH_SUCCEEDED, CLEAR_ACM_SUBSCRIPTION } from './constants';
import { canSubmit } from '../utils';

const unitOperations = {
  day: addDays,
  month: addMonths,
  year: addYears,
};

const filterCalendarExpiration = (numberOfYears) => both(
  pipe(path(['calendarExpiration', 'unit']), equals('year')),
  pipe(path(['calendarExpiration', 'value']), equals(numberOfYears))
);

const propEqCurry = curry(propEq(__));

export const selectPartNumber = ({
  engineHours,
  numberOfYears,
  subscription,
  subscriptions,
}) => pipe(find(both(
  both(
    filterCalendarExpiration(numberOfYears),
    propEqCurry('engineHours', engineHours)
  ),
  propEqCurry('description', subscription)
)), propOr('', 'partNumber'))(subscriptions);

const toggleSubscription = (description, subscription) => description === subscription ? '' : description;

const hasItem = (id, items) => any(propEq('id', id), items);

const findItem = (id, items) => find(propEq('id', id), items);

const removeItem = (id, items) => reject(propEq('id', id), items);

const toggleItem = ({ id, ...data }, items) => hasItem(id, items) ? removeItem(id, items) : [{ id, ...data }, ...items];

const getActiveSubscription = find(and(propEq('activationStatus', DEVICE_FEATURE_STATUS.activated), propEq('active', true)));

const calculateExpirationDate = (activationDate, feature) => {
  const unit = pathOr(null, ['calendarExpiration', 'unit'], feature);
  const value = pathOr(null, ['calendarExpiration', 'value'], feature);

  if (!activationDate || (!unit && !value)) return '';

  return format(prop(unit, unitOperations)(activationDate, value), DATE_FORMAT);
};

export default {
  [SUBSCRIPTIONS_GROUPED_SUCCEEDED]: (state, action) => state
    .setIn(['acm', 'engineHour'], '')
    .setIn(['acm', 'numberOfYear'], '')
    .setIn(['acm', 'partNumber'], '')
    .setIn(['acm', 'subscription'], '')
    .setIn(['acm', 'hasActivatedSubscription'], false)
    .setIn(['acm', 'hasAvailableSubscription'], false)
    .setIn(['acm', 'subscriptions'], fromJS(action.subscriptions)),
  [CHANGE_ENGINE_HOURS]: (state, action) => {
    const activePurchase = getActiveSubscription(state.getIn(['product', 'purchases']));
    const engineHours =
      action.value + (activePurchase ?
        activePurchase.expirationEngineHours :
        state.getIn(['acm', 'currentEngineHours']));
    return state.setIn(['acm', 'engineHour'], action.value)
      .setIn(['acm', 'partNumber'], selectPartNumber({
        engineHours: action.value,
        numberOfYears: state.getIn(['acm', 'numberOfYear']),
        subscription: state.getIn(['acm', 'subscription']),
        subscriptions: state.getIn(['product', 'subscriptions']).toJS(),
      }))
      .setIn(['acm', 'estimatedEngineHour'], engineHours);
  },
  [CHANGE_NUMBER_OF_YEARS]: (state, action) => state
    .setIn(['acm', 'numberOfYear'], action.value)
    .setIn(
      ['acm', 'partNumber'],
      selectPartNumber({
        engineHours: state.getIn(['acm', 'engineHour']),
        numberOfYears: action.value,
        subscription: state.getIn(['acm', 'subscription']),
        subscriptions: state.getIn(['product', 'subscriptions']).toJS(),
      })
    )
    .setIn(['acm', 'estimatedDate'], format(addYears(new Date(), action.value), DATE_FORMAT)),
  [TOGGLE_UPGRADE]: (state) => state
    .setIn(['acm', 'showSubscriptions'], !state.getIn(['acm', 'showSubscriptions'])),
  [TOGGLE_FEATURE_WITH_PARTNUMBER]: (state, action) => {
    const cart = toggleItem({
      id: action.id,
    }, state.get('cart').toJS());

    const nextState = state
      .set('cart', fromJS(cart));
    return nextState
      .set('canSubmit', canSubmit(nextState.toJS()));
  },
  [CHANGE_FEATURE_SUBSCRIPTION]: (state, action) => state
    .setIn(['acm', 'engineHour'], '')
    .setIn(['acm', 'numberOfYear'], '')
    .setIn(['acm', 'partNumber'], '')
    .setIn(['acm', 'subscription'], toggleSubscription(action.description, state.getIn(['acm', 'subscription']))),
  [ENGINE_HOURS_FETCH_SUCCEEDED]: (state, action) => state
    .setIn(['acm', 'currentEngineHours'], action.currentEngineHours),
  [VIN_FETCH_SUCCEEDED]: (state, action) => state
    .setIn(['acm', 'vin'], action.vin),
  [PURCHASES_TRANFORMED_SUCCEEDED]: (state, action) => state
    .setIn(['product', 'purchases'], fromJS(action.purchases))
    .setIn(['acm', 'hasActivatedSubscription'], action.hasActivatedSubscription)
    .setIn(['acm', 'hasAvailableSubscription'], action.hasAvailableSubscription),
  [CAN_USER_ENROLL_FETCHED]: (state, action) => state
    .setIn(['acm', 'canUserEnroll'], action.canUserEnroll),
  [TOGGLE_RENEW]: (state, action) => {
    const purchases = state.getIn(['product', 'purchases']);
    const selectedPurchase = findItem(action.id, purchases);
    const feature = propOr({}, 'feature', selectedPurchase);

    const cart = toggleItem({
      id: action.id,
      estimatedHour: add(feature.engineHours, selectedPurchase.expirationEngineHours),
      estimatedDate: calculateExpirationDate(selectedPurchase.expirationDate, feature),
      vendor: feature.vendor,
    }, state.get('cart').toJS());

    const nextState = state
      .set('cart', fromJS(cart));

    return nextState
      .set('canSubmit', canSubmit(nextState.toJS()));
  },
  [CLEAR_ACM_SUBSCRIPTION]: (state) => state
    .setIn(['acm', 'partNumber'], fromJS(''))
    .setIn(['acm', 'engineHour'], fromJS(''))
    .setIn(['acm', 'numberOfYear'], fromJS('')),
};
