import React, { PureComponent } from 'react';
import { object, func } from 'prop-types';
import { length, any, propEq, or, equals } from 'ramda';
import { push } from 'react-router-redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { filterUpgrades, filterSubscriptions } from 'transformers/orders';
import Container from 'components/Container';
import Button from 'components/Button';
import Block from 'components/Block';
import translate from 'utils/translate';
import styled from 'styled-components';
import { convertInputToField } from 'utils/form';
import theme from 'utils/styles';
import DeviceInsight from './DeviceInsight';
import Enroll from './Enroll';
import Trimble from './Trimble';
import { edit, checkout, changeField } from './actions';
import Info from './Info';
import Product from './Product';
import Features from './Features';
import { makeSelectCheckout } from './selectors';
import { makeSelectProduct } from '../ProductPage/selectors';
import { makeSelectApp } from '../App/selectors';

const StyledBlock = styled(Block)`
  margin: 35px 0;
  width: 33.33%;
  float: right;
`;

const StyledButton = styled(Button)`
  margin-right: 10px;
  width: calc(50% - 5px);
  font-size: ${theme.font.small};
  padding: 10px 30px;

  &:last-child {
    margin-right: 0;
  }
`;

export class CheckoutPage extends PureComponent {
  static propTypes = {
    product: object,
    app: object,
    data: object,
    onEdit: func,
    redirectHome: func,
    onCheckout: func,
    onChange: func,
  };

  componentDidMount() {
    const {
      product: {
        cart,
      },
      redirectHome,
    } = this.props;

    if (!length(cart)) {
      redirectHome();
    }
  }
  render() {
    const {
      product: {
        customerReference,
        serialNumber,
        vendor,
        cart,
        product: {
          vin,
          NT03AGCOserialNumber,
        },
      },
      data: {
        isPlaced,
        fields,
      },
      app: {
        emailAddress,
      },
      onEdit,
      onCheckout,
      onChange,
    } = this.props;

    const isDeviceInsight = vendor.value === 'DeviceInsight';
    const isTrimbleVRS = vendor.value === 'Trimble' && any(propEq('metaTags', 'VRS'), cart);
    const canEnroll = vendor.value === 'AGCO' && isPlaced && length(filterSubscriptions(cart));
    const isAGCOorNT03 = or(equals(vendor.value, 'AGCO'), equals(vendor.value, 'NT03'));
    return (
      <Container>
        <Info isPlaced={isPlaced} customerReference={customerReference} emailAddress={emailAddress} />
        <Product {...vendor} serialNumber={isAGCOorNT03 ? NT03AGCOserialNumber : serialNumber} vin={vin} />
        <If condition={canEnroll}>
          <Enroll vin={vin} />
        </If>
        <Features vendor={vendor.value} upgrades={filterUpgrades(cart)} subscriptions={filterSubscriptions(cart)} />
        <If condition={!isPlaced}>
          <Choose>
            <When condition={isDeviceInsight} >
              <DeviceInsight onChange={onChange} fields={fields} />
            </When>
            <When condition={isTrimbleVRS}>
              <Trimble onChange={onChange} fields={fields} />
            </When>
          </Choose>
          <StyledBlock right>
            <StyledButton color="sanJuan" onClick={onEdit}>{translate('ORDERS.EDIT_ORDER')}</StyledButton>
            <StyledButton color="sanJuan" onClick={onCheckout} filled>{translate('ORDERS.PLACE_ORDER')}</StyledButton>
          </StyledBlock>
        </If>
      </Container>
    );
  }
}


const mapStateToProps = createStructuredSelector({
  product: makeSelectProduct(),
  app: makeSelectApp(),
  data: makeSelectCheckout(),
});

function mapDispatchToProps(dispatch) {
  return {
    onCheckout: () => dispatch(checkout()),
    redirectHome: () => dispatch(push({
      pathname: '/',
    })),
    onEdit: () => dispatch(edit()),
    onChange: (evt) => dispatch(changeField(convertInputToField(evt))),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPage);
