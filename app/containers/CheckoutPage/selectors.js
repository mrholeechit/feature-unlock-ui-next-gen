import { createSelector } from 'reselect';

export const makeSelectCheckout = () => createSelector(
  (state) => state.get('checkout'),
  (state) => state.toJS()
);
