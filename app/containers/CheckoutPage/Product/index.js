import React from 'react';
import Title from 'components/Title';
import styled from 'styled-components';
import Block from 'components/Block';
import { string } from 'prop-types';
import { isNil } from 'ramda';
import translate from 'utils/translate';
import Image from './Image';
import SerialNumber from './SerialNumber';
import Vin from './Vin';

const StyledBlock = styled(Block)`
  position: relative;
  padding-left: 125px;
  margin-bottom: 35px;
`;

const Product = ({
  image, // eslint-disable-line
  label,
  serialNumber,
  vin,
}) => (
  <StyledBlock>
    <Image src={image} />
    <Title big>{label}</Title>
    <If condition={!isNil(vin)}>
      <Vin>{'VIN #'}{vin}</Vin>
    </If>
    <SerialNumber>{translate('SERIAL')} #{serialNumber}</SerialNumber>
  </StyledBlock>
);

Product.propTypes = {
  image: string,
  label: string,
  serialNumber: string,
  vin: string,
};

export default Product;
