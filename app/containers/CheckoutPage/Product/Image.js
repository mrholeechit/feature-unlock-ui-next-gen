import styled from 'styled-components';
import { mixins } from 'utils/styles';

export default styled.img`
  position: absolute;
  ${mixins.center('vertical')}
  left: 0;
  width: 100px;
`;
