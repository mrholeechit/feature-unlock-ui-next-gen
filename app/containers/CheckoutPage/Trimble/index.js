import React from 'react';
import { func, object } from 'prop-types';
import Form from 'components/Form';
import Title from 'components/Title';
import Input from 'components/Input';
import Label from 'components/Label';
import Block from 'components/Block';
import styled from 'styled-components';
import translate from 'utils/translate';
import theme from 'utils/styles';

const StyledForm = styled(Form)`
  float: left;
  width: 100%;
  padding: 35px 0;
  border-top: 1px ${theme.color.alto} solid;
  border-bottom: 1px ${theme.color.alto} solid;
`;

const StyledBlock = styled(Block)`
  width: 33.33%;
  padding-right: 20px;
  margin-top: 15px;

  &:nth-child(3n) {
    padding-right: 0;
  }

  .input {
    width: 100%;
  }
`;

const Trimble = ({
  onChange,
  fields,
}) => (
  <StyledForm>
    <Title big>{translate('CREDENTIALS.SET_CREDENTIALS')}</Title>
    <p>{translate('CREDENTIALS.ABOUT_ID')}</p>
    <Block>
      <StyledBlock>
        <Label htmlFor="POS_ID">{translate('CREDENTIALS.POS_ID')} / {translate('CREDENTIALS.USER_ID')}</Label>
        <Input
          id="POS_ID"
          name="POS_ID"
          error={fields.POS_ID.error}
          value={fields.POS_ID.value}
          placeholder={translate('CREDENTIALS.ENTER_USER_ID')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="POS_PASSWORD">{translate('CREDENTIALS.PASSWORD')}</Label>
        <Input
          id="POS_PASSWORD"
          name="POS_PASSWORD"
          type="password"
          error={fields.POS_PASSWORD.error}
          value={fields.POS_PASSWORD.value}
          placeholder={translate('CREDENTIALS.ENTER_PASSWORD')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="POS_PASSWORD_CONFIRM">{translate('CREDENTIALS.CONFIRM_PASSWORD')}</Label>
        <Input
          id="POS_PASSWORD_CONFIRM"
          name="POS_PASSWORD_CONFIRM"
          type="password"
          error={fields.POS_PASSWORD_CONFIRM.error}
          value={fields.POS_PASSWORD_CONFIRM.value}
          placeholder={translate('CREDENTIALS.CONFIRM_PASSWORD')}
          onChange={onChange}
        />
      </StyledBlock>
    </Block>
  </StyledForm>
);

Trimble.propTypes = {
  onChange: func,
  fields: object,
};

export default Trimble;
