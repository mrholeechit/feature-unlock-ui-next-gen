import Yup from 'yup';

export default Yup.object({
  POS_ID: Yup.string().max(20).required(),
  POS_PASSWORD: Yup.string().min(6).required(),
  POS_PASSWORD_CONFIRM: Yup.string().min(6).required().equalTo(Yup.ref('POS_PASSWORD')),
});
