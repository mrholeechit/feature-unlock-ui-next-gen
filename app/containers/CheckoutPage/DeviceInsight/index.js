import React from 'react';
import { func, object } from 'prop-types';
import Form from 'components/Form';
import Title from 'components/Title';
import Input from 'components/Input';
import Label from 'components/Label';
import Block from 'components/Block';
import Select from 'components/Select';
import Link from 'components/Link';
import Checkbox from 'components/Checkbox';
import styled from 'styled-components';
import translate from 'utils/translate';
import { LANGUAGES } from 'utils/constants';
import theme from 'utils/styles';

const StyledForm = styled(Form)`
  float: left;
  width: 100%;
  padding: 35px 0;
  border-top: 1px ${theme.color.alto} solid;
  border-bottom: 1px ${theme.color.alto} solid;
`;

const StyledBlock = styled(Block)`
  width: 33.33%;
  padding-right: 20px;
  margin-top: 15px;

  &:nth-child(3n) {
    padding-right: 0;
  }

  .input {
    width: 100%;
  }
`;

const TermsBlock = styled(StyledBlock)`
  float: right;
`;

const DeviceInsight = ({
  onChange,
  fields,
}) => (
  <StyledForm>
    <Title big>{translate('CREDENTIALS.CUSTOMER_FMIS_INFO')}</Title>
    <Block>
      <StyledBlock>
        <Label htmlFor="CUSTOMER_NAME">{translate('CREDENTIALS.COMPANY_NAME')}</Label>
        <Input
          id="CUSTOMER_NAME"
          name="CUSTOMER_NAME"
          error={fields.CUSTOMER_NAME.error}
          value={fields.CUSTOMER_NAME.value}
          placeholder={translate('CREDENTIALS.ENTER_COMPANY_NAME')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="CUSTOMER_FIRSTNAME">{translate('CREDENTIALS.FIRST_NAME')}</Label>
        <Input
          id="CUSTOMER_FIRSTNAME"
          name="CUSTOMER_FIRSTNAME"
          error={fields.CUSTOMER_FIRSTNAME.error}
          value={fields.CUSTOMER_FIRSTNAME.value}
          placeholder={translate('CREDENTIALS.ENTER_FIRST_NAME')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="CUSTOMER_LASTNAME">{translate('CREDENTIALS.LAST_NAME')}</Label>
        <Input
          id="CUSTOMER_LASTNAME"
          name="CUSTOMER_LASTNAME"
          error={fields.CUSTOMER_LASTNAME.error}
          value={fields.CUSTOMER_LASTNAME.value}
          placeholder={translate('CREDENTIALS.ENTER_LAST_NAME')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="FIS_USERNAME">{translate('CREDENTIALS.FMIS_USER_ID')}</Label>
        <Input
          id="FIS_USERNAME"
          name="FIS_USERNAME"
          error={fields.FIS_USERNAME.error}
          value={fields.FIS_USERNAME.value}
          placeholder={translate('CREDENTIALS.ENTER_USER_ID')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="FIS_PASSWORD">{translate('CREDENTIALS.FMIS_PASS')}</Label>
        <Input
          id="FIS_PASSWORD"
          name="FIS_PASSWORD"
          type="password"
          error={fields.FIS_PASSWORD.error}
          value={fields.FIS_PASSWORD.value}
          placeholder={translate('CREDENTIALS.ENTER_PASSWORD')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="FIS_PASSWORD_CONFIRM">{translate('CREDENTIALS.CONFIRM_FMIS_PASS')}</Label>
        <Input
          id="FIS_PASSWORD_CONFIRM"
          name="FIS_PASSWORD_CONFIRM"
          type="password"
          error={fields.FIS_PASSWORD_CONFIRM.error}
          value={fields.FIS_PASSWORD_CONFIRM.value}
          placeholder={translate('CREDENTIALS.CONFIRM_FMIS_PASS')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="CUSTOMER_EMAIL">{translate('CREDENTIALS.EMAIL_ADDRESS')}</Label>
        <Input
          id="CUSTOMER_EMAIL"
          name="CUSTOMER_EMAIL"
          error={fields.CUSTOMER_EMAIL.error}
          value={fields.CUSTOMER_EMAIL.value}
          placeholder={translate('CREDENTIALS.ENTER_EMAIL')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="CUSTOMER_EMAIL_CONFIRM">{translate('CREDENTIALS.CONFIRM_EMAIL')}</Label>
        <Input
          id="CUSTOMER_EMAIL_CONFIRM"
          name="CUSTOMER_EMAIL_CONFIRM"
          error={fields.CUSTOMER_EMAIL_CONFIRM.error}
          value={fields.CUSTOMER_EMAIL_CONFIRM.value}
          placeholder={translate('CREDENTIALS.CONFIRM_EMAIL')}
          onChange={onChange}
        />
      </StyledBlock>
      <StyledBlock>
        <Label htmlFor="CUSTOMER_PREFERRED_LANGUAGE">{translate('CREDENTIALS.CUSTOMER_LANGUAGE')}</Label>
        <Select
          id="CUSTOMER_PREFERRED_LANGUAGE"
          name="CUSTOMER_PREFERRED_LANGUAGE"
          error={fields.CUSTOMER_PREFERRED_LANGUAGE.error}
          value={fields.CUSTOMER_PREFERRED_LANGUAGE.value}
          options={LANGUAGES}
          placeholder={translate('CREDENTIALS.SELECT_LANG_PREF')}
          onChange={onChange}
        />
      </StyledBlock>
    </Block>
    <TermsBlock>
      <Link small to={`${window.location.origin}/terms/${fields.CUSTOMER_PREFERRED_LANGUAGE.value}`} underlined target="_blank">{translate('TERMS_AND_CONDITIONS')}</Link>
      <Block>
        <Checkbox
          name="terms"
          checked={fields.terms.value}
          onChange={onChange}
          error={fields.terms.error}
        >{translate('I_AGREE')}
        </Checkbox>
      </Block>
    </TermsBlock>
  </StyledForm>
);

DeviceInsight.propTypes = {
  onChange: func,
  fields: object,
};

export default DeviceInsight;
