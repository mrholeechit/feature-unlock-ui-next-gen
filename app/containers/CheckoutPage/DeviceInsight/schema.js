import Yup from 'yup';

export default Yup.object({
  CUSTOMER_NAME: Yup.string().ensure().trim().required(),
  CUSTOMER_FIRSTNAME: Yup.string().required(),
  CUSTOMER_LASTNAME: Yup.string().required(),
  FIS_USERNAME: Yup.string().required(),
  FIS_PASSWORD: Yup.string().min(8).required(),
  FIS_PASSWORD_CONFIRM: Yup.string().min(8).required().equalTo(Yup.ref('FIS_PASSWORD')),
  CUSTOMER_EMAIL: Yup.string().email().required(),
  CUSTOMER_EMAIL_CONFIRM: Yup.string().email().required().equalTo(Yup.ref('CUSTOMER_EMAIL')),
  CUSTOMER_PREFERRED_LANGUAGE: Yup.string().required(),
  terms: Yup.bool().oneOf([true]).required(),
});
