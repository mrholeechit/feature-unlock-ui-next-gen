import React from 'react';
import Link from 'components/Link';
import { string } from 'prop-types';
import translate from 'utils/translate';
import { AC2_URL, AUTH_REDIRECT_URL } from 'config';
import styled from 'styled-components';

const Wrapper = styled.p`
  float: left;
  width: 100%;
  margin-top: -15px;
`;

const Enroll = ({ vin }) => (
  <Wrapper>
    <Link underlined to={`${AC2_URL}admin/equipmentEdit?identificationNumber=${vin}&referrer=${AUTH_REDIRECT_URL}`}>{translate('PRODUCT_PAGE.ENROLL_MACHINE')}</Link> - {translate('PRODUCT_PAGE.ENROLLMENT_MESSAGE')}
  </Wrapper>
);

Enroll.propTypes = {
  vin: string,
};

export default Enroll;
