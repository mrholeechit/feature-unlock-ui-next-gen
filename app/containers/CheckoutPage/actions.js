import { EDIT, CHECKOUT, CHECKOUT_SUCCEEDED, CHECKOUT_FAILED, CHANGE_FIELD, DEFAULT_VALUES } from './constants';

export function edit() {
  return ({
    type: EDIT,
  });
}

export function checkout() {
  return ({
    type: CHECKOUT,
  });
}

export function checkoutSucceeded() {
  return ({
    type: CHECKOUT_SUCCEEDED,
  });
}

export function checkoutFailed(errors) {
  return ({
    type: CHECKOUT_FAILED,
    errors,
  });
}

export function changeField({ name, value }) {
  return ({
    type: CHANGE_FIELD,
    name,
    value,
  });
}

export function resetToDefaultValues() {
  return ({
    type: DEFAULT_VALUES,
  });
}
