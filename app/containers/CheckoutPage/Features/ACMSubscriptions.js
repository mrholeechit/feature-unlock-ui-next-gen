import React from 'react';
import FeatureInfo from 'containers/ProductPage/components/FeatureInfo'; // TODO: remove from product page
import { length } from 'ramda';
import { array } from 'prop-types';
import translate from 'utils/translate';
import Table from 'components/Table';

const ACMSubscriptions = ({
  items,
}) => {
  const fields = [
    {
      field: 'description',
      name: translate('TABLES.FEATURES'),
      format: (props) => (
        <FeatureInfo {...props} withoutCheckbox withoutPadding />
      ),
    },
    {
      field: 'engineHours',
      name: translate('TABLES.ENGINE_HOURS'),
      width: 200,
    },
    {
      field: 'numberOfYears',
      name: translate('TABLES.NUMBER_OF_YEARS'),
      width: 200,
      format: ({ calendarExpiration: { value } }) => value, // eslint-disable-line
    },
  ];

  return (
    <Table noDataText="" minRows={length(items)} items={items} fields={fields} />
  );
};

ACMSubscriptions.propTypes = {
  items: array,
};

export default ACMSubscriptions;
