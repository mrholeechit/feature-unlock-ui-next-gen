import React from 'react';
import { length } from 'ramda';
import translate from 'utils/translate';
import { array, string } from 'prop-types';
import styled from 'styled-components';
import theme from 'utils/styles';
import Accordion from 'components/Accordion';
import Block from 'components/Block';
import Upgrades from './Upgrades';
import Subscriptions from './Subscriptions';
import ACMSubscriptions from './ACMSubscriptions';

const StyledAccordion = styled(Accordion)`
  .accordion__body[aria-hidden="false"] {
    border-bottom-left-radius: ${theme.border.radius};
    border-bottom-right-radius: ${theme.border.radius};
  }

  .accordion__title {
    background: #fff;
  }

  .title {
    color: ${theme.color.monza};
  }

  &:last-child {
    padding-bottom: 35px;
  }
`;

const Features = ({
  subscriptions,
  upgrades,
  vendor,
}) => (
  <Block>
    <If condition={length(upgrades)}>
      <StyledAccordion expanded title={`${translate('PRODUCT_TABLE.UPGRADES')} (${translate('PRODUCT_TABLE.AUTH_CODE')})`}>
        <Upgrades items={upgrades} />
      </StyledAccordion>
    </If>
    <If condition={length(subscriptions)}>
      <StyledAccordion expanded title={translate('PRODUCT_TABLE.SUBSCRIPTIONS')}>
        <Choose>
          <When condition={vendor === 'AGCO'}>
            <ACMSubscriptions items={subscriptions} />
          </When>
          <Otherwise>
            <Subscriptions items={subscriptions} />
          </Otherwise>
        </Choose>
      </StyledAccordion>
    </If>
  </Block>
);

Features.propTypes = {
  subscriptions: array,
  upgrades: array,
  vendor: string,
};

export default Features;
