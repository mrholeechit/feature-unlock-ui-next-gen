import React from 'react';
import FeatureInfo from 'containers/ProductPage/components/FeatureInfo'; // TODO: remove from product page
import { length } from 'ramda';
import { array } from 'prop-types';
import { format } from 'date-fns';
import translate from 'utils/translate';
import { DATE_FORMAT } from 'utils/constants';
import Table from 'components/Table';

const Subscriptions = ({
  items,
}) => {
  const fields = [
    {
      field: 'description',
      name: translate('TABLES.FEATURES'),
      format: (props) => (
        <FeatureInfo {...props} withoutCheckbox withoutPadding />
      ),
    },
    {
      field: 'activationDate',
      name: translate('TABLES.ACTIVATION'),
      format: ({ activationDate }) => ( // eslint-disable-line
        activationDate || format(new Date(), DATE_FORMAT)
      ),
      width: 200,
    },
    {
      field: 'expirationDate',
      name: translate('TABLES.EXPIRATION'),
      width: 200,
    },
  ];

  return (
    <Table noDataText="" minRows={length(items)} items={items} fields={fields} />
  );
};

Subscriptions.propTypes = {
  items: array,
};

export default Subscriptions;
