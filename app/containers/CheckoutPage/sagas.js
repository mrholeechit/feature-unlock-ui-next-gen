import { takeLatest, put, select, call } from 'redux-saga/effects';
import { goBack } from 'react-router-redux';
import { map, any, propEq, prop } from 'ramda';
import API from 'api';
import Yup from 'yup';
import { toggleLoading } from 'containers/App/actions';
import { validate } from 'utils/validations';
import { transformOrdersToCheckout } from 'transformers/orders';
import { EDIT, CHECKOUT } from './constants';
import { checkoutSucceeded, checkoutFailed } from './actions';
import { toggleFetch } from '../ProductPage/Generic/actions';
import { makeSelectProduct } from '../ProductPage/selectors';
import { makeSelectCheckout } from './selectors';

const schemas = {
  Trimble: require('./Trimble/schema').default, // eslint-disable-line
  DeviceInsight: require('./DeviceInsight/schema').default, // eslint-disable-line
};

export function* editPage() {
  yield put(toggleFetch());
  yield put(goBack());
}

function* validateFields({ cart, vendor, extraAttributes }) {
  const isTrimbleVRS = vendor === 'Trimble' && any(propEq('metaTags', 'VRS'), cart);
  const isDeviceInsight = vendor === 'DeviceInsight';

  let schema = Yup.object({});

  if (isTrimbleVRS) {
    schema = prop('Trimble', schemas);
  } else if (isDeviceInsight) {
    schema = prop('DeviceInsight', schemas);
  }

  yield validate(schema, extraAttributes);
}

export function* checkout() {
  yield put(toggleLoading(true));

  const { cart, vendor: { value: vendor } } = yield select(makeSelectProduct());
  const { fields } = yield select(makeSelectCheckout());

  const extraAttributes = map(prop('value'), fields);

  try {
    yield validateFields({ cart, extraAttributes, vendor });

    const orders = transformOrdersToCheckout({ vendor, cart, extraAttributes });

    yield call(API.orders.post, { orders });
    yield put(checkoutSucceeded());
  } catch (e) {
    yield put(checkoutFailed(e));
  }

  yield put(toggleLoading(false));
}

export function* listeners() {
  yield takeLatest(EDIT, editPage);
  yield takeLatest(CHECKOUT, checkout);
}

export default [
  listeners,
];
