import React from 'react';
import styled from 'styled-components';
import { bool, string } from 'prop-types';
import Title from 'components/Title';
import Block from 'components/Block';
import theme from 'utils/styles';
import { format } from 'date-fns';
import { DATE_FORMAT } from 'utils/constants';
import translate from 'utils/translate';

const StyledTitle = Title.withComponent('h1').extend`
  margin-bottom: 10px;
`;

const StyledBlock = styled(Block)`
  padding-bottom: 35px;
  margin-bottom: 35px;
  border-bottom: 1px ${theme.color.alto} solid;
`;

const Info = ({ isPlaced, customerReference, emailAddress }) => (
  <StyledBlock center>
    <StyledTitle big>{isPlaced ? translate('ORDERS.THANK_YOU') : translate('ORDERS.YOUR_ORDER')}</StyledTitle>
    <If condition={isPlaced}>
      <Block>
        <strong>{translate('ORDERS.PURCHASE_DATE')}:</strong> <span>{format(new Date(), DATE_FORMAT)}</span>
      </Block>
    </If>
    <Block>
      <strong>{translate('PRODUCT_PAGE.CUSTOMER_REFERENCE')}:</strong> <span>{customerReference}</span>
    </Block>
    <Block>
      <strong>{translate('CREDENTIALS.EMAIL_ADDRESS')}:</strong> <span>{emailAddress}</span>
    </Block>
  </StyledBlock>
);

Info.propTypes = {
  isPlaced: bool,
  customerReference: string,
  emailAddress: string,
};

export default Info;
