import { fromJS } from 'immutable';
import { forEachObjIndexed, propOr } from 'ramda';
import { CHECKOUT_SUCCEEDED, CHECKOUT_FAILED, CHANGE_FIELD, DEFAULT_VALUES } from './constants';

const initialState = fromJS({
  isPlaced: false,
  fields: {
    CUSTOMER_NAME: {
      value: '',
      error: '',
    },
    CUSTOMER_FIRSTNAME: {
      value: '',
      error: '',
    },
    CUSTOMER_LASTNAME: {
      value: '',
      error: '',
    },
    FIS_USERNAME: {
      value: '',
      error: '',
    },
    FIS_PASSWORD: {
      value: '',
      error: '',
    },
    FIS_PASSWORD_CONFIRM: {
      value: '',
      error: '',
    },
    CUSTOMER_EMAIL: {
      value: '',
      error: '',
    },
    CUSTOMER_EMAIL_CONFIRM: {
      value: '',
      error: '',
    },
    CUSTOMER_PREFERRED_LANGUAGE: {
      value: 'en',
      error: '',
    },
    POS_ID: {
      value: '',
      error: '',
    },
    POS_PASSWORD: {
      value: '',
      error: '',
    },
    POS_PASSWORD_CONFIRM: {
      value: '',
      error: '',
    },
    terms: {
      value: false,
      error: '',
    },
  },
});

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_FIELD:
      return state
        .setIn(['fields', action.name, 'value'], action.value)
        .setIn(['fields', action.name, 'error'], '');
    case CHECKOUT_SUCCEEDED:
      return state
        .set('isPlaced', true);
    case CHECKOUT_FAILED: {
      let nextState = state;

      forEachObjIndexed((_, name) => {
        nextState = nextState.setIn(['fields', name, 'error'], propOr('', name, action.errors));
      }, nextState.get('fields').toJS());

      return nextState;
    }
    case DEFAULT_VALUES:
      return initialState;
    default:
      return state;
  }
};
