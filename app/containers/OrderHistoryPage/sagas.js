import API from 'api';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { toggleLoading } from 'containers/App/actions';
import { transformOrders, transformOrdersFilters, transformOrdersCount, transformOrdersCountFilters } from 'transformers/orders';
import { ORDERS_FETCH_REQUESTED } from './constants';
import { ordersFetchSucceeded } from './actions';
import { makeSelectOrderHistory } from './selectors';

export function* fetchOrders() {
  yield put(toggleLoading(true));

  const { searchCriteria } = yield select(makeSelectOrderHistory());
  const orders = transformOrders(yield call(API.orders.get, transformOrdersFilters(searchCriteria)));
  const length = transformOrdersCount(yield call(API.orders.head, transformOrdersCountFilters(searchCriteria)));

  yield put(ordersFetchSucceeded({ orders, length }));
  yield put(toggleLoading(false));
}

export function* listeners() {
  yield takeLatest(ORDERS_FETCH_REQUESTED, fetchOrders);
}

export default [
  listeners,
];
