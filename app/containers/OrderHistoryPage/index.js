import React, { PureComponent } from 'react';
import { func, object } from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import Pager from 'components/Pager';
import { format } from 'date-fns';
import { path } from 'ramda';
import Title from 'components/Title';
import Container from 'components/Container';
import translate from 'utils/translate';
import { convertInputToField } from 'utils/form';
import { makeSelectOrderHistory } from './selectors';
import List from './List';
import { fetchOrders, ordersNextPage, ordersPreviousPage, changeField } from './actions';
import SearchBox from './SearchBox';

class OrderHistoryPage extends PureComponent {
  static propTypes = {
    data: object,
    onFetch: func,
    onClickNextPage: func,
    onClickPreviousPage: func,
    onChange: func,
    onSubmit: func,
    location: object,
  }

  componentDidMount() {
    const {
      onFetch,
      location: {
        query: {
          orderReference,
        },
      },
    } = this.props;

    onFetch(orderReference);
  }

  componentDidUpdate(prevProps) {
    const getOrderReference = path(['location', 'query', 'orderReference']);

    if (getOrderReference(this.props) !== getOrderReference(prevProps)) {
      this.props.onFetch();
    }
  }

  render() {
    const {
      data: {
        orders,
        searchCriteria: {
          customerReference,
          offset,
          limit,
        },
      },
      onClickNextPage,
      onClickPreviousPage,
      onChange,
      onSubmit,
    } = this.props;

    const today = format(new Date(), 'MM/DD/YYYY');

    return (
      <Container>
        <Title big>{translate('ORDER_HISTORY')}</Title>
        <br />
        <span>{translate('AS_OF')} {today}</span>
        <SearchBox
          onChange={onChange}
          onSubmit={onSubmit}
          value={customerReference}
        />
        <List items={orders.items} />
        <Pager
          offset={offset}
          onPreviousPage={onClickPreviousPage}
          onNextPage={onClickNextPage}
          length={orders.length}
          limit={limit}
        />
      </Container>
    );
  }
}

export const mapDispatchToProps = (dispatch) => ({
  onFetch: (orderReference) => dispatch(fetchOrders(orderReference)),
  onClickNextPage: () => {
    dispatch(ordersNextPage());
    dispatch(fetchOrders());
  },
  onClickPreviousPage: () => {
    dispatch(ordersPreviousPage());
    dispatch(fetchOrders());
  },
  onChange: (evt) => dispatch(changeField(convertInputToField(evt))),
  onSubmit: () => dispatch(fetchOrders()),
});

const mapStateToProps = createStructuredSelector({
  data: makeSelectOrderHistory(),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderHistoryPage);
