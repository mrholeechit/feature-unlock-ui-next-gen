import { CHANGE_FIELD, ORDERS_FETCH_REQUESTED, ORDERS_FETCH_SUCCEEDED, ORDERS_PREVIOUS_PAGE, ORDERS_NEXT_PAGE } from './constants';

export function changeField({ name, value }) {
  return {
    type: CHANGE_FIELD,
    name,
    value,
  };
}

export function fetchOrders() {
  return {
    type: ORDERS_FETCH_REQUESTED,
  };
}

export function ordersFetchSucceeded({
  orders,
  length,
}) {
  return {
    type: ORDERS_FETCH_SUCCEEDED,
    orders,
    length,
  };
}

export function ordersNextPage() {
  return {
    type: ORDERS_NEXT_PAGE,
  };
}

export function ordersPreviousPage() {
  return {
    type: ORDERS_PREVIOUS_PAGE,
  };
}
