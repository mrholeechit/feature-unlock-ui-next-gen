import { createSelector } from 'reselect';

export const makeSelectOrderHistory = () => createSelector(
  (state) => state.get('orderHistory'),
  (state) => state.toJS()
);
