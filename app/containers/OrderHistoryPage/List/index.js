import React from 'react';
import Link from 'components/Link';
import { arrayOf, object } from 'prop-types';
import Table from 'components/Table';
import Badge from 'components/Badge';
import camelCase from 'lodash.camelcase';
import styled from 'styled-components';
import translate from 'utils/translate';

const StyledTable = styled(Table)`
  margin: 20px 0;
`;

const List = ({
  items,
}) => {
  const fields = [
    {
      field: 'orderDate',
      name: translate('ORDERS.ORDER_DATE'),
      width: 120,
    },
    {
      field: 'orderReference',
      name: translate('ORDERS.ORDER_REFERENCE'),
      width: 165,
    },
    {
      field: 'customerReference',
      name: translate('PRODUCT_PAGE.CUSTOMER_REFERENCE'),
      width: 195,
    },
    {
      field: 'serialNumber',
      name: translate('PRODUCT_PAGE.SERIAL_NUMBER'),
      format: ({ serialNumber, realVendor }) => ( // eslint-disable-line
        <Link underlined to={{ pathname: '/product', query: { serialNumber, vendor: realVendor } }}>{serialNumber}</Link>
      ),
      width: 185,
    },
    {
      field: 'product',
      name: translate('PRODUCT'),
      width: 540,
    },
    {
      field: 'status',
      name: translate('API.STATUS.STATUS'),
      format: ({ status }) => ( // eslint-disable-line
        <Badge state={camelCase(status)}>{status}</Badge>
      ),
      width: 115,
    },
  ];

  return (
    <StyledTable noDataText="Hmmm... Looks like no orders have been placed yet!!" height="calc(100vh - 320px)" items={items} fields={fields} />
  );
};

List.propTypes = {
  items: arrayOf(object),
};

export default List;
