import React from 'react';
import styled from 'styled-components';
import { string, func } from 'prop-types';
import translate from 'utils/translate';
import Input from 'components/Input';
import Button from 'components/Button';
import Form from 'components/Form';

const StyledInput = styled(Input)`
  padding: 12px;
  width: calc(100% - 150px);
  margin-right: 15px;
`;

const StyledForm = styled(Form)`
  margin-top: 10px;
`;

const SearchBox = ({
  onSubmit,
  onChange,
  value,
}) => (
  <StyledForm onSubmit={onSubmit}>
    <StyledInput name="customerReference" placeholder={translate('ORDERS.SEARCH_BY_CUSTOMER_REFERENCE')} autoHeight onChange={onChange} value={value} />
    <Button type="submit" color="sanJuan" filled>{translate('SEARCH')}</Button>
  </StyledForm>
);

SearchBox.propTypes = {
  onSubmit: func,
  onChange: func,
  value: string,
};

export default SearchBox;
