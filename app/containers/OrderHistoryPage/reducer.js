import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { ORDERS_FETCH_REQUESTED, CHANGE_FIELD, ORDERS_FETCH_SUCCEEDED, ORDERS_PREVIOUS_PAGE, ORDERS_NEXT_PAGE } from './constants';

const initialState = fromJS({
  orders: {
    items: [],
    length: 0,
  },
  searchCriteria: {
    offset: 0,
    limit: 20,
    customerReference: '',
    shouldResetOffset: false,
  },
});

export default (state = initialState, action) => {
  switch (action.type) {
    case ORDERS_FETCH_REQUESTED: {
      const offset = state.getIn(['searchCriteria', 'shouldResetOffset']) ? 0 : state.getIn(['searchCriteria', 'offset']);
      return state.setIn(['searchCriteria', 'offset'], offset);
    }
    case CHANGE_FIELD:
      return state
        .setIn(['searchCriteria', action.name], action.value)
        .setIn(['searchCriteria', 'shouldResetOffset'], true);
    case ORDERS_FETCH_SUCCEEDED:
      return state
        .setIn(['orders', 'items'], action.orders)
        .setIn(['orders', 'length'], action.length)
        .setIn(['searchCriteria', 'shouldResetOffset'], false);
    case LOCATION_CHANGE:
      return initialState;
    case ORDERS_NEXT_PAGE:
      return state.setIn(['searchCriteria', 'offset'], state.getIn(['searchCriteria', 'offset']) + 1);
    case ORDERS_PREVIOUS_PAGE:
      return state.setIn(['searchCriteria', 'offset'], state.getIn(['searchCriteria', 'offset']) - 1);
    default:
      return state;
  }
};
