import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { ADD_TOKEN, CLEAR_TOKEN, LOADED, ADD_USER, TOGGLE_DROPDOWN, TOGGLE_LOADING } from './constants';

const initialState = fromJS({
  token: {},
  roles: [],
  user: '',
  emailAddress: '',
  dealer: {},
  isSupport: false,
  showDropdown: false,
  loadingStack: [],
  loading: false,
  loaded: false,
});

const toggleLoading = (state, show) => {
  const loadingStack = show ? state.get('loadingStack').push(true) : state.get('loadingStack').pop();

  return state
    .set('loading', !!loadingStack.count())
    .set('loadingStack', loadingStack);
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TOKEN:
      window.localStorage.setItem('token', JSON.stringify(action.token));

      return state
        .set('token', action.token);
    case CLEAR_TOKEN:
      window.localStorage.removeItem('token');
      return state
        .set('token', {})
        .set('loading', true);
    case TOGGLE_LOADING:
      return toggleLoading(state, action.show);
    case TOGGLE_DROPDOWN:
      return state
        .set('showDropdown', action.show);
    case LOCATION_CHANGE:
      return state
        .set('showDropdown', false);
    case ADD_USER:
      return state
        .set('isSupport', action.isSupport)
        .set('roles', action.roles)
        .set('dealer', action.dealer)
        .set('user', action.user)
        .set('emailAddress', action.emailAddress);
    case LOADED:
      return state
        .set('loaded', true);
    default:
      return state;
  }
};
