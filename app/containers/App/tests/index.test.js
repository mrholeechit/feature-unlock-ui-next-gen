import React from 'react';
import { shallow } from 'enzyme';
import Loader from 'components/Loader';
import { merge } from 'ramda';
import { expect } from 'chai';
import { App } from '../index';

const defaultProps = {
  onAddToken: () => {},
  location: {
    pathname: '/',
  },
  app: {
    user: {},
  },
  search: {
    query: '',
  },
};

describe('<App />', () => {
  it('should show loader', () => {
    const props = merge(defaultProps, { app: { loading: true } });
    const component = (
      <App {...props} />
    );

    const wrapper = shallow(component, { disableLifecycleMethods: true });

    expect(wrapper.find(Loader).first().prop('show')).to.equal(true);
  });

  it('shouldn\'t show loader', () => {
    const props = merge(defaultProps, { app: { loading: false } });
    const component = (
      <App {...props} />
    );

    const wrapper = shallow(component, { disableLifecycleMethods: true });

    expect(wrapper.find(Loader).first().prop('show')).to.equal(false);
  });
});
