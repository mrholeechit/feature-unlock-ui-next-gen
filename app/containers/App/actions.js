import { ADD_TOKEN, CLEAR_TOKEN, LOADED, ADD_USER, TOGGLE_DROPDOWN, TOGGLE_LOADING } from './constants';

export function addToken(token) {
  return {
    type: ADD_TOKEN,
    token,
  };
}

export function toggleLoading(show) {
  return {
    type: TOGGLE_LOADING,
    show,
  };
}

export function addUser({
  roles,
  user,
  isSupport,
  dealer,
  emailAddress,
}) {
  return {
    type: ADD_USER,
    roles,
    user,
    dealer,
    isSupport,
    emailAddress,
  };
}

export function loaded() {
  return {
    type: LOADED,
  };
}

export function toggleDropdown(show) {
  return {
    type: TOGGLE_DROPDOWN,
    show,
  };
}

export function clearToken() {
  return {
    type: CLEAR_TOKEN,
  };
}
