import styled from 'styled-components';
import Link from 'components/Link';

export default styled(Link).attrs({
  'aria-label': 'logo',
})`
  float: left;
`;
