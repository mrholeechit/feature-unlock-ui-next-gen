import React from 'react';
import ClickOutside from 'react-click-outside';
import { func, string, bool, object, arrayOf } from 'prop-types';
import { VelocityTransitionGroup } from 'velocity-react';
import styled from 'styled-components';
import Autocomplete from 'components/Autocomplete';
import theme from 'utils/styles';
import SearchIcon from 'images/icons/search.svg?fill=#FFF'; // eslint-disable-line
import Button from 'components/Button';
import Hamburger from 'components/Hamburger';
import translate from 'utils/translate';
import Brand from './Brand';
import Logo from './Logo';
import Dropdown from './Dropdown';
import Separator from './Separator';
import Description from './Description';
import Navbar from './Navbar';
import Link from './Link';
import Username from './Username';

const StyledClickOutside = styled(ClickOutside)`
  display: inline-block;
`;

const StyledHeader = styled.header`
  position: fixed;
  top: 0;
  width: 100%;
  padding: 12px;
  background: ${theme.color.mineShaft};
  letter-spacing: 1px;
  z-index: 1;

  .autocomplete {
    float: left;
    margin-left: 15px;

    .input{
      width: 35px;
      cursor: pointer;
      background: ${theme.color.mineShaft} url(${SearchIcon}) 6px 47% no-repeat;
      padding: 9px 8px 5px 30px;
      background-size: 18px;
      color: #fff;
      border: none;
      transition: ${theme.transition};

      &:focus {
        width: 240px;
        cursor: auto;
      }
    }

    .list,
    .item {
      padding: 5px;
      font-size: 12px;
    }
  }
`;

const Header = ({
  query,
  vendors,
  onChangeQuery,
  onClickVendor,
  onLogout,
  user,
  isSupport,
  showDropdown,
  onClickMenu,
  onClickSupportDashboard,
  token,
}) => (
  <StyledHeader>
    <Brand to="/">
      <Logo />
      <Separator />
      <Description />
    </Brand>
    <Autocomplete
      placeholder={translate('FIND_PRODUCTS')}
      value={query}
      items={vendors}
      onChange={onChangeQuery}
      onSelect={(_, vendor) => onClickVendor({
        vendor,
        query,
      })}
      autoHeight
      id="menu-search"
    />
    <Navbar>
      <If condition={!isSupport}>
        <Link to="/order-history">{translate('ORDER_HISTORY')}</Link>
        <Separator />
      </If>
      <Username name={user} />
      <Choose>
        <When condition={isSupport}>
          <StyledClickOutside onClickOutside={() => onClickMenu(false)}>
            <Hamburger onClick={() => onClickMenu(!showDropdown)} />
            <VelocityTransitionGroup enter={{ animation: 'fadeIn', duration: 150 }} leave={{ animation: 'fadeOut', duration: 150 }}>
              <If condition={showDropdown}>
                <Dropdown onLogout={onLogout} onClickSupportDashboard={() => onClickSupportDashboard(token)} />
              </If>
            </VelocityTransitionGroup>
          </StyledClickOutside>
        </When>
        <Otherwise>
          <Button compact color="transparent" onClick={onLogout}>{translate('LOGOUT')}</Button>
        </Otherwise>
      </Choose>
    </Navbar>
  </StyledHeader>
);

Header.propTypes = {
  onLogout: func,
  user: string,
  isSupport: bool,
  showDropdown: bool,
  onClickMenu: func.isRequired,
  query: string,
  vendors: arrayOf(object),
  onChangeQuery: func,
  onClickVendor: func,
  onClickSupportDashboard: func,
  token: object,
};

export default Header;
