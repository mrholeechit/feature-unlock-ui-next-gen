import React from 'react';
import styled from 'styled-components';
import FuseLogo from 'images/fuse-logo.svg';

const Logo = styled.img`
  height: 30px;
  width: 88.2px;
  vertical-align: middle;
`;

export default () => (
  <Logo alt="Feature Unlock Logo" src={FuseLogo} />
);
