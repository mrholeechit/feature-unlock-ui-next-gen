import styled from 'styled-components';
import theme from 'utils/styles';
import Link from 'components/Link';

export default styled(Link)`
  color: #fff;
  font-size: 16px;
  margin: 0;
  display: inline-block;
  vertical-align: middle;
  margin-right: 20px;
  font-weight: ${theme.font.semiBold};

  &:last-child {
    margin-right: 0;
  }
`;
