import React from 'react';
import styled from 'styled-components';
import { string } from 'prop-types';
import { mixins } from 'utils/styles';

const StyledUsername = styled.span`
  font-size: 12px;
  margin-right: 20px;
  color: #fff;
  display: inline-block;
  vertical-align: middle;
  ${mixins.ellipsis('120px')}
`;

const Username = ({ name }) => (
  <StyledUsername title={name}>{name}</StyledUsername>
);

Username.propTypes = {
  name: string,
};

export default Username;
