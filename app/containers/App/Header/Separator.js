import styled from 'styled-components';
import { mixins } from 'utils/styles';

export default styled.span`
  padding: 4px 20px;
  position: relative;
  vertical-align: middle;

  &:before {
    content: '';
    position: absolute;
    ${mixins.center('horizontal')}
    height: 100%;
    background: #fff;
    width: 1px;
  }
`;
