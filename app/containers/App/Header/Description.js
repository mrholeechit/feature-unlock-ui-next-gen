import React from 'react';
import styled from 'styled-components';
import theme from 'utils/styles';
import translate from 'utils/translate';

const Description = styled.p`
  color: #fff;
  font-size: ${theme.font.medium};
  margin: 0;
  display: inline-block;
  font-weight: ${theme.font.semiBold};
  vertical-align: middle;
`;

export default () => (
  <Description>{translate('UPGRADES_SUBSCRIPTIONS')}</Description>
);
