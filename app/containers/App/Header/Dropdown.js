import React from 'react';
import Link from 'components/Link';
import { func } from 'prop-types';
import styled from 'styled-components';
import theme from 'utils/styles';
import translate from 'utils/translate';

const List = styled.ul`
  position: absolute;
  top: 70px;
  right: 0;
  margin: 0;
  padding: 0;
  list-style: none;
  background: #333333;
  width: 200px;
  border-radius: 10px;
  box-shadow: 0 20px 20px -15px rgba(0, 0, 0, 0.5);
  z-index: -1;

  &:before {
    content: '';
    position: absolute;
    width: 0;
    height: 0;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-bottom: 10px solid #333333;
    right: 12px;
    transform: translateY(-50%);
    top: -5px;
  }
`;

const Item = styled.li`
  font-size: 16px;
  list-style: none;
  display: inline-block;
  float: left;
  width: 100%;
  position: relative;
  line-height: 54px;
  border-bottom: 1px solid #545454;
  transition: ${theme.transition};

  &:first-child{
    border-radius: 10px 10px 0px 0px;
  }

  &:last-child {
    border-radius: 0px 0px 10px 10px;
  }

  &:hover {
    background: #545454;
  }
`;

const StyledLink = styled(Link)`
  padding: 15px;
  font-size: ${theme.font.tiny};
  line-height: 14px;
  width: 100%;
  float: left;
  color: #FFF;
  text-align: left;

  &:last-child {
    border-bottom: none;
  }
`;

const Dropdown = ({ onLogout, onClickSupportDashboard }) => (
  <List>
    <Item>
      <StyledLink to="/order-history">{translate('ORDER_HISTORY')}</StyledLink>
    </Item>
    <Item>
      <StyledLink onClick={onClickSupportDashboard} >{translate('SUPPORT_DASHBOARD')}</StyledLink>
    </Item>
    <Item>
      <StyledLink onClick={onLogout}>{translate('LOGOUT')}</StyledLink>
    </Item>
  </List>
);

Dropdown.propTypes = {
  onLogout: func,
  onClickSupportDashboard: func,
};

export default Dropdown;
