import React, { PureComponent } from 'react';
import { push } from 'react-router-redux';
import theme from 'utils/styles';
import { parseHash } from 'utils/url';
import { has, not, equals, and } from 'ramda';
import { AUTH, SUPPORT_DASHBOARD_URL } from 'config';
import { makeSelectSearch } from 'containers/SearchPage/selectors';
import { node, func, object } from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectLocale } from 'containers/LanguageProvider/selectors';
import { queryChange } from 'containers/SearchPage/actions';
import Helmet from 'react-helmet';
import styled from 'styled-components';
import Loader from 'components/Loader';
import { addToken, clearToken, toggleDropdown } from './actions';
import { makeSelectApp } from './selectors';
import Header from './Header';
import Footer from './Footer';

const AppContainer = styled.div`
  float: left;
  width: 100%;
  overflow-x: hidden;
`;

const MainContainer = styled.div`
  float: left;
  width: 100%;
  background: ${theme.color.gallery};
  min-height: ${theme.fullHeightNoHeader};
  padding-top: ${({ withPadding }) => withPadding ? '50px' : '0'};
`;

export class App extends PureComponent {
  static propTypes = {
    children: node,
    onAddToken: func,
    onLogin: func,
    onLogout: func,
    router: object,
    app: object,
    location: object,
    onToggleDropdown: func,
    onChangeQuery: func,
    onClickVendor: func,
    search: object,
    onClickSupportDashboard: func,
  };

  componentWillMount() {
    const { router, onAddToken, onLogin } = this.props;

    const urlToken = parseHash(window.location.href);
    const localStorageToken = JSON.parse(window.localStorage.getItem('token'));

    if (has('access_token', urlToken)) {
      onAddToken(urlToken);
      router.replace('/');
    } else if (localStorageToken) {
      onAddToken(localStorageToken);
    } else {
      onLogin();
    }
  }

  componentWillReceiveProps(props) {
    const {
      app: {
        token,
      },
      onLogout,
    } = props;

    const {
      app: {
        token: previousToken,
      },
    } = this.props;

    const tokenIsChanged = not(equals(previousToken, token));
    const hasNewToken = has('access_token', token);

    if (and(tokenIsChanged, not(hasNewToken))) {
      onLogout();
    }
  }

  render() {
    const {
      onLogout,
      children,
      location: {
        pathname,
      },
      app: {
        token,
        user,
        isSupport,
        loading,
        loaded,
        showDropdown,
      },
      search: {
        query,
        vendors,
      },
      onChangeQuery,
      onToggleDropdown,
      onClickVendor,
      onClickSupportDashboard,
    } = this.props;

    const isHome = pathname === '/';

    return (
      <AppContainer>
        <Helmet
          titleTemplate="AGCO Feature Unlock - %s"
          defaultTitle="AGCO Feature Unlock"
        />
        <Loader show={loading} />
        <Loader show={!loaded} />
        <If condition={loaded}>
          <Header
            onLogout={onLogout}
            user={user}
            showDropdown={showDropdown}
            isSupport={isSupport}
            onClickMenu={onToggleDropdown}
            onChangeQuery={onChangeQuery}
            onClickVendor={onClickVendor}
            query={query}
            vendors={vendors}
            onClickSupportDashboard={onClickSupportDashboard}
            token={token}
          />
          <MainContainer withPadding={!isHome}>
            { React.Children.toArray(children) }
          </MainContainer>
          <If condition={isHome}>
            <Footer />
          </If>
        </If>
      </AppContainer>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    onAddToken: (token) => dispatch(addToken(token)),
    onLogin: () => {
      window.location = AUTH.loginUrl;
    },
    onLogout: () => {
      dispatch(clearToken());
      window.location = AUTH.logoutUrl;
    },
    onClickSupportDashboard: (token) => {
      const supportUrl = `${SUPPORT_DASHBOARD_URL}#access_token=${token.access_token}&expires_in=${token.expires_in}&token_type=${token.token_type}`;
      window.location = supportUrl;
    },
    onToggleDropdown: (show) => dispatch(toggleDropdown(show)),
    onChangeQuery: (_, value) => dispatch(queryChange(value)),
    onClickVendor: ({ vendor: { value }, query }) => {
      const isOrderReference = value === 'orderReference';

      if (isOrderReference) {
        return dispatch(push({
          pathname: 'order-history',
          query: {
            orderReference: query,
          },
        }));
      }

      return dispatch(push({
        pathname: 'product',
        query: {
          vendor: value,
          serialNumber: query,
        },
      }));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  search: makeSelectSearch(),
  app: makeSelectApp(),
  locale: makeSelectLocale(),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
