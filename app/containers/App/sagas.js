import { call, put, takeLatest } from 'redux-saga/effects';
import { values, contains, any, __ } from 'ramda';
import API from 'api';
import { AUTH } from 'config';
import { ADD_TOKEN } from './constants';
import { clearToken, addUser, loaded } from './actions';

const isSupport = (roles, userType) => contains(AUTH.roles.SystemUser, roles) && userType !== AUTH.allowedUserTypes.DealerUsers;
const hasUserType = (userType) => contains(userType, values(AUTH.allowedUserTypes));
const hasRoles = (roles) => any(contains(__, roles), values(AUTH.roles));

export function* checkRoles() {
  try {
    const {
      roles, preferredName, countryCode, emailAddress, regionCode, code, userType,
    } = yield call(API.users.get);
    const isAllowed = hasRoles(roles) && hasUserType(userType);

    if (!isAllowed) throw new Error('User not authorized!');

    yield put(addUser({
      roles,
      user: preferredName,
      dealer: {
        countryCode,
        regionCode,
        code,
      },
      isSupport: isSupport(roles, userType),
      emailAddress,
    }));

    yield put(loaded());
  } catch (err) {
    yield put(clearToken());
  }
}

export function* listeners() {
  yield takeLatest(ADD_TOKEN, checkRoles);
}

export default [
  listeners,
];
