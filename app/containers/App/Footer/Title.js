import React from 'react';
import styled from 'styled-components';
import theme from 'utils/styles';
import translate from 'utils/translate';

const Title = styled.strong`
  font-size: ${theme.font.base};
  font-weight: ${theme.font.semiBold};
  color: #fff;
  margin-bottom: 25px;
  display: inline-block;
`;

export default () => (
  <Title>{translate('SUPPORTED_BRANDS')}</Title>
);
