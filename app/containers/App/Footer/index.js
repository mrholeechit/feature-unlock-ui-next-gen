import React from 'react';
import styled from 'styled-components';
import { SUPPORTED_PRODUCTS } from './constants';
import Title from './Title';
import Products from './Products';

const Footer = styled.footer`
  position: fixed;
  bottom: 0;
  text-align: center;
  width: 100%;
`;

export default () => (
  <Footer>
    <Title />
    <Products items={SUPPORTED_PRODUCTS} />
  </Footer>
);
