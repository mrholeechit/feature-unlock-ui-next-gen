import React from 'react';
import {
  arrayOf,
  string,
} from 'prop-types';
import theme from 'utils/styles';
import styled from 'styled-components';

const List = styled.ul`
  float: left;
  width: 100%;
  padding: 0 0 50px 0;
  margin: 0;
`;

const Item = styled.li`
  display: inline-block;
  color: #fff;
  font-size: ${theme.font.normal};
  border-right: 1px solid #fff;
  padding: 0 20px;
  font-weight: ${theme.font.light};

  &:last-child {
    border-right: none;
  }
`;

const Products = ({ items }) => (
  <List>
    <For each="item" index="index" of={items}>
      <Item key={index}>{item}</Item>
    </For>
  </List>
);

Products.propTypes = {
  items: arrayOf(string),
};

export default Products;
