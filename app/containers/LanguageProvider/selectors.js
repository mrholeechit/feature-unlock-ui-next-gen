import { createSelector } from 'reselect';

export const makeSelectLocale = () => createSelector(
  (state) => state.get('language'),
  (state) => state.get('locale')
);
