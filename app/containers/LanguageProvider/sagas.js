import { call, put, takeLatest } from 'redux-saga/effects';
import API from 'api';
import { LOADED } from 'containers/App/constants';
import { prop } from 'ramda';
import acceptLanguage from 'accept-language';
import { DEFAULT_LOCALE } from './constants';
import { changeLocale } from './actions';
import { appLocales } from '../../i18n';

export function* setLocale() {
  try {
    const data = yield call(API.utils.getHeaders);
    acceptLanguage.languages(appLocales);
    const locale = acceptLanguage.get(prop('Accept-Language', data));
    yield put(changeLocale(locale));
  } catch (err) {
    yield put(changeLocale(DEFAULT_LOCALE));
  }
}

export function* listeners() {
  yield takeLatest(LOADED, setLocale);
}

export default [
  listeners,
];
