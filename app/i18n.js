import { addLocaleData } from 'react-intl';

import da from 'react-intl/locale-data/da';
import de from 'react-intl/locale-data/de';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import fi from 'react-intl/locale-data/fi';
import fr from 'react-intl/locale-data/fr';
import it from 'react-intl/locale-data/it';
import nb from 'react-intl/locale-data/nb';
import nl from 'react-intl/locale-data/nl';
import pl from 'react-intl/locale-data/pl';
import pt from 'react-intl/locale-data/pt';
import ru from 'react-intl/locale-data/ru';
import sv from 'react-intl/locale-data/sv';
import tr from 'react-intl/locale-data/tr';

import { DEFAULT_LOCALE } from 'containers/LanguageProvider/constants';

import daTranslationMessages from './translations/da';
import deTranslationMessages from './translations/de';
import enTranslationMessages from './translations/en';
import esTranslationMessages from './translations/es';
import fiTranslationMessages from './translations/fi';
import frTranslationMessages from './translations/fr';
import itTranslationMessages from './translations/it';
import nbTranslationMessages from './translations/nb';
import nlTranslationMessages from './translations/nl';
import plTranslationMessages from './translations/pl';
import ptTranslationMessages from './translations/pt';
import ruTranslationMessages from './translations/ru';
import svTranslationMessages from './translations/sv';
import trTranslationMessages from './translations/tr';

addLocaleData([...da, ...de, ...en, ...es, ...fi, ...fr, ...it, ...nb, ...nl, ...pl, ...pt, ...ru, ...sv, ...tr]);

export const appLocales = ['da', 'de', 'en', 'es', 'fi', 'fr', 'it', 'nb', 'nl', 'pl', 'pt', 'ru', 'sv', 'tr'];

export const formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages = locale !== DEFAULT_LOCALE
    ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages)
    : {};
  return Object.keys(messages).reduce((formattedMessages, key) => {
    let message = messages[key];
    if (!message && locale !== DEFAULT_LOCALE) {
      message = defaultFormattedMessages[key];
    }
    return Object.assign(formattedMessages, { [key]: message });
  }, {});
};

export const translationMessages = {
  da: formatTranslationMessages('da', daTranslationMessages),
  de: formatTranslationMessages('de', deTranslationMessages),
  en: formatTranslationMessages('en', enTranslationMessages),
  es: formatTranslationMessages('es', esTranslationMessages),
  fi: formatTranslationMessages('fi', fiTranslationMessages),
  fr: formatTranslationMessages('fr', frTranslationMessages),
  it: formatTranslationMessages('it', itTranslationMessages),
  nb: formatTranslationMessages('nb', nbTranslationMessages),
  nl: formatTranslationMessages('nl', nlTranslationMessages),
  pl: formatTranslationMessages('pl', plTranslationMessages),
  pt: formatTranslationMessages('pt', ptTranslationMessages),
  ru: formatTranslationMessages('ru', ruTranslationMessages),
  sv: formatTranslationMessages('sv', svTranslationMessages),
  tr: formatTranslationMessages('tr', trTranslationMessages),
};

