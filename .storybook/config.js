import { configure, setAddon } from '@kadira/storybook';
import infoAddon from '@kadira/react-storybook-addon-info';
import notesAddon from '@kadira/storybook-addon-notes';

const req = require.context('../app/components', true, /stories\.js$/)

function loadStories() {
  req.keys().forEach((filename) => req(filename))
}

setAddon(infoAddon);
setAddon(notesAddon);

configure(loadStories, module);
